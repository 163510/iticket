package iticket.integration.iticket.usuario.exception.usuario;

public class UsuarioNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 3626595141453756039L;

	public UsuarioNotFoundException() {
		super();
	}
	
	public UsuarioNotFoundException(String msg) {
		super(msg);
	}
}
