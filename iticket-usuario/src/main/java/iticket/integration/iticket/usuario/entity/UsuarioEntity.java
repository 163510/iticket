package iticket.integration.iticket.usuario.entity;

import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import iticket.integration.iticket.usuario.domain.usuario.UsuarioRequest;
import iticket.integration.iticket.usuario.utils.EncryptUtils;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Table(name = "usuario")
@NoArgsConstructor
@AllArgsConstructor
public class UsuarioEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "nome")
	private String nome;
	
	@Column(name = "telefone")
	private String telefone;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "senha")
	private String senha;
	
	@Column(name = "primeiro_acesso")
	private Boolean primeiroAcesso;
	
	@Column(name = "usuario_criador_id")
	private Long usuarioCriadorId;
	
	@Column(name = "data_criacao")
	private LocalDateTime dataCriacao;
	
	@Column(name = "data_atualizacao")
	private LocalDateTime dataAtualizacao;
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "usuario_grupo",
			joinColumns = @JoinColumn(name = "usuario_id"),
			inverseJoinColumns = @JoinColumn(name = "grupo_id")
			)
	private List<GrupoEntity> grupos;

	public UsuarioEntity(UsuarioRequest user) throws NoSuchAlgorithmException {
		this.nome = user.getNome();
		this.telefone = user.getTelefone();
		this.email = user.getEmail();
		this.senha = EncryptUtils.encrypt(user.getSenha());
		this.usuarioCriadorId = user.getUsuarioCriadorId();
		this.grupos = user.getGrupos().stream().map(GrupoEntity::new).collect(Collectors.toList());
		this.primeiroAcesso = true;
	}
	
}
