package iticket.integration.iticket.usuario.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import iticket.integration.iticket.usuario.entity.GrupoEntity;

@Repository
public interface GrupoRepository extends JpaRepository<GrupoEntity, Long>{

	boolean existsByNome(String nome);
	
	List<GrupoEntity> findByNome(String nome);
	
}
