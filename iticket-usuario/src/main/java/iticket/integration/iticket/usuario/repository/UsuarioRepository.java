package iticket.integration.iticket.usuario.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import iticket.integration.iticket.usuario.entity.UsuarioEntity;

@Repository
public interface UsuarioRepository extends JpaRepository<UsuarioEntity, Long>{

	List<UsuarioEntity> findByEmail(String email);
	
	Optional<UsuarioEntity> findByEmailAndSenha(String email, String senha);
	
	@Query(value = "SELECT * FROM usuario u INNER JOIN usuario_grupo ug ON u.id = ug.usuario_id INNER JOIN grupo g ON ug.grupo_id = g.id where g.id IN :atendentesGruposId", nativeQuery = true)
	List<UsuarioEntity> usuarioAtendentes(List<Long> atendentesGruposId);
}
