package iticket.integration.iticket.usuario.service;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import iticket.integration.iticket.usuario.dto.LoginResponse;
import iticket.integration.iticket.usuario.dto.UserTokenInfo;
import iticket.integration.iticket.usuario.exception.InvalidTokenException;
import iticket.integration.iticket.usuario.utils.JsonUtils;

public class TokenAuthenticationService {

	private TokenAuthenticationService() {}

	// EXPIRATION_TIME = 10 dias
	static final long EXPIRATION_TIME = 860_000_000;
	static final String SECRET = "QQHKH50=Yoz3aA7w-156HjSp";

	public static LoginResponse generateAuthToken(UserTokenInfo tokenInfo) throws JsonProcessingException {
		String jwtToken = Jwts.builder().setSubject(JsonUtils.writeValueAsString(tokenInfo))
				.setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
				.signWith(SignatureAlgorithm.HS512, SECRET).compact();

		LoginResponse response = new LoginResponse();
		response.setToken(jwtToken);
		response.setUserName(tokenInfo.getNome());
		response.setRoles(tokenInfo.getRoles());
		response.setId(tokenInfo.getId());

		return response;
	}

	public static UserTokenInfo validateToken(String token) throws JsonProcessingException, InvalidTokenException {
		String userInfo = Jwts.parser().setSigningKey(SECRET).parseClaimsJws(token).getBody().getSubject();

		if (userInfo != null) {
			return JsonUtils.readValue(userInfo, UserTokenInfo.class);
		}

		throw new InvalidTokenException("Invalid received auth token!");
	}

	public static Boolean userHasRoleAccess(String token, List<String> expectedRoles)
			throws JsonProcessingException, InvalidTokenException {

		UserTokenInfo tokenInfo = validateToken(token);

		for (String role : expectedRoles) {
			if(tokenInfo.getRoles().contains(role)) {
				return true;
			}			
		}
		
		return false;
	}

}
