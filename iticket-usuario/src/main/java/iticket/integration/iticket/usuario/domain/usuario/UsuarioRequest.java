package iticket.integration.iticket.usuario.domain.usuario;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import iticket.integration.iticket.usuario.dto.GrupoDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class UsuarioRequest {

	private String nome;
	private String telefone;
	private String email;
	private String senha;
	private Long usuarioCriadorId;
	private List<GrupoDTO> grupos;
	
}
