package iticket.integration.iticket.usuario.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.springframework.stereotype.Component;
import org.springframework.util.Base64Utils;

@Component
public class EncryptUtils {

	private EncryptUtils() {}
	
	public static String encrypt(String msg) throws NoSuchAlgorithmException {
		MessageDigest digest = MessageDigest.getInstance("SHA-256");
		byte[] bytes = digest.digest(msg.getBytes());
		return Base64Utils.encodeToString(bytes);
	}
	
}
