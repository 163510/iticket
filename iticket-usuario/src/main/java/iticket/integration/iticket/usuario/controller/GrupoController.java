package iticket.integration.iticket.usuario.controller;

import static iticket.integration.iticket.usuario.constants.PermissionRoles.ADMIN;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import iticket.integration.iticket.usuario.domain.grupo.GrupoRequest;
import iticket.integration.iticket.usuario.dto.GrupoDTO;
import iticket.integration.iticket.usuario.service.GrupoService;
import iticket.integration.iticket.usuario.service.UsuarioService;

@RestController
public class GrupoController {

	@Autowired
	private GrupoService grupoService;
	
	@Autowired
	private UsuarioService usuarioService;
	
	@GetMapping("/grupo/all")
	public ResponseEntity<Object> findGrupos(@RequestHeader("Authorization") String token) {
		
		ResponseEntity<Object> userPermission = usuarioService.validateUsertokenAndPremission(token, Arrays.asList(ADMIN.getRole()));
		
		if(userPermission != null) {
			return userPermission;
		}
		
		List<GrupoDTO> grupos = grupoService.findAll();
		return ResponseEntity.status(HttpStatus.OK).body(grupos);
	}
	
	@GetMapping("/grupo/{id}")
	public ResponseEntity<Object> findGrupoById(@PathVariable("id") Long id, @RequestHeader("Authorization") String token) {
		
		ResponseEntity<Object> userPermission = usuarioService.validateUsertokenAndPremission(token, Arrays.asList(ADMIN.getRole()));
		
		if(userPermission != null) {
			return userPermission;
		}
		
		GrupoDTO grupo = grupoService.findById(id);
		return ResponseEntity.status(HttpStatus.OK).body(grupo);
	}
	
	@PostMapping("/grupo/save")
	public ResponseEntity<Object> saveGrupo(@RequestHeader("Authorization") String token, @RequestBody GrupoRequest request) {
		
		ResponseEntity<Object> userPermission = usuarioService.validateUsertokenAndPremission(token, Arrays.asList(ADMIN.getRole()));
		
		if(userPermission != null) {
			return userPermission;
		}
		
		return grupoService.saveGrupo(request);
	}
	
	@DeleteMapping("/grupo/delete/{id}")
	public ResponseEntity<Object> deleteGrupo(@PathVariable("id") Long id, @RequestHeader("Authorization") String token) {
		
		ResponseEntity<Object> userPermission = usuarioService.validateUsertokenAndPremission(token, Arrays.asList(ADMIN.getRole()));
		
		if(userPermission != null) {
			return userPermission;
		}
		
		return grupoService.deleteGrupo(id);
	}
}
