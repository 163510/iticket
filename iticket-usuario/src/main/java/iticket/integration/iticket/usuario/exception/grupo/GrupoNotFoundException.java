package iticket.integration.iticket.usuario.exception.grupo;

public class GrupoNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -3160909036367513182L;

	public GrupoNotFoundException() {
		super();
	}
	
	public GrupoNotFoundException(String msg) {
		super(msg);
	}
}
