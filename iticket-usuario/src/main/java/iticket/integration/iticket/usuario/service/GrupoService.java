package iticket.integration.iticket.usuario.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import iticket.integration.iticket.usuario.domain.DefaultResponse;
import iticket.integration.iticket.usuario.domain.grupo.GrupoRequest;
import iticket.integration.iticket.usuario.dto.GrupoDTO;
import iticket.integration.iticket.usuario.entity.GrupoEntity;
import iticket.integration.iticket.usuario.exception.grupo.GrupoAlreadyRegisteredException;
import iticket.integration.iticket.usuario.exception.grupo.GrupoNotFoundException;
import iticket.integration.iticket.usuario.repository.GrupoRepository;

@Service
public class GrupoService {

	@Autowired
	private GrupoRepository grupoRepository;
	
	public ResponseEntity<Object> saveGrupo(GrupoRequest request) {
		
		if(grupoRepository.existsByNome(request.getNome())) {
			throw new GrupoAlreadyRegisteredException("O grupo <" + request.getNome() + "> já está registrado!");
		}

		GrupoEntity grupoEntity = new GrupoEntity(request);
		
		grupoRepository.save(grupoEntity);
		
		DefaultResponse response = buildResponseWithSingleMessage(true, "Grupo <" + grupoEntity.getNome() +"> successfully registered!");
		
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}



	public List<GrupoDTO> findAll() {

		List<GrupoEntity> grupos = grupoRepository.findAll();
		
		return grupos.stream()
				.map(GrupoDTO::new)
				.collect(Collectors.toList());
	}

	
	private DefaultResponse buildResponseWithSingleMessage(boolean isSuccess, String msg) {
		List<String> message = new ArrayList<>();
		message.add(msg);
		return new DefaultResponse(isSuccess, message);
	}



	public GrupoDTO findById(Long id) {

		Optional<GrupoEntity> optGrupo = grupoRepository.findById(id);
		
		if(!optGrupo.isPresent()) {
			throw new GrupoNotFoundException("Nenhum grupo foi encontrado com o id <" + id + ">");
		}
		
		GrupoEntity grupo = optGrupo.get();
		
		return new GrupoDTO(grupo);
	}



	public ResponseEntity<Object> deleteGrupo(Long id) {

		grupoRepository.deleteById(id);
		
		DefaultResponse response = new DefaultResponse(true, new ArrayList<>());
		response.getMessage().add("Grupo deletado!");
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
}
