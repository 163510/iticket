package iticket.integration.iticket.usuario.service;

import static iticket.integration.iticket.usuario.constants.PermissionRoles.ATENDENTE;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;

import iticket.integration.iticket.usuario.domain.DefaultResponse;
import iticket.integration.iticket.usuario.domain.usuario.ChangePasswordRequest;
import iticket.integration.iticket.usuario.domain.usuario.LoginRequest;
import iticket.integration.iticket.usuario.domain.usuario.UsuarioRequest;
import iticket.integration.iticket.usuario.domain.usuario.UsuarioUpdate;
import iticket.integration.iticket.usuario.dto.GrupoDTO;
import iticket.integration.iticket.usuario.dto.LoginResponse;
import iticket.integration.iticket.usuario.dto.UserTokenInfo;
import iticket.integration.iticket.usuario.dto.UsuarioDTO;
import iticket.integration.iticket.usuario.entity.GrupoEntity;
import iticket.integration.iticket.usuario.entity.UsuarioEntity;
import iticket.integration.iticket.usuario.exception.usuario.UserInvalidPasswordException;
import iticket.integration.iticket.usuario.exception.usuario.UsuarioNotFoundException;
import iticket.integration.iticket.usuario.repository.GrupoRepository;
import iticket.integration.iticket.usuario.repository.UsuarioRepository;
import iticket.integration.iticket.usuario.utils.EncryptUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class UsuarioService {

	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Autowired
	private GrupoRepository grupoRepository;
	
	private static final String EMAIL_EM_USO = "Email já utilizado!";
	
	public ResponseEntity<Object> saveUsuario(UsuarioRequest request) throws NoSuchAlgorithmException {
		
		DefaultResponse response = new DefaultResponse(true, new ArrayList<>());
		
		validateRequest(request, response);
		
		if(!response.isSuccess()) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}

		UsuarioEntity userEntity = new UsuarioEntity(request);
		
		usuarioRepository.save(userEntity);
		
		response.getMessage().add("Usuário Cadastrado com sucesso!");
		return ResponseEntity.status(HttpStatus.OK).body(response);
		
	}

	private void validateRequest(UsuarioRequest request, DefaultResponse response) {

		if(request.getEmail() == null || request.getEmail().isEmpty()) {
			response.setSuccess(false);
			response.getMessage().add("E-mail é requerido!");
		}
		
		if(request.getNome() == null || request.getNome().isEmpty()) {
			response.setSuccess(false);
			response.getMessage().add("Nome é requerido!");
		}
		
		if(request.getSenha() == null || request.getSenha().isEmpty()) {
			response.setSuccess(false);
			response.getMessage().add("Senha é requerido!");
		}
		
		if(request.getTelefone() == null || request.getTelefone().isEmpty()) {
			response.setSuccess(false);
			response.getMessage().add("Telefone é requerido!");
		}
		
		if(request.getGrupos() == null || request.getGrupos().isEmpty()) {
			response.setSuccess(false);
			response.getMessage().add("É necessário vincular o usuário a um grupo!");
			return;
		}
		validateEmail(request.getEmail(), response, null);
		
		for (GrupoDTO grupo : request.getGrupos()) {
			if(!grupoRepository.existsById(grupo.getId())) {
				response.setSuccess(false);
				response.getMessage().add("Grupo <" + grupo.getNome() + "> inválido.");
			}
		}
		
	}

	private void validateEmail(String email, DefaultResponse response, Long id) {
		if(email != null) {
			if(!Pattern.compile("^(.+)@(\\S+)$").matcher(email).matches()) {
				response.setSuccess(false);
				response.getMessage().add("É necessário informar um email válido!");
			}
			
			List<UsuarioEntity> findByEmail = usuarioRepository.findByEmail(email);
			
			if(id != null) {
				validaForUpdate(findByEmail, id, response);
				return;
			}
			
			if(!findByEmail.isEmpty()) {
				response.setSuccess(false);
				response.getMessage().add(EMAIL_EM_USO);
			}
			
		}
	}

	private void validaForUpdate(List<UsuarioEntity> findByEmail, Long id, DefaultResponse response) {
		if(findByEmail.size() == 1) {
			
			if(!findByEmail.get(0).getId().equals(id)) {
				response.setSuccess(false);
				response.getMessage().add(EMAIL_EM_USO);
			}
			
		}else if(!findByEmail.isEmpty()) {
			response.setSuccess(false);
			response.getMessage().add(EMAIL_EM_USO);
		}
		
	}

	public ResponseEntity<Object> changePassoword(ChangePasswordRequest request) {

		DefaultResponse response = new DefaultResponse(true, new ArrayList<>());
		
		Optional<UsuarioEntity> optUser = usuarioRepository.findById(request.getUserId());
		
		if(!optUser.isPresent()) {
			response.setSuccess(false);
			response.getMessage().add("Por Favor informe o id do usuário!");
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}
		
		UsuarioEntity user = optUser.get();
		if(!user.getSenha().equalsIgnoreCase(request.getOldSenha())) {
			throw new UserInvalidPasswordException("Senha inválida!");
		}
		
		validatePassowrdRequest(request, response);
		
		if(!response.isSuccess()) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}
		
		user.setSenha(request.getNewSenha());
		usuarioRepository.save(user);
		
		response.getMessage().add("Senha atualizada!");
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}

	private void validatePassowrdRequest(ChangePasswordRequest request, DefaultResponse response) {

		if(request.getOldSenha() == null || request.getOldSenha().isEmpty()) {
			response.setSuccess(false);
			response.getMessage().add("Por favor, informe a antiga senha!");
		}
		
		if(request.getNewSenha() == null || request.getNewSenha().isEmpty()) {
			response.setSuccess(false);
			response.getMessage().add("Por favor, informe a nova senha!");
		}
		
		if(request.getOldSenha() != null && !request.getOldSenha().isEmpty() 
				&& request.getNewSenha() != null && !request.getNewSenha().isEmpty() 
				&& request.getOldSenha().equalsIgnoreCase(request.getNewSenha())) {
			response.setSuccess(false);
			response.getMessage().add("A nova senha não pode ser igual a antiga!");
		}
		
	}

	public ResponseEntity<Object> updateUser(Long id, UsuarioUpdate updateRequest) {
		
		DefaultResponse response = new DefaultResponse(true, new ArrayList<>());
		
		Optional<UsuarioEntity> optUsuario = usuarioRepository.findById(id);
		
		if(!optUsuario.isPresent()) {
			response.setSuccess(false);
			response.getMessage().add("Usuário não encontrado!");
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
		}
		
		UsuarioEntity user = optUsuario.get();
		
		validateUpdateRequest(updateRequest, response, id);
		
		if(!response.isSuccess()) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}
		
		user.setNome(updateRequest.getNome());
		user.setEmail(updateRequest.getEmail());
		user.setGrupos(updateRequest.getGrupos().stream().map(GrupoEntity::new).collect(Collectors.toList()));
		user.setTelefone(updateRequest.getTelefone());
		
		usuarioRepository.save(user);
		
		response.getMessage().add("Usuário atualizado!");
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}

	private void validateUpdateRequest(UsuarioUpdate updateRequest, DefaultResponse response, Long id) {

		if(updateRequest.getEmail() == null || updateRequest.getEmail().isEmpty()) {
			response.setSuccess(false);
			response.getMessage().add("Por favor informe um e-mail!");
		}
		
		if(updateRequest.getNome() == null || updateRequest.getNome().isEmpty()) {
			response.setSuccess(false);
			response.getMessage().add("Por favor informe o nome do usuário");
		}
		
		if(updateRequest.getTelefone() == null || updateRequest.getTelefone().isEmpty()) {
			response.setSuccess(false);
			response.getMessage().add("Por favor informe o telefone!");
		}
		
		if(updateRequest.getGrupos() == null || updateRequest.getGrupos().isEmpty()) {
			response.setSuccess(false);
			response.getMessage().add("É necessário vincular o usuário a um grupo!");
			return;
		}
		validateEmail(updateRequest.getEmail(), response, id);
		for (GrupoDTO grupo : updateRequest.getGrupos()) {
			if(!grupoRepository.existsById(grupo.getId())) {
				response.setSuccess(false);
				response.getMessage().add("Grupo <" + grupo.getNome() + "> inválido.");
			}
		}
	}

	public ResponseEntity<Object> findAll() {
		
		List<UsuarioEntity> users = usuarioRepository.findAll();
		
		List<UsuarioDTO> usersDto = users.stream().map(UsuarioDTO::new).collect(Collectors.toList());
		
		return ResponseEntity.status(HttpStatus.OK).body(usersDto);
	}
	
	public ResponseEntity<Object> findAllAtendentes() {
		
		List<GrupoEntity> atendetesRoles = grupoRepository.findByNome(ATENDENTE.getRole());
		
		if(atendetesRoles.isEmpty()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
		
		List<Long> ids = atendetesRoles.stream().map(GrupoEntity::getId).collect(Collectors.toList());
		
		List<UsuarioEntity> users = usuarioRepository.usuarioAtendentes(ids);
		
		List<UsuarioDTO> usersDto = users.stream().map(UsuarioDTO::new).collect(Collectors.toList());
		
		return ResponseEntity.status(HttpStatus.OK).body(usersDto);
	}

	public ResponseEntity<Object> findById(Long id) {
		
		Optional<UsuarioEntity> optUser = usuarioRepository.findById(id);
		
		if(!optUser.isPresent()) {
			throw new UsuarioNotFoundException("Usuário com id <" + id + "> não encontrado!");
		}
		
		UsuarioEntity user = optUser.get();
		
		return ResponseEntity.status(HttpStatus.OK).body(new UsuarioDTO(user));
	}

	public ResponseEntity<Object> deleteUsuario(Long id) {

		usuarioRepository.deleteById(id);
		
		DefaultResponse response = new DefaultResponse(true, new ArrayList<>());
		response.getMessage().add("Usuário deletado!");
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}

	public ResponseEntity<LoginResponse> login(LoginRequest login) throws NoSuchAlgorithmException, JsonProcessingException {

		String senha = EncryptUtils.encrypt(login.getSenha());
		
		Optional<UsuarioEntity> optUser = usuarioRepository.findByEmailAndSenha(login.getEmail(), senha);
		
		if(!optUser.isPresent()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
		
		UserTokenInfo tokenInfo = new UserTokenInfo(optUser.get());
		
		LoginResponse response = TokenAuthenticationService.generateAuthToken(tokenInfo);
		
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
	public ResponseEntity<Object> validateUsertokenAndPremission(String token, List<String> desiredPermission) {
		
		Boolean userHasRoleAccess = false;
		
		try {
			userHasRoleAccess = TokenAuthenticationService.userHasRoleAccess(token, desiredPermission);
		}catch (Exception e) {
			log.error("findAll() - [ERROR] - <{}> - message: <{}>", e.getClass(), e.getMessage());
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		
		if(userHasRoleAccess == null || Boolean.FALSE.equals(userHasRoleAccess)) {
			return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
		}
		
		return null;
	}
	
}
