package iticket.integration.iticket.usuario.exception.handle;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import feign.FeignException;
import iticket.integration.iticket.usuario.domain.DefaultResponse;
import iticket.integration.iticket.usuario.exception.grupo.GrupoAlreadyRegisteredException;
import iticket.integration.iticket.usuario.exception.grupo.GrupoNotFoundException;
import iticket.integration.iticket.usuario.exception.usuario.UserInvalidPasswordException;
import iticket.integration.iticket.usuario.exception.usuario.UsuarioNotFoundException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestControllerAdvice
public class ExceptionHandle {

	// #####################
	// ####### GRUPO #######
	// #####################
	
	@ExceptionHandler(GrupoAlreadyRegisteredException.class)
	public ResponseEntity<DefaultResponse> handleGrupoAlreadyRegisteredException(WebRequest request, GrupoAlreadyRegisteredException e) {
		log.info("handleGrupoAlreadyRegisteredException() - [INFO] - path <{}> - message <{}>", request.getContextPath(), e.getMessage());
		List<String> messages = new ArrayList<>();
		messages.add(e.getMessage());
		DefaultResponse response = new DefaultResponse(false, messages);
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
	}
	
	@ExceptionHandler(GrupoNotFoundException.class)
	public ResponseEntity<DefaultResponse> handleGrupoNotFoundException(WebRequest request, GrupoNotFoundException e) {
		log.info("handleGrupoNotFoundException() - [INFO] - path <{}> - message <{}>", request.getContextPath(), e.getMessage());
		List<String> messages = new ArrayList<>();
		messages.add(e.getMessage());
		DefaultResponse response = new DefaultResponse(false, messages);
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
	}
	
	// #####################
	// ####### USER ########
	// #####################
	
	@ExceptionHandler(UserInvalidPasswordException.class)
	public ResponseEntity<DefaultResponse> handleUserInvalidPasswordException(WebRequest request, UserInvalidPasswordException e) {
		log.info("handleUserInvalidPasswordException() - [INFO] - path <{}> - message <{}>", request.getContextPath(), e.getMessage());
		List<String> messages = new ArrayList<>();
		messages.add(e.getMessage());
		DefaultResponse response = new DefaultResponse(false, messages);
		return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(response);
	}
	
	@ExceptionHandler(UsuarioNotFoundException.class)
	public ResponseEntity<DefaultResponse> handleUsuarioNotFoundException(WebRequest request, UsuarioNotFoundException e) {
		log.info("handleUsuarioNotFoundException() - [INFO] - path <{}> - message <{}>", request.getContextPath(), e.getMessage());
		List<String> messages = new ArrayList<>();
		messages.add(e.getMessage());
		DefaultResponse response = new DefaultResponse(false, messages);
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
	}
	
	
	// #####################
	// ###### GENERIC ######
	// #####################
	
	@ExceptionHandler(FeignException.class)
	public ResponseEntity<DefaultResponse> handleFeignException(WebRequest request, FeignException e) {
		log.error("handleFeignException() - [INFO] - path <{}> - message <{}>", request.getContextPath(), e.contentUTF8());
		List<String> messages = new ArrayList<>();
		messages.add(e.contentUTF8());
		DefaultResponse response = new DefaultResponse(false, messages);
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
	}
	
	@ExceptionHandler(Exception.class)
	public ResponseEntity<DefaultResponse> handleException(WebRequest request, Exception e) {
		log.error("handleException() - [ERROR] - path <{}> - message <{}> -  e:", request.getContextPath(), e.getMessage(), e);
		List<String> messages = new ArrayList<>();
		messages.add(e.getMessage());
		DefaultResponse response = new DefaultResponse(false, messages);
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
	}
}
