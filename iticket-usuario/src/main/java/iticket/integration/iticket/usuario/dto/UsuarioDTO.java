package iticket.integration.iticket.usuario.dto;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import iticket.integration.iticket.usuario.entity.UsuarioEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class UsuarioDTO {

	private Long id;
	private String nome;
	private String telefone;
	private String email;
	private Long usuarioCriadorId;
	private Boolean primeiroAcesso;
	private LocalDateTime dataCriacao;
	private LocalDateTime dataAtualizacao;
	private List<GrupoDTO> grupos;
	
	public UsuarioDTO (UsuarioEntity entity) {
		this.id = entity.getId();
		this.nome = entity.getNome();
		this.telefone = entity.getTelefone();
		this.email = entity.getEmail();
		this.usuarioCriadorId = entity.getUsuarioCriadorId();
		this.primeiroAcesso = entity.getPrimeiroAcesso();
		this.dataCriacao = entity.getDataCriacao();
		this.dataAtualizacao = entity.getDataAtualizacao();
		this.grupos = entity.getGrupos().stream().map(GrupoDTO::new).collect(Collectors.toList());
	}
	
}
