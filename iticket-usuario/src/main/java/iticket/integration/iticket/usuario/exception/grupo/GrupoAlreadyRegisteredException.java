package iticket.integration.iticket.usuario.exception.grupo;

public class GrupoAlreadyRegisteredException extends RuntimeException {

	private static final long serialVersionUID = 3320853945470345252L;

	public GrupoAlreadyRegisteredException() {
		super();
	}
	
	public GrupoAlreadyRegisteredException(String msg) {
		super(msg);
	}
}
