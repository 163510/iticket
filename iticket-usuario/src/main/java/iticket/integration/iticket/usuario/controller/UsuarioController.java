package iticket.integration.iticket.usuario.controller;

import static iticket.integration.iticket.usuario.constants.PermissionRoles.ADMIN;
import static iticket.integration.iticket.usuario.constants.PermissionRoles.ATENDENTE;
import static iticket.integration.iticket.usuario.constants.PermissionRoles.USUARIO;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;

import iticket.integration.iticket.usuario.domain.DefaultResponse;
import iticket.integration.iticket.usuario.domain.usuario.ChangePasswordRequest;
import iticket.integration.iticket.usuario.domain.usuario.LoginRequest;
import iticket.integration.iticket.usuario.domain.usuario.UsuarioRequest;
import iticket.integration.iticket.usuario.domain.usuario.UsuarioUpdate;
import iticket.integration.iticket.usuario.dto.LoginResponse;
import iticket.integration.iticket.usuario.service.UsuarioService;

@RestController
public class UsuarioController {

	@Autowired
	private UsuarioService usuarioService;
	
	private static final String APIKEY = "NkkkcmpSbmV2ZDQzTVI3byoycFdiTk1fMmFMVmVTLWZIcjZMU2dGQ0tHNUU0RUZGTiU=";
	
	@GetMapping(path = "/usuario/all", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> findAllUsers(@RequestHeader("Authorization") String token) {
		
		ResponseEntity<Object> userPermission = usuarioService.validateUsertokenAndPremission(token, Arrays.asList(ADMIN.getRole()));
		
		if(userPermission != null) {
			return userPermission;
		}
		
		return usuarioService.findAll();
	}
	
	@GetMapping(path = "/usuario/atendentes", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> findAllAtendenteUsers(@RequestHeader("Authorization") String token) {
		
		ResponseEntity<Object> userPermission = usuarioService.validateUsertokenAndPremission(token, Arrays.asList(ADMIN.getRole(), ATENDENTE.getRole(), USUARIO.getRole()));
		
		if(userPermission != null) {
			return userPermission;
		}
		
		return usuarioService.findAllAtendentes();
	}
	
	@GetMapping(path = "/usuario/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> findUserById(@PathVariable("id") Long id, @RequestHeader("Authorization") String token) {
		
		ResponseEntity<Object> userPermission = usuarioService.validateUsertokenAndPremission(token, Arrays.asList(ADMIN.getRole(), ATENDENTE.getRole(), USUARIO.getRole()));
		
		if(userPermission != null) {
			return userPermission;
		}
		
		return usuarioService.findById(id);
	}
	
	@PostMapping(path = "/usuario/save", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> saveUsuario(@RequestBody UsuarioRequest request, @RequestHeader("Authorization") String token) throws NoSuchAlgorithmException {
		
		ResponseEntity<Object> userPermission = usuarioService.validateUsertokenAndPremission(token, Arrays.asList(ADMIN.getRole()));
		
		if(userPermission != null) {
			return userPermission;
		}
		
		return usuarioService.saveUsuario(request);
	}
	
	@PostMapping(path ="/usuario/login", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LoginResponse> login(@RequestBody LoginRequest login) throws NoSuchAlgorithmException, JsonProcessingException {
		return usuarioService.login(login);
	}

	@PutMapping(path = "/usuario/change/password", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> updatePassword(@RequestHeader("Authorization") String token, @RequestBody ChangePasswordRequest request) {
		
		ResponseEntity<Object> userPermission = usuarioService.validateUsertokenAndPremission(token, Arrays.asList(ADMIN.getRole()));
		
		if(userPermission != null) {
			return userPermission;
		}
		
		return usuarioService.changePassoword(request);
	}
	
	@PutMapping(path = "/usuario/update/{id}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> updateUser(@PathVariable("id") Long id, @RequestHeader("Authorization") String token, @RequestBody UsuarioUpdate updateRequest) {
		
		ResponseEntity<Object> userPermission = usuarioService.validateUsertokenAndPremission(token, Arrays.asList(ADMIN.getRole()));
		
		if(userPermission != null) {
			return userPermission;
		}
		
		return usuarioService.updateUser(id, updateRequest);
	}
	
	@DeleteMapping(path = "/usuario/delete/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> deleteUsuario(@PathVariable("id") Long id, @RequestHeader("Authorization") String token) {
		
		ResponseEntity<Object> userPermission = usuarioService.validateUsertokenAndPremission(token, Arrays.asList(ADMIN.getRole()));
		
		if(userPermission != null) {
			return userPermission;
		}
		
		return usuarioService.deleteUsuario(id);
	}
	
	@PostMapping(path = "/usuario/validate/token", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> validateToken(@RequestHeader("apikey") String apikey, @RequestHeader("Authorization") String token, @RequestParam("roles") List<String> roles) {
		
		if(apikey == null || !APIKEY.equals(apikey)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		
		ResponseEntity<Object> userPermission = usuarioService.validateUsertokenAndPremission(token, roles);
		
		if(userPermission != null) {
			return userPermission;
		}
		
		return ResponseEntity.status(HttpStatus.OK).body(new DefaultResponse(true, new ArrayList<>()));
	}
	
}
