package iticket.integration.iticket.usuario.dto;

import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import iticket.integration.iticket.usuario.entity.GrupoEntity;
import iticket.integration.iticket.usuario.entity.UsuarioEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_NULL)
public class UserTokenInfo {

	private Long id;
	private String nome;
	private List<String> roles;
	
	public UserTokenInfo(UsuarioEntity entity) {
		this.id = entity.getId();
		this.nome = entity.getNome();
		this.roles = entity.getGrupos().stream().map(GrupoEntity::getNome).collect(Collectors.toList());
	} 
}
