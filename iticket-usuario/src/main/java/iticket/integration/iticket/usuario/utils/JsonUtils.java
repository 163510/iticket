package iticket.integration.iticket.usuario.utils;

import java.io.IOException;

import javax.servlet.ServletInputStream;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

@Component
public class JsonUtils {

	private JsonUtils() {}
	
	private static final ObjectMapper MAPPER = new ObjectMapper()
			.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
			.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
			.registerModule(new JavaTimeModule());
	
	
	public static <T> T readValue (String value, Class<T> targetClass) throws JsonProcessingException {
		return MAPPER.readValue(value, targetClass);
	}

	public static <T> T readValue (ServletInputStream value, Class<T> targetClass) throws IOException {
		return MAPPER.readValue(value, targetClass);
	}
	
	public static String writeValueAsString(Object obj) throws JsonProcessingException {
		return MAPPER.writeValueAsString(obj);
	}
}
