package iticket.integration.iticket.usuario;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IticketUsuarioApplication {

	public static void main(String[] args) {
		SpringApplication.run(IticketUsuarioApplication.class, args);
	}

}
