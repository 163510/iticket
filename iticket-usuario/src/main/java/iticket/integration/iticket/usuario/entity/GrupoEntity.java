package iticket.integration.iticket.usuario.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import iticket.integration.iticket.usuario.domain.grupo.GrupoRequest;
import iticket.integration.iticket.usuario.dto.GrupoDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Table(name = "grupo")
@NoArgsConstructor
@AllArgsConstructor
public class GrupoEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "nome")
	private String nome;
	
	@Lob
	@Column(name = "descricao")
	private String descricao;
	
	@Column(name = "data_criacao")
	private LocalDateTime dataCriacao;
	
	@Column(name = "data_atualizacao")
	private LocalDateTime dataAtualizacao;
	
	public GrupoEntity(GrupoRequest request) {
		nome = request.getNome();
		descricao = request.getDescricao();
	}
	
	public GrupoEntity(GrupoDTO dto) {
		this.id = dto.getId();
		this.nome = dto.getNome();
		this.descricao = dto.getDescricao();
		this.dataCriacao = dto.getDataCriacao();
		this.dataAtualizacao = dto.getDataAtualizacao();
	}
	
}
