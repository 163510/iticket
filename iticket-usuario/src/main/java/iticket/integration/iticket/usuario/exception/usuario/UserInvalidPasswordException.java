package iticket.integration.iticket.usuario.exception.usuario;

public class UserInvalidPasswordException extends RuntimeException {

	private static final long serialVersionUID = -6238298552145005459L;

	public UserInvalidPasswordException() {
		super();
	}
	
	public UserInvalidPasswordException(String msg) {
		super(msg);
	}
}
