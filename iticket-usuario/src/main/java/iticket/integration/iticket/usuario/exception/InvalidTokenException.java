package iticket.integration.iticket.usuario.exception;

public class InvalidTokenException extends Exception {

	private static final long serialVersionUID = 5784911675741114467L;

	public InvalidTokenException() {
		super();
	}
	
	public InvalidTokenException(String msg) {
		super(msg);
	}
}
