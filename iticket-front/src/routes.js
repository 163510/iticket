import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Login from "./Login";
import PrivateRoute from "./PrivateRoute";
import Home2 from "./Home2";
import Usuario from "./Usuario";
import Categoria from "./Categoria";
import UsuarioCreate from "./UsuarioCreate";
import Grupo from "./Grupo";
import SubCategoria from "./SubCategoria";
import Chamado from "./Chamado";
import ChamadoDetalhe from "./ChamadoDetalhe";
import Nota from "./Nota";
import AtendenteNota from "./AtendenteNota";
import CategoriaCreate from "./CategoriaCreate";
import SubCategoriaCreate from "./SubCategoriaCreate";
import ChamadoCreate from "./ChamadoCreate";
import RelatorioChamadoCategoria from "./RelatorioChamadoCategoria";
import SedeSetorRelatorio from "./SedeSetorRelatorio";
import SetorCreate from "./SetorCreate";
import Setor from "./Setor";
import Sede from "./Sede";
import SedeCreate from "./SedeCreate";

const Routes = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path="/login" component={Login} />
      <PrivateRoute path="/home" component={Home2} />
      <PrivateRoute path="/usuario" component={Usuario} />
      <PrivateRoute path="/grupo" component={Grupo} />
      <PrivateRoute path="/usuariocreate" component={UsuarioCreate} />
      <PrivateRoute path="/categoria" component={Categoria} />
      <PrivateRoute path="/subcategoria" component={SubCategoria} />
      <PrivateRoute path="/chamados" component={Chamado} />
      <PrivateRoute path="/chamado/:id" component={ChamadoDetalhe} />
      <PrivateRoute path="/notaRelatorio" component={Nota} />
      <PrivateRoute path="/atendenteNota/:id" component={AtendenteNota} />
      <PrivateRoute path="/categoriaCreate" component={CategoriaCreate} />
      <PrivateRoute path="/subCategoriaCreate" component={SubCategoriaCreate} />
      <PrivateRoute path="/chamadoCreate" component={ChamadoCreate} />
      <PrivateRoute
        path="/categoriaRelatorio"
        component={RelatorioChamadoCategoria}
      />
      <PrivateRoute path="/sedeRelatorio" component={SedeSetorRelatorio} />
      <PrivateRoute path="/setorCreate" component={SetorCreate} />
      <PrivateRoute path="/setor" component={Setor} />
      <PrivateRoute path="/sede" component={Sede} />
      <PrivateRoute path="/sedeCreate" component={SedeCreate} />
    </Switch>
  </BrowserRouter>
);

export default Routes;
