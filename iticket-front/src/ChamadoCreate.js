import React, { Component } from "react";
import userService from "./service/UserService";
import "bootstrap/dist/css/bootstrap.min.css";
import Supernav from "./Supernav";
import { Button, Col, Row, Form } from "react-bootstrap";

class ChamadoCreate extends Component {
  state = {
    chamado: {
      titulo: null,
      descricao: null,
      categoria: null,
      categorias: [],
      subcategoria: null,
      subcategorias: [],
      sedes: [],
      sede_id: null,
      setores: [],
      setor_id: null,
    },
  };

  handleChamadoChange(change, e) {
    let chamado = this.state.chamado;
    chamado[change] = e.target.value;
    this.setState({ chamado: chamado });
    console.log(this.state);
  }

  handleChange(change, e) {
    let chamado = this.state.chamado;
    chamado[change] = e.target.value;
    this.setState({ chamado: chamado });
    console.log(this.state);
  }

  handleSedeChange(e) {
    let chamado = this.state.chamado;
    chamado.sede_id = e.target.value;
    this.setState({ chamado: chamado });
  }

  handleSetorChange(e) {
    let chamado = this.state.chamado;
    chamado.setor_id = e.target.value;
    this.setState({ chamado: chamado });
  }

  handleSubCategoriaChange(e) {
    let chamado = this.state.chamado;
    chamado.subcategoria = e.target.value;
    if (e.target.value !== "null") {
      const subcategoriaSelecionada = JSON.parse(e.target.value);
      console.log(subcategoriaSelecionada);
      chamado.descricao = subcategoriaSelecionada.template;
    }
    this.setState({ chamado: chamado });
    console.log(`Hello ${this.state}`);
  }
  handleCategoriaChange(e) {
    let chamado = this.state.chamado;

    if (e.target.value === "null") {
      chamado.categoria = null;
      chamado.subcategorias = [];
    } else {
      chamado.categoria = e.target.value;
      const value = JSON.parse(e.target.value);
      this.findSubCategories(value.id);
    }
    this.setState({ chamado: chamado });
    console.log(this.state);
  }
  async findSubCategories(id) {
    const response = await this.getSubcategories(id);
    let chamado = this.state.chamado;
    chamado.subcategorias = response;
    this.setState({ chamado: chamado });
  }
  async getSubcategories(id) {
    try {
      const userData = userService.getLoggedUser();
      const response = await fetch(
        `http://localhost:3032/subcategoria/categoria/${id}`,
        {
          method: "GET",
          mode: "cors",
          headers: new Headers({
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: userData.token,
          }),
        }
      );
      return await response.json();
    } catch (error) {
      return [];
    }
  }
  onChange(e) {
    const re = /^[0-9\b]+$/;
    console.log(re.test(e.target.value));
    if (e.target.value === "" || re.test(e.target.value)) {
      let subCategoria = this.state.subCategoria;
      subCategoria.tempoAtendimento = e.target.value;
      this.setState({ subCategoria: subCategoria });
    }
  }

  createChamado() {
    try {
      const userData = userService.getLoggedUser();
      fetch("http://localhost:3030/chamado", {
        method: "POST",
        mode: "cors",
        headers: new Headers({
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: userData.token,
        }),
        body: JSON.stringify(
          this.parseCreateChamadoCreateBody(this.state.chamado, userData)
        ),
      });
    } catch (error) {
      console.log(error);
    }
  }
  parseCreateChamadoCreateBody(chamado, userData) {
    let request = {
      clienteId: userData.id,
      titulo: chamado.titulo,
      descricao: chamado.descricao,
      sedeId: chamado.sede_id,
      setorId: chamado.setor_id,
    };
    const subcategoria = JSON.parse(chamado.subcategoria);
    request.subCategoriaId = subcategoria.id;
    console.log(request);
    return request;
  }

  async componentDidMount() {
    const response = await this.getCategories();
    console.log(response);
    const setores = await this.getSetores();
    console.log(setores);
    const sedes = await this.getSedes();
    console.log(sedes);
    let chamado = this.state.chamado;
    chamado.categorias = response;
    chamado.sedes = sedes;
    chamado.setores = setores;
    this.setState({ chamado: chamado });
  }

  async getCategories() {
    try {
      const userData = userService.getLoggedUser();
      const response = await fetch("http://localhost:3032/categoria/all", {
        method: "GET",
        mode: "cors",
        headers: new Headers({
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: userData.token,
        }),
      });
      return await response.json();
    } catch (error) {
      console.log(error);
    }
  }

  async getSedes() {
    try {
      const userData = userService.getLoggedUser();
      const response = await fetch("http://localhost:3033/sede/all", {
        method: "GET",
        mode: "cors",
        headers: new Headers({
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: userData.token,
        }),
      });
      return await response.json();
    } catch (error) {
      console.log(error);
    }
  }

  async getSetores() {
    try {
      const userData = userService.getLoggedUser();
      const response = await fetch("http://localhost:3033/setor/all", {
        method: "GET",
        mode: "cors",
        headers: new Headers({
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: userData.token,
        }),
      });
      return await response.json();
    } catch (error) {
      console.log(error);
    }
  }

  async getSubCategoriesByCategoryId(id) {
    try {
      const userData = userService.getLoggedUser();
      const response = await fetch("http://localhost:3032/categoria/all", {
        method: "GET",
        mode: "cors",
        headers: new Headers({
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: userData.token,
        }),
      });
      return await response.json();
    } catch (error) {
      console.log(error);
    }
  }

  cantSubmit(obj) {
    return (
      obj.nome === null ||
      obj.nome === "" ||
      obj.categoria === null ||
      obj.categoria === "null" ||
      obj.subcategoria === null ||
      obj.subcategoria === "null" ||
      obj.sede_id === null ||
      obj.sede_id === "null" ||
      obj.setor_id === null ||
      obj.setor_id === "null"
    );
  }
  buttonVoltar() {
    this.props.history.push(`/chamados`);
  }

  render() {
    const { chamado } = this.state;
    return (
      <div>
        <div style={{ paddingBottom: "14px" }}>
          <Supernav />
        </div>
        <div
          style={{
            padding: "40px",
            backgroundColor: "#F3F3F3",
          }}
        >
          <Form>
            <Row className="mb-1">
              <Form.Group as={Col}>
                <Form.Label>Título</Form.Label>
                <Form.Control
                  type="text"
                  value={chamado.titulo}
                  placeholder="Título"
                  onChange={this.handleChamadoChange.bind(this, "titulo")}
                />
              </Form.Group>
            </Row>

            <Row className="mb-2">
              <Form.Group as={Col}>
                <Form.Label>Categoria</Form.Label>
                <Form.Select onChange={this.handleCategoriaChange.bind(this)}>
                  <option value="null">...</option>
                  {chamado.categorias.map((categoria) => (
                    <option value={JSON.stringify(categoria)}>
                      {categoria.nome}
                    </option>
                  ))}
                </Form.Select>
              </Form.Group>
              <Form.Group as={Col}>
                <Form.Label>Sub Categoria</Form.Label>
                <Form.Select
                  onChange={this.handleSubCategoriaChange.bind(this)}
                >
                  <option value="null">...</option>
                  {chamado.subcategorias.map((subcategoria) => (
                    <option value={JSON.stringify(subcategoria)}>
                      {subcategoria.nome}
                    </option>
                  ))}
                </Form.Select>
              </Form.Group>
            </Row>

            <Row className="mb-2">
              <Form.Group as={Col}>
                <Form.Label>Sede</Form.Label>
                <Form.Select onChange={this.handleSedeChange.bind(this)}>
                  <option value="null">...</option>
                  {chamado.sedes.map((sede) => (
                    <option value={sede.id}>{sede.nome}</option>
                  ))}
                </Form.Select>
              </Form.Group>
              <Form.Group as={Col}>
                <Form.Label>Setor</Form.Label>
                <Form.Select onChange={this.handleSetorChange.bind(this)}>
                  <option value="null">...</option>
                  {chamado.setores.map((setor) => (
                    <option value={setor.id}>{setor.nome}</option>
                  ))}
                </Form.Select>
              </Form.Group>
            </Row>

            <Row className="mb-2">
              <Form.Group as={Col}>
                <Form.Label>Descrição</Form.Label>
                <Form.Control
                  as="textarea"
                  rows={10}
                  placeholder="Descrição"
                  value={chamado.descricao}
                  onChange={this.handleChamadoChange.bind(this, "descricao")}
                />
              </Form.Group>
            </Row>
            <Button
              style={{ float: "left" }}
              onClick={() => this.buttonVoltar()}
            >
              Voltar
            </Button>
            <Button
              style={{ float: "right" }}
              variant="primary"
              type="submit"
              onClick={() => this.createChamado()}
              disabled={this.cantSubmit(chamado)}
            >
              Cadastrar
            </Button>
          </Form>
        </div>
      </div>
    );
  }
}

export default ChamadoCreate;
