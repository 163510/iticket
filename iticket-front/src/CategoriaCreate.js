import React, { Component } from "react";
import userService from "./service/UserService";
import "bootstrap/dist/css/bootstrap.min.css";
import Supernav from "./Supernav";
import { Button, Col, Row, Form } from "react-bootstrap";

class CategoriaCreate extends Component {
  state = {
    categoria: {
      nome: null,
      descricao: null,
    },
  };

  handleChamadoChange(change, e) {
    let categoria = this.state.categoria;
    categoria[change] = e.target.value;
    this.setState({ categoria: categoria });
  }

  createCategoria() {
    try {
      const userData = userService.getLoggedUser();
      fetch("http://localhost:3032/categoria", {
        method: "POST",
        mode: "cors",
        headers: new Headers({
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: userData.token,
        }),
        body: JSON.stringify(
          this.parseCreateCategoriaBody(this.state.categoria)
        ),
      });
    } catch (error) {
      console.log(error);
    }
  }
  parseCreateCategoriaBody(categoria) {
    return {
      nome: categoria.nome,
      descricao: categoria.descricao,
    };
  }

  canSubmit(categoria) {
    return categoria.nome === null || categoria.nome === "";
  }
  listCategory() {
    this.props.history.push(`/categoria`);
  }
  render() {
    const { categoria } = this.state;
    return (
      <div>
        <div style={{ paddingBottom: "14px" }}>
          <Supernav />
        </div>
        <div
          style={{
            padding: "40px",
            backgroundColor: "#F3F3F3",
          }}
        >
          <Form action="#">
            <Row className="mb-1">
              <Form.Group as={Col}>
                <Form.Label>Título</Form.Label>
                <Form.Control
                  type="text"
                  value={categoria.nome}
                  placeholder="Nome"
                  onChange={this.handleChamadoChange.bind(this, "nome")}
                />
              </Form.Group>
            </Row>
            <Row className="mb-1">
              <Form.Group as={Col}>
                <Form.Label>Descrição</Form.Label>
                <Form.Control
                  as="textarea"
                  rows={10}
                  placeholder="Descrição"
                  value={categoria.descricao}
                  onChange={this.handleChamadoChange.bind(this, "descricao")}
                />
              </Form.Group>
            </Row>
            <Button
              style={{ float: "left" }}
              onClick={() => this.listCategory()}
            >
              Voltar
            </Button>
            <Button
              style={{ float: "right" }}
              variant="primary"
              type="submit"
              onClick={() => this.createCategoria()}
              disabled={this.canSubmit(categoria)}
            >
              Cadastrar
            </Button>
          </Form>
        </div>
      </div>
    );
  }
}

export default CategoriaCreate;
