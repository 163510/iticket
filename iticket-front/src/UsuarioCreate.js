import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import Supernav from "./Supernav";
import { Button, Col, Row, Form } from "react-bootstrap";

class UsuarioCreate extends Component {
  state = {
    users: [],
  };

  render() {
    return (
      <section>
        <div style={{ paddingBottom: "14px" }}>
          <Supernav />
        </div>

        <div
          style={{
            backgroundColor: "lightGrey",
            paddingLeft: "10px",
            paddingRight: "10px",
            paddingBottom: "10px",
          }}
        >
          <Form action="#">
            <Row className="mb-2">
              <Form.Group as={Col}>
                <Form.Label>Nome</Form.Label>
                <Form.Control type="text" placeholder="Ex.: Adamastor José" />
              </Form.Group>

              <Form.Group as={Col}>
                <Form.Label>E-mail</Form.Label>
                <Form.Control
                  type="email"
                  placeholder="Ex.: email@exemplo.com"
                />
              </Form.Group>
            </Row>
            <Row className="mb-2">
              <Form.Group as={Col}>
                <Form.Label>Senha</Form.Label>
                <Form.Control type="password" placeholder="Senha" />
              </Form.Group>

              <Form.Group as={Col}>
                <Form.Label>Confirmar senha</Form.Label>
                <Form.Control type="password" placeholder="Confirmar senha" />
              </Form.Group>
            </Row>

            <Button variant="primary" type="submit">
              Submit
            </Button>
          </Form>
        </div>
      </section>
    );
  }
}

export default UsuarioCreate;
