import React, { Component } from "react";
import userService from "./sevice/UserService";

class Form extends Component {
  state = {
    email: "",
    senha: "",
  };

  handleInputEmailChange = (e) => {
    this.setState({ email: e.target.value });
  };

  handleInputSenhaChange = (e) => {
    this.setState({ senha: e.target.value });
  };

  handleSubmit = async (e) => {
    e.preventDefault();
    console.log("funcionou", this.state);
    const response = userService.login(this.state);
    userService.setLoggedUser(response);
  };

  async login(data) {
    console.log(data);
    const response = await fetch("http://localhost:3031/usuario/login", {
      method: "POST",
      body: JSON.stringify(data),
      mode: "cors",
      headers: new Headers({
        Accept: "application/json",
        "Content-Type": "application/json",
      }),
    });
    return response;
  }

  render() {
    const { name } = this.state;
    return (
      <section>
        <form onSubmit={this.handleSubmit}>
          <label>
            email:
            <input
              onChange={this.handleInputEmailChange}
              type="email"
              placeholder="Email"
            />
          </label>
          <label>
            senha:
            <input
              onChange={this.handleInputSenhaChange}
              type="password"
              placeholder="Senha"
            />
          </label>
          <button type="submit">Enviar</button>
        </form>
        <h3>{name}</h3>
      </section>
    );
  }
}

export default Form;
