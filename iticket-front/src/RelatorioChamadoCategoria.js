import React, { Component } from "react";
import userService from "./service/UserService";
import "bootstrap/dist/css/bootstrap.min.css";
import Supernav from "./Supernav";

class RelatorioChamadoCategoria extends Component {
  state = {
    chamados: [],
  };

  async componentDidMount() {
    let response = await this.getMedia();
    console.log(response);
    this.setState({ chamados: response });
  }

  async getMedia() {
    try {
      const userData = userService.getLoggedUser();
      const response = await fetch(
        "http://localhost:3030/chamados/subcategoria?days=30",
        {
          method: "GET",
          mode: "cors",
          headers: new Headers({
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: userData.token,
          }),
        }
      );
      return await response.json();
    } catch (error) {
      console.log(error);
    }
  }

  getNotaColor(value) {
    if (value >= 7) {
      return "#01D541";
    } else if (value >= 4) {
      return "#CACF05";
    } else {
      return "#FF0000";
    }
  }

  detalheAtendente(id) {
    this.props.history.push(`/atendenteNota/${id}`);
  }

  render() {
    const { chamados } = this.state;
    return (
      <div>
        <div style={{ paddingBottom: "14px" }}>
          <Supernav />
        </div>
        <div>
          <table className="table table-striped">
            <thead>
              <tr>
                <th>id da Subcateogria</th>
                <th>Subcategoria</th>
                <th>categoria</th>
                <th>Quantidade</th>
                <th>Aberto</th>
                <th>Em Atendimento</th>
                <th>Aguardando Serviço Externo</th>
                <th>Finalizado</th>
                <th>Concluido</th>
              </tr>
            </thead>
            <tbody>
              {chamados.map((chamado) => (
                <tr key={chamado.subCategoriaId}>
                  <td>{chamado.subCategoriaId}</td>
                  <td>{chamado.subCategoria}</td>
                  <td>{chamado.categoria}</td>
                  <td>{chamado.quantity}</td>
                  {chamado.chamadosByStatus.map((status) => (
                    <td>{status.quantity}</td>
                  ))}
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default RelatorioChamadoCategoria;
