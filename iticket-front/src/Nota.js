import React, { Component } from "react";
import userService from "./service/UserService";
import "bootstrap/dist/css/bootstrap.min.css";
import Supernav from "./Supernav";

class Nota extends Component {
  state = {
    media: {
      average: (0.0).toFixed(2),
      atendentesAverages: [],
      quantity: 0,
    },
  };

  async componentDidMount() {
    let response = await this.getMedia();
    response.average = response.average.toFixed(2);
    console.log(response);
    this.setState({ media: response });
  }

  async getMedia() {
    try {
      const userData = userService.getLoggedUser();
      const response = await fetch(
        "http://localhost:3030/nota/average?days=30",
        {
          method: "GET",
          mode: "cors",
          headers: new Headers({
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: userData.token,
          }),
        }
      );
      return await response.json();
    } catch (error) {
      console.log(error);
    }
  }

  getNotaColor(value) {
    if (value >= 7) {
      return "#01D541";
    } else if (value >= 4) {
      return "#CACF05";
    } else {
      return "#FF0000";
    }
  }

  detalheAtendente(id) {
    this.props.history.push(`/atendenteNota/${id}`);
  }

  render() {
    const { media } = this.state;
    return (
      <div>
        <div style={{ paddingBottom: "14px" }}>
          <Supernav />
        </div>
        <h1
          style={{
            display: "float",
            textAlign: "Center",
            paddingTop: "70px",
            paddingBottom: "70px",
            fontSize: "100px",
            color: this.getNotaColor(media.average),
          }}
        >
          {media.average}
        </h1>
        <div>
          <table className="table table-striped">
            <thead>
              <tr>
                <th>id</th>
                <th>Nome</th>
                <th>E-mail</th>
                <th>Qnt. Avaliações</th>
                <th>Média</th>
              </tr>
            </thead>
            <tbody>
              {media.atendentesAverages.map((atendente) => (
                <tr
                  key={atendente.id}
                  onClick={() => this.detalheAtendente(atendente.id)}
                >
                  <td>{atendente.id}</td>
                  <td>{atendente.nome}</td>
                  <td>{atendente.email}</td>
                  <td>{atendente.quantity}</td>
                  <td
                    style={{
                      color: this.getNotaColor(atendente.average),
                      fontWeight: "100px",
                    }}
                  >
                    {atendente.average.toFixed(2)}
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default Nota;
