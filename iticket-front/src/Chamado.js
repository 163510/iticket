import React, { Component } from "react";
import userService from "./service/UserService";
import "bootstrap/dist/css/bootstrap.min.css";
import Supernav from "./Supernav";
import { Button } from "react-bootstrap";
import Helmet from "react-helmet";

class Chamado extends Component {
  state = {
    chamados: [],
    selecionado: null,
  };

  async componentDidMount() {
    const response = await this.getChamados();
    this.setState({ chamados: response });
  }

  async getChamados() {
    try {
      const userData = userService.getLoggedUser();
      const response = await fetch("http://localhost:3030/chamados", {
        method: "GET",
        mode: "cors",
        headers: new Headers({
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: userData.token,
        }),
      });
      return await response.json();
    } catch (error) {
      console.log(error);
    }
  }

  abrirChamado(id) {
    this.props.history.push(`/chamadoCreate`);
  }

  updateSelecionado(id) {
    this.props.history.push(`chamado/${id}`);
  }

  render() {
    const { chamados } = this.state;

    return (
	<div style={{ paddingLeft: "10px", paddingRight: "10px" }}>
        <Helmet>
          <title>Chamados - iTicket</title>
        </Helmet>        <div style={{ paddingBottom: "14px" }}>
          <Supernav />
        </div>

        <h1>Chamados</h1>
        <div>
          <Button
            style={{ float: "right" }}
            onClick={() => this.abrirChamado()}
          >
            Abrir Chamado
          </Button>
        </div>
        <div>
          <table className="table table-striped">
            <thead>
              <tr>
                <th>id</th>
                <th>titulo</th>
                <th>status</th>
              </tr>
            </thead>
            <tbody>
              {chamados.map((chamado) => (
                <tr
                  key={chamado.id}
                  onClick={() => this.updateSelecionado(chamado.id)}
                >
                  <td>{chamado.id}</td>
                  <td>{chamado.titulo}</td>
                  <td>{chamado.status}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default Chamado;
