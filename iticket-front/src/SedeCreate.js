import React, { Component } from "react";
import userService from "./service/UserService";
import "bootstrap/dist/css/bootstrap.min.css";
import Supernav from "./Supernav";
import { Button, Col, Row, Form } from "react-bootstrap";

class SedeCreate extends Component {
  state = {
    sede: {
      nome: "",
    },
  };

  handleChange(change, e) {
    let sede = this.state.sede;
    sede[change] = e.target.value;
    this.setState({ sede: sede });
  }

  createSetor() {
    try {
      const userData = userService.getLoggedUser();
      fetch("http://localhost:3033/sede", {
        method: "POST",
        mode: "cors",
        headers: new Headers({
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: userData.token,
        }),
        body: JSON.stringify(this.parseCreateSedeBody(this.state.sede)),
      });
      let sede = this.state.sede;
      sede.nome = "";
      this.setState({ setor: sede });
    } catch (error) {
      console.log(error);
    }
  }
  parseCreateSedeBody(sede) {
    return {
      nome: sede.nome,
    };
  }

  canSubmit(sede) {
    return sede.nome === null || sede.nome === "";
  }
  listSetor() {
    this.props.history.push(`/sede`);
  }
  render() {
    const { sede } = this.state;
    return (
      <div>
        <div style={{ paddingBottom: "14px" }}>
          <Supernav />
        </div>
        <div
          style={{
            padding: "40px",
            backgroundColor: "#F3F3F3",
          }}
        >
          <Form>
            <Row className="mb-1">
              <Form.Group as={Col}>
                <Form.Label>Nome</Form.Label>
                <Form.Control
                  type="text"
                  value={sede.nome}
                  placeholder="Nome"
                  onChange={this.handleChange.bind(this, "nome")}
                />
              </Form.Group>
            </Row>
            <Button style={{ float: "left" }} onClick={() => this.listSetor()}>
              Voltar
            </Button>
            <Button
              style={{ float: "right" }}
              variant="primary"
              type="submit"
              onClick={() => this.createSetor()}
              disabled={this.canSubmit(sede)}
            >
              Cadastrar
            </Button>
          </Form>
        </div>
      </div>
    );
  }
}

export default SedeCreate;
