import React, { Component } from "react";
import userService from "./service/UserService";
import "bootstrap/dist/css/bootstrap.min.css";
import Supernav from "./Supernav";
import { Button } from "react-bootstrap";
import Helmet from "react-helmet";

class Categoria extends Component {
  state = {
    categorias: [],
  };

  async componentDidMount() {
    const response = await this.getCategories();
    this.setState({ categorias: response });
  }

  async getCategories() {
    try {
      const userData = userService.getLoggedUser();
      const response = await fetch("http://localhost:3032/categoria/all", {
        method: "GET",
        mode: "cors",
        headers: new Headers({
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: userData.token,
        }),
      });
      return await response.json();
    } catch (error) {
      console.log(error);
    }
  }

  updateCategoria(user) {}

  createCategory() {
    this.props.history.push(`/categoriaCreate`);
  }

  render() {
    const { categorias } = this.state;

    return (
      <div style={{ paddingLeft: "10px", paddingRight: "10px" }}>
        <Helmet>
          <title>Categorias - iTicket</title>
        </Helmet>
        <div style={{ paddingBottom: "14px" }}>
          <Supernav />
        </div>
        <div>
          <h1>Categorias</h1>
        </div>

        <div>
          <table className="table table-striped">
            <thead>
              <tr>
                <th>id</th>
                <th>nome</th>
                <th>descrição</th>
              </tr>
            </thead>
            <tbody>
              {categorias.map((categoria) => (
                <tr key={categoria.id}>
                  <td>{categoria.id}</td>
                  <td>{categoria.nome}</td>
                  <td>{categoria.descricao}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
        <div>
          <Button
            style={{ float: "right" }}
            onClick={() => this.createCategory()}
          >
            Adicionar
          </Button>
        </div>
      </div>
    );
  }
}

export default Categoria;
