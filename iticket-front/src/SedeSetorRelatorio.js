import React, { Component } from "react";
import userService from "./service/UserService";
import "bootstrap/dist/css/bootstrap.min.css";
import Supernav from "./Supernav";

class SedeSetorRelatorio extends Component {
  state = {
    response: {
      chamados: [],
      total: 0,
    },
  };

  async componentDidMount() {
    let response = await this.getMedia();
    console.log(response);
    this.setState({ response: response });
  }

  async getMedia() {
    try {
      const userData = userService.getLoggedUser();
      const response = await fetch(
        "http://localhost:3030/chamados/sede/setor?days=30",
        {
          method: "GET",
          mode: "cors",
          headers: new Headers({
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: userData.token,
          }),
        }
      );
      return await response.json();
    } catch (error) {
      console.log(error);
    }
  }

  getNotaColor(value) {
    if (value >= 7) {
      return "#01D541";
    } else if (value >= 4) {
      return "#CACF05";
    } else {
      return "#FF0000";
    }
  }

  detalheAtendente(id) {
    this.props.history.push(`/atendenteNota/${id}`);
  }

  render() {
    const { response } = this.state;
    return (
      <div>
        <div style={{ paddingBottom: "14px" }}>
          <Supernav />
        </div>
        <div>
          <table className="table table-striped">
            <thead>
              <tr>
                <th>Sede</th>
                <th>Setor</th>
                <th>Chamados da Sede</th>
                <th>Chamados do Setor</th>
                <th>Porc. Sede dos Chamados</th>
                <th>Porc. Setor dos Chamados</th>
              </tr>
            </thead>
            <tbody>
              {response.chamados.map((chamado) => (
                <tr>
                  <td>{chamado.sede}</td>
                  <td>{chamado.setor}</td>
                  <td>{chamado.sedeTotalChamados}</td>
                  <td>{chamado.setorTotalChamados}</td>
                  <td>{chamado.porcentagemSedeDeChamados}%</td>
                  <td>{chamado.porcentagemSetorDeChamados}%</td>
                </tr>
              ))}

              <tr>
                <td>Total de chamados:</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>{response.total}</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default SedeSetorRelatorio;
