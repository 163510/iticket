import React, { Component } from "react";
import userService from "./service/UserService";
import "./signin.css";
import Helmet from "react-helmet";
import logo from './iticket.png';

class Login extends Component {
  state = {
    email: "",
    senha: "",
  };

  handleInputEmailChange = (e) => {
    this.setState({ email: e.target.value });
  };

  handleInputSenhaChange = (e) => {
    this.setState({ senha: e.target.value });
  };

  handleSubmit = async (e) => {
    e.preventDefault();
    console.log("funcionou", this.state);
    const response = await userService.login(this.state);
    const status = response.status;
    if (status === 200) {
      const body = await response.json();
      userService.setLoggedUser(body);
      this.props.history.push("/home");
    }
  };

  async login(data) {
    console.log(data);
    const response = await fetch("http://localhost:3031/usuario/login", {
      method: "POST",
      body: JSON.stringify(data),
      mode: "cors",
      headers: new Headers({
        Accept: "application/json",
        "Content-Type": "application/json",
      }),
    });
    return response;
  }

  render() {
    userService.logout();
    return (
      <section>
        <Helmet>
          <title>Login - iTicket</title>
        </Helmet>
        <body class="text-center">
          <form onSubmit={this.handleSubmit} class="form-signin">
            <img
              class="mb-4"
              src={logo}
              alt=""
              width="72"
              height="72"
            />
            <h1 class="h3 mb-3 font-weight-normal">iTicket</h1>
            <label for="inputEmail" class="sr-only">
              Endereço de email
            </label>
            <input
              onChange={this.handleInputEmailChange}
              type="email"
              id="inputEmail"
              class="form-control"
              placeholder="Seu email"
              required
              autofocus
            />
            <label for="inputPassword" class="sr-only">
              Senha
            </label>
            <input
              onChange={this.handleInputSenhaChange}
              type="password"
              id="inputPassword"
              class="form-control"
              placeholder="Senha"
              required
            />
            <button class="btn btn-lg btn-primary btn-block" type="submit">
              Login
            </button>
            <p class="mt-5 mb-3 text-muted">
              &copy; 2021 <br></br> Danrlei Montagna <br></br> Leonardo Zanoni
              Bertuzzi
            </p>
          </form>
        </body>
      </section>
    );
  }
}

export default Login;
