import React, { Component } from "react";
import userService from "./service/UserService";
import "bootstrap/dist/css/bootstrap.min.css";
import Supernav from "./Supernav";
import { Button, Col, Row, Form } from "react-bootstrap";
import Helmet from "react-helmet";

class ChamadoDetalhe extends Component {
  state = {
    chamado: null,
    mensagens: null,
    newMessage: null,
    nota: null,
    notaMsg: null,
    atendentes: [],
    sedes: [],
    setores: [],
    isAtendente: false,
  };

  async componentDidMount() {
    const chamado = await this.getChamado();
    console.log(chamado);
    this.setState({ chamado: chamado });
    const mensagens = await this.getMensagens();
    this.setState({ mensagens: mensagens });
    const atendentes = await this.getAtendentes();
    console.log(atendentes);
    this.setState({ atendentes: atendentes });
    const sedes = await this.getSedes();
    console.log(sedes);
    this.setState({ sedes: sedes });
    const setores = await this.getSetores();
    console.log(setores);
    this.setState({ setores: setores });
    const isAtendente = this.isAtendente();
    this.setState({ isAtendente: isAtendente });
  }

  async getSetores() {
    try {
      const userData = userService.getLoggedUser();
      const response = await fetch("http://localhost:3033/setor/all", {
        method: "GET",
        mode: "cors",
        headers: new Headers({
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: userData.token,
        }),
      });
      return await response.json();
    } catch (error) {
      console.log(error);
    }
  }

  async getSedes() {
    try {
      const userData = userService.getLoggedUser();
      const response = await fetch("http://localhost:3033/sede/all", {
        method: "GET",
        mode: "cors",
        headers: new Headers({
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: userData.token,
        }),
      });
      return await response.json();
    } catch (error) {
      console.log(error);
    }
  }

  async getAtendentes() {
    try {
      const userData = userService.getLoggedUser();

      const response = await fetch(`http://localhost:3031/usuario/atendentes`, {
        method: "GET",
        mode: "cors",
        headers: new Headers({
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: userData.token,
        }),
      });
      return await response.json();
    } catch (error) {
      console.log(error);
    }
  }

  async getMensagens() {
    try {
      const userData = userService.getLoggedUser();
      const chamadoId = this.props.match.params.id;
      const response = await fetch(
        `http://localhost:3030/mensagem/chamado?chamado_id=${chamadoId}`,
        {
          method: "GET",
          mode: "cors",
          headers: new Headers({
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: userData.token,
          }),
        }
      );
      return await response.json();
    } catch (error) {
      console.log(error);
    }
  }

  async getChamado() {
    try {
      const userData = userService.getLoggedUser();
      const chamadoId = this.props.match.params.id;

      const response = await fetch(
        `http://localhost:3030/chamado/${chamadoId}`,
        {
          method: "GET",
          mode: "cors",
          headers: new Headers({
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: userData.token,
          }),
        }
      );
      return await response.json();
    } catch (error) {
      console.log(error);
    }
  }

  updateSelecionado(id) {
    this.props.history.push(`chamado/${id}`);
  }

  isReady(obj) {
    return obj != null;
  }

  isFinished(chamado) {
    return chamado.status === "Finalizado" || chamado.status === "Concluido";
  }

  isFinalizado(chamado) {
    return chamado.status === "Finalizado";
  }

  getBackGroundColor(status) {
    if (status === "Concluido") {
      return "lightBlue";
    }
    return "lightYellow";
  }
  handleChamadoChange(change, e) {
    let chamado = this.state.chamado;
    chamado[change] = e.target.value;
    this.setState({ chamado: chamado });
  }

  handleMensagemContent(e) {
    let value = e.target.value;
    this.setState({ newMessage: value });
  }

  handleNotaChange(e) {
    console.log(e.target.value);
    this.setState({ nota: e.target.value });
  }

  handleNotaComent(e) {
    this.setState({ notaMsg: e.target.value });
  }

  async sendNota() {
    const userData = userService.getLoggedUser();

    if (this.state.nota != null) {
      await fetch(`http://localhost:3030/nota`, {
        method: "POST",
        mode: "cors",
        headers: new Headers({
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: userData.token,
        }),
        body: JSON.stringify(
          this.parseNotaBody(
            this.state.chamado,
            userData.id,
            this.state.nota,
            this.state.notaMsg
          )
        ),
      });
    }

    const chamado = await this.getChamado();
    console.log(chamado);
    this.setState({ chamado: chamado, nota: null });
  }

  parseNotaBody(chamado, userId, nota, msg) {
    return {
      usuario_id: userId,
      atendente_id: chamado.atendente.id,
      chamado_id: chamado.id,
      nota: nota,
      comentario: msg,
    };
  }

  handleStatusChange(e) {
    let chamado = this.state.chamado;
    this.updateStatusBack(e.target.value, chamado);
  }

  handleSedeChange(e) {
    let chamado = this.state.chamado;
    this.updateSedeBack(e.target.value, chamado);
  }
  handleSetorChange(e) {
    let chamado = this.state.chamado;
    this.updateSetorBack(e.target.value, chamado.id);
  }
  handleAtendenteChange(e) {
    let chamado = this.state.chamado;

    if (e.target.value !== "...") {
      this.updateAtendenteBack(e.target.value, chamado.id);
    }
  }

  async updateAtendenteBack(atendenteId, chamadoId) {
    const userData = userService.getLoggedUser();
    await fetch(
      `http://localhost:3030/chamado/${chamadoId}/update/atendente/${atendenteId}`,
      {
        method: "PUT",
        mode: "cors",
        headers: new Headers({
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: userData.token,
        }),
      }
    );

    const chamado = await this.getChamado();
    console.log(chamado);
    this.setState({ chamado: chamado });
  }

  async updateSetorBack(setorId, chamadoId) {
    const userData = userService.getLoggedUser();
    await fetch(
      `http://localhost:3030/chamado/${chamadoId}/update/setor/${setorId}`,
      {
        method: "PUT",
        mode: "cors",
        headers: new Headers({
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: userData.token,
        }),
      }
    );

    const chamado = await this.getChamado();
    console.log(chamado);
    this.setState({ chamado: chamado });
  }

  async updateSedeBack(sedeId, chm) {
    const userData = userService.getLoggedUser();
    await fetch(
      `http://localhost:3030/chamado/${chm.id}/update/sede/${sedeId}`,
      {
        method: "PUT",
        mode: "cors",
        headers: new Headers({
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: userData.token,
        }),
      }
    );

    const chamado = await this.getChamado();
    console.log(chamado);
    this.setState({ chamado: chamado });
  }

  async updateStatusBack(status, chm) {
    const userData = userService.getLoggedUser();
    await fetch(`http://localhost:3030/chamado/update/status`, {
      method: "PUT",
      mode: "cors",
      headers: new Headers({
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: userData.token,
      }),
      body: JSON.stringify(this.parseUpdateStatusBody(status, chm.id)),
    });

    const chamado = await this.getChamado();
    console.log(chamado);
    this.setState({ chamado: chamado });
  }
  parseUpdateStatusBody(status, id) {
    return {
      id: id,
      status: status,
    };
  }
  updateChamado() {
    const userData = userService.getLoggedUser();

    fetch(`http://localhost:3030/chamado/update/${this.state.chamado.id}`, {
      method: "PUT",
      mode: "cors",
      headers: new Headers({
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: userData.token,
      }),
      body: JSON.stringify(this.parseUpdateBody(this.state.chamado)),
    });
  }

  async addMessage() {
    const userData = userService.getLoggedUser();

    await fetch(`http://localhost:3030/mensagem`, {
      method: "POST",
      mode: "cors",
      headers: new Headers({
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: userData.token,
      }),
      body: JSON.stringify(
        this.parseNewMessageBody(
          this.state.chamado.id,
          this.state.newMessage,
          userData.id
        )
      ),
    });

    const mensagens = await this.getMensagens();
    this.setState({ mensagens: mensagens, newMessage: "" });
  }

  parseNewMessageBody(chamadoId, newMessage, userId) {
    return {
      usuarioId: userId,
      chamadoId: chamadoId,
      conteudo: newMessage,
    };
  }

  translateStatus(status) {
    switch (status) {
      case "Aberto":
        return "OPEN";
      case "Em Atendimento":
        return "WORKING_IN_PROGRESS";
      case "Aguardando Serviço Externo":
        return "WAITING_EXTERNAL_SUPPORT";
      case "Finalizado":
        return "FINISHED";
      case "Concluido":
        return "CONCLUDED";
      default:
        return "OPEN";
    }
  }

  getChamadoSede(sede) {
    console.log(sede);
    if (sede) {
      return sede.id;
    }
    return "...";
  }

  parseUpdateBody(chamado) {
    return {
      subCategoriaId: chamado.subCategoria.id,
      clienteId: chamado.cliente.id,
      atendenteId: chamado.atendente.id,
      titulo: chamado.titulo,
      descricao: chamado.descricao,
    };
  }

  isAtendente() {
    const user = userService.getLoggedUser();
    let isAtendente = false;
    user.roles.forEach((role) => {
      if (role === "ATENDENTE") {
        isAtendente = true;
      }
    });
    return isAtendente;
  }

  render() {
    const { chamado } = this.state;
    const { mensagens } = this.state;
    const { atendentes } = this.state;
    const { sedes } = this.state;
    const { setores } = this.state;
    const { isAtendente } = this.state;
    const { newMessage } = this.state;
    return (
      <section>
        <Helmet>
          <title>Chamados - iTicket</title>
        </Helmet>
        <div style={{ paddingBottom: "14px" }}>
          <Supernav />
        </div>
        <div
          style={{
            backgroundColor: "#F3F3F3",
            paddingLeft: "1%",
            paddingRight: "1%",
            paddingBottom: "1%",
          }}
        >
          {this.isReady(chamado) ? (
            <Form>
              <Row className="mb-3">
                <Form.Group as={Col}>
                  <Form.Label>Id</Form.Label>
                  <Form.Control
                    type="text"
                    value={chamado.id}
                    disabled={true}
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Label>Usuario</Form.Label>
                  <Form.Control
                    type="text"
                    value={chamado.cliente.nome}
                    disabled={true}
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Label>Atendente</Form.Label>
                  {console.log(chamado.atendente.nome)}
                  <Form.Select
                    value={chamado.atendente ? chamado.atendente.id : "..."}
                    onChange={this.handleAtendenteChange.bind(this)}
                    disabled={isAtendente ? false : true}
                  >
                    <option value="...">...</option>
                    {atendentes.map((atendente) => (
                      <option value={atendente.id}>{atendente.nome}</option>
                    ))}
                  </Form.Select>
                </Form.Group>
              </Row>
              <Row className="mb-3">
                <Form.Group as={Col}>
                  <Form.Label>Categoria</Form.Label>
                  <Form.Control
                    type="text"
                    value={chamado.subCategoria.categoria}
                    disabled={true}
                  />
                </Form.Group>

                <Form.Group as={Col}>
                  <Form.Label>SubCategoria</Form.Label>
                  <Form.Control
                    type="text"
                    value={chamado.subCategoria.subCategoria}
                    disabled={true}
                  />
                </Form.Group>

                <Form.Group as={Col}>
                  <Form.Label>Status</Form.Label>
                  <Form.Select
                    defaultValue={this.translateStatus(chamado.status)}
                    onChange={this.handleStatusChange.bind(this)}
                    disabled={isAtendente ? false : true}
                  >
                    <option value="OPEN">Aberto</option>
                    <option value="WORKING_IN_PROGRESS">Em Atendimento</option>
                    <option value="WAITING_EXTERNAL_SUPPORT">
                      Aguardando Serviço Externo
                    </option>
                    <option value="FINISHED">Finalizado</option>
                    <option value="CONCLUDED">Concluido</option>
                  </Form.Select>
                </Form.Group>
              </Row>

              <Row className="mb-2">
                <Form.Group as={Col}>
                  <Form.Label>Sede</Form.Label>
                  <Form.Select
                    value={chamado.sede.id}
                    onChange={this.handleSedeChange.bind(this)}
                    disabled={isAtendente ? false : true}
                  >
                    {sedes.map((sede) => (
                      <option value={sede.id}>{sede.nome}</option>
                    ))}
                  </Form.Select>
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Label>Setor</Form.Label>
                  <Form.Select
                    value={chamado.setor.id}
                    onChange={this.handleSetorChange.bind(this)}
                    disabled={isAtendente ? false : true}
                  >
                    {setores.map((setor) => (
                      <option value={setor.id}>{setor.nome}</option>
                    ))}
                  </Form.Select>
                </Form.Group>
              </Row>

              <Row className="mb-2">
                <Form.Group as={Col}>
                  <Form.Label>Titulo</Form.Label>
                  <Form.Control
                    type="text"
                    value={chamado.titulo}
                    disabled={true}
                  />
                </Form.Group>
              </Row>

              <Row className="mb-2">
                <Form.Group as={Col}>
                  <Form.Label>Descrição</Form.Label>
                  <Form.Control
                    as="textarea"
                    rows={10}
                    value={chamado.descricao}
                    disabled={true}
                  />
                </Form.Group>
              </Row>

              <Row className="mb-3">
                <Form.Group as={Col}>
                  <Form.Label>Data de Criação</Form.Label>
                  <Form.Control
                    type="text"
                    value={chamado.dataCriacao}
                    disabled={true}
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Label>Data de Atualização</Form.Label>
                  <Form.Control
                    type="text"
                    value={chamado.dataAtualizacao}
                    disabled={true}
                  />
                </Form.Group>
                {this.isFinished(chamado) ? (
                  <Form.Group as={Col}>
                    <Form.Label>Data de Finalização</Form.Label>
                    <Form.Control
                      type="text"
                      value={chamado.dataFinalizacao}
                      disabled={true}
                    />
                  </Form.Group>
                ) : (
                  <Form.Group as={Col}>
                    <Form.Label>Data de Finalização</Form.Label>
                    <Form.Control type="text" disabled={true} />
                  </Form.Group>
                )}
              </Row>
            </Form>
          ) : (
            <div></div>
          )}

          {this.isReady(chamado) && this.isFinalizado(chamado) ? (
            <div>
              <Form>
                <Row className="mb-1">
                  <Form.Label>Adicionar uma Nota: </Form.Label>
                  <div onChange={this.handleNotaChange.bind(this)}>
                    <input type="radio" id="nota1" name="nota" value="1" />
                    <label for="nota1">1</label>
                    <input type="radio" id="nota2" name="nota" value="2" />
                    <label for="nota2">2</label>
                    <input type="radio" id="nota3" name="nota" value="3" />
                    <label for="nota3">3</label>
                    <input type="radio" id="nota4" name="nota" value="4" />
                    <label for="nota4">4</label>
                    <input type="radio" id="nota5" name="nota" value="5" />
                    <label for="nota5">5</label>
                    <input type="radio" id="nota6" name="nota" value="6" />
                    <label for="nota6">6</label>
                    <input type="radio" id="nota7" name="nota" value="7" />
                    <label for="nota7">7</label>
                    <input type="radio" id="nota8" name="nota" value="8" />
                    <label for="nota8">8</label>
                    <input type="radio" id="nota9" name="nota" value="9" />
                    <label for="nota9">9</label>
                    <input type="radio" id="nota10" name="nota" value="10" />
                    <label for="nota10">10</label>
                  </div>
                  <Form.Label>Comentario:</Form.Label>
                  <Form.Control
                    as="textarea"
                    rows={5}
                    onChange={this.handleNotaComent.bind(this)}
                  />
                </Row>
                <Button onClick={() => this.sendNota()}>Enviar Nota</Button>
              </Form>
            </div>
          ) : (
            <div></div>
          )}

          <div
            style={{
              backgroundColor: "#F9F8F7",
              paddingLeft: "0.8%",
              paddingRight: "1%",
              paddingBottom: "1%",
              paddingTop: "10px",
            }}
          >
            {this.isReady(chamado) && this.isFinalizado(chamado) ? (
              <div></div>
            ) : (
              <div>
                <Row className="mb-1">
                  <Form.Label>Adicionar um comentario: </Form.Label>
                  <Form.Control
                    as="textarea"
                    rows={5}
                    value={newMessage}
                    onChange={this.handleMensagemContent.bind(this)}
                  />
                </Row>
                <Button
                  variant="primary"
                  type="submit"
                  onClick={() => this.addMessage()}
                >
                  Salvar
                </Button>
              </div>
            )}

            {this.isReady(mensagens) ? (
              <div>
                {mensagens.map((mensagem) => (
                  <Form>
                    <Row className="mb-1">
                      <Form.Label>{mensagem.username}:</Form.Label>
                      <Form.Control
                        as="textarea"
                        rows={5}
                        value={mensagem.conteudo}
                        disabled={true}
                      />
                    </Row>
                  </Form>
                ))}
              </div>
            ) : (
              <div></div>
            )}
          </div>
        </div>
      </section>
    );
  }
}

export default ChamadoDetalhe;
