import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Navbar, Nav, NavDropdown } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faHouse,
  faUsers,
  faList,
  faListUl,
  faLaptopMedical,
  faArrowRightFromBracket,
  faArrowDownShortWide,
  faChartLine,
  faArrowDown19,
  faArrowDownAZ,
  faMapLocation,
  faBuilding,
  faNetworkWired
} from "@fortawesome/free-solid-svg-icons";

class Supernav extends Component {
  render() {
    return (
      <Navbar
        style={{ height: "54px", paddingLeft: "10px" }}
        fixed="top"
        bg="dark"
        variant="dark"
      >
        <Navbar.Brand href="./home">iTicket</Navbar.Brand>
        <Nav className="m-auto">
          <Nav.Link href="./home">
            <FontAwesomeIcon icon={faHouse} /> Página inicial
          </Nav.Link>
          <Nav.Link href="./usuario">
            <FontAwesomeIcon icon={faUsers} /> Usuários
          </Nav.Link>
          <NavDropdown
            title={
              <span>
                <FontAwesomeIcon icon={faMapLocation} /> Local
              </span>
            }
          >
            <NavDropdown.Item href="./sede">
              <FontAwesomeIcon icon={faBuilding} /> Sede
            </NavDropdown.Item>
            <NavDropdown.Item href="./setor">
              <FontAwesomeIcon icon={faNetworkWired} /> Setor
            </NavDropdown.Item>

          </NavDropdown>
          <NavDropdown
            title={
              <span>
                <FontAwesomeIcon icon={faArrowDownShortWide} /> Categorias
              </span>
            }
          >
            <NavDropdown.Item href="./categoria">
              <FontAwesomeIcon icon={faList} /> Categorias
            </NavDropdown.Item>
            <NavDropdown.Item href="./subCategoria">
              <FontAwesomeIcon icon={faListUl} /> Subcategorias
            </NavDropdown.Item>
          </NavDropdown>
          <Nav.Link href="./chamados">
            <FontAwesomeIcon icon={faLaptopMedical} /> Chamados
          </Nav.Link>
          <NavDropdown
            title={
              <span>
                <FontAwesomeIcon icon={faChartLine} /> Relatórios
              </span>
            }
          >
            <NavDropdown.Item href="./notaRelatorio">
              <FontAwesomeIcon icon={faArrowDown19} /> Notas
            </NavDropdown.Item>
            <NavDropdown.Item href="./categoriaRelatorio">
              <FontAwesomeIcon icon={faArrowDownAZ} /> Categorias
            </NavDropdown.Item>
            <NavDropdown.Item href="./sedeRelatorio">
              <FontAwesomeIcon icon={faArrowDownShortWide} /> Sedes
            </NavDropdown.Item>
          </NavDropdown>
          <Nav.Link href="./login">
            <FontAwesomeIcon icon={faArrowRightFromBracket} /> Sair
          </Nav.Link>
        </Nav>
      </Navbar>
    );
  }
}

export default Supernav;
