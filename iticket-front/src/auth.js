import userService from "./service/UserService";

const isAuthenticated = () => {
  const data = userService.getLoggedUser();
  console.log("User data", data);
  if (!data) {
    return false;
  }

  if (!data.token) {
    return false;
  }

  return true;
};

export default isAuthenticated;
