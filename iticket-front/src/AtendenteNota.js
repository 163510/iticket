import React, { Component } from "react";
import userService from "./service/UserService";
import "bootstrap/dist/css/bootstrap.min.css";
import Supernav from "./Supernav";

class AtendenteNota extends Component {
  state = {
    atendente: {
      average: (0.0).toFixed(2),
      notas: [],
    },
  };

  async componentDidMount() {
    let response = await this.getAtendenteNotas();
    response.average = response.average.toFixed(2);
    console.log(response);
    this.setState({ atendente: response });
  }

  async getAtendenteNotas() {
    try {
      const userData = userService.getLoggedUser();
      const id = this.props.match.params.id;
      const response = await fetch(
        `http://localhost:3030/nota/average/${id}?days=30`,
        {
          method: "GET",
          mode: "cors",
          headers: new Headers({
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: userData.token,
          }),
        }
      );
      return await response.json();
    } catch (error) {
      console.log(error);
    }
  }

  getNotaColor(value) {
    if (value >= 7) {
      return "#01D541";
    } else if (value >= 4) {
      return "#CACF05";
    } else {
      return "#FF0000";
    }
  }

  detalheAtendente(id) {
    this.props.history.push(`/atendenteNota/${id}`);
  }

  render() {
    const { atendente } = this.state;
    return (
      <div>
        <div style={{ paddingBottom: "14px" }}>
          <Supernav />
        </div>
        <h1
          style={{
            display: "float",
            textAlign: "Center",
            paddingTop: "70px",
            paddingBottom: "70px",
            fontSize: "100px",
            color: this.getNotaColor(atendente.average),
          }}
        >
          {atendente.average}
        </h1>
        <div>
          <table className="table table-striped">
            <thead>
              <tr>
                <th>id</th>
                <th>Atendente</th>
                <th>Chamado</th>
                <th>Nome</th>
                <th>E-mail</th>
                <th>Data</th>
                <th>Nota</th>
              </tr>
            </thead>
            <tbody>
              {atendente.notas.map((nota) => (
                <tr key={nota.id}>
                  <td>{nota.id}</td>
                  <td>{atendente.id}</td>
                  <td>{nota.chamado_id}</td>
                  <td>{atendente.nome}</td>
                  <td>{atendente.email}</td>
                  <td>{nota.data_criacao}</td>
                  <td
                    style={{
                      color: this.getNotaColor(nota.nota),
                      fontWeight: "100px",
                    }}
                  >
                    {nota.nota.toFixed(2)}
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default AtendenteNota;
