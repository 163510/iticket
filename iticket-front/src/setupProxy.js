// const proxy = require("http-proxy-middleware");

// module.exports = function (app) {
//   app.use(
//     "/usuario/login",
//     proxy({
//       target: "http://localhost:3031",
//       changeOrigin: true,
//     })
//   );
// };

const { createProxyMiddleware } = require("http-proxy-middleware");

module.exports = function (app) {
  app.use(
    "http://localhost:3031",
    createProxyMiddleware({
      target: "/usuario/login",
      changeOrigin: true,
    })
  );

  app.use(
    "http://localhost:3031",
    createProxyMiddleware({
      target: "/usuario/all",
      changeOrigin: true,
    })
  );
};
