import React, { Component } from "react";
import userService from "./service/UserService";
import "bootstrap/dist/css/bootstrap.min.css";
import Supernav from "./Supernav";
import { Button, Col, Row, Form } from "react-bootstrap";

class SubCategoriaCreate extends Component {
  state = {
    subCategoria: {
      nome: null,
      descricao: null,
      template: null,
      categoria: null,
      categorias: [],
      tempoAtendimento: null,
    },
  };

  handleSubCategoriaChange(change, e) {
    let subCategoria = this.state.subCategoria;
    subCategoria[change] = e.target.value;
    this.setState({ subCategoria: subCategoria });
    console.log(this.state);
  }

  onChange(e) {
    const re = /^[0-9\b]+$/;
    console.log(re.test(e.target.value));
    if (e.target.value === "" || re.test(e.target.value)) {
      let subCategoria = this.state.subCategoria;
      subCategoria.tempoAtendimento = e.target.value;
      this.setState({ subCategoria: subCategoria });
    }
  }

  async componentDidMount() {
    const response = await this.getCategories();
    console.log(response);
    let subCategoria = this.state.subCategoria;
    subCategoria.categorias = response;
    this.setState({ subCategoria: subCategoria });
  }

  async getCategories() {
    try {
      const userData = userService.getLoggedUser();
      const response = await fetch("http://localhost:3032/categoria/all", {
        method: "GET",
        mode: "cors",
        headers: new Headers({
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: userData.token,
        }),
      });
      return await response.json();
    } catch (error) {
      console.log(error);
    }

    let subCategoria = this.state.subCategoria;
    subCategoria.nome = null;
    subCategoria.descricao = null;
    subCategoria.template = null;
    this.setState({ subCategoria: subCategoria });
  }

  cantSubmit(obj) {
    return (
      obj.nome === null ||
      obj.nome === "" ||
      obj.categoria === null ||
      obj.categoria === "null"
    );
  }
  buttonVoltar() {
    this.props.history.push(`/subcategoria`);
  }

  createSubCategoria() {
    try {
      const userData = userService.getLoggedUser();
      fetch("http://localhost:3032/subcategoria", {
        method: "POST",
        mode: "cors",
        headers: new Headers({
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: userData.token,
        }),
        body: JSON.stringify(
          this.parseCreateSubCategoriaBody(this.state.subCategoria)
        ),
      });
    } catch (error) {
      console.log(error);
    }
  }

  parseCreateSubCategoriaBody(subcategoria) {
    let request = {
      nome: subcategoria.nome,
      descricao: subcategoria.descricao,
      template: subcategoria.template,
      tempoPrevistoAtendimento: subcategoria.tempoAtendimento,
      categoria: subcategoria.categoria,
    };

    request.categoria = JSON.parse(subcategoria.categoria);

    return request;
  }

  render() {
    const { subCategoria } = this.state;
    return (
      <div>
        <div style={{ paddingBottom: "14px" }}>
          <Supernav />
        </div>
        <div
          style={{
            padding: "40px",
            backgroundColor: "#F3F3F3",
          }}
        >
          <Form action="#">
            <Row className="mb-1">
              <Form.Group as={Col}>
                <Form.Label>Título</Form.Label>
                <Form.Control
                  type="text"
                  value={subCategoria.nome}
                  placeholder="Nome"
                  onChange={this.handleSubCategoriaChange.bind(this, "nome")}
                />
              </Form.Group>
            </Row>
            <Row className="mb-2">
              <Form.Group as={Col}>
                <Form.Label>Descrição</Form.Label>
                <Form.Control
                  as="textarea"
                  rows={3}
                  placeholder="Descrição"
                  value={subCategoria.descricao}
                  onChange={this.handleSubCategoriaChange.bind(
                    this,
                    "descricao"
                  )}
                />
              </Form.Group>
            </Row>
            <Row className="mb-1">
              <Form.Group as={Col}>
                <Form.Label>Categoria</Form.Label>
                <Form.Select
                  onChange={this.handleSubCategoriaChange.bind(
                    this,
                    "categoria"
                  )}
                >
                  <option value="null">...</option>
                  {subCategoria.categorias.map((categoria) => (
                    <option value={JSON.stringify(categoria)}>
                      {categoria.nome}
                    </option>
                  ))}
                </Form.Select>
              </Form.Group>
              <Form.Group as={Col}>
                <Form.Label>
                  Tempo Previsto para Atendimento (Em minutos)
                </Form.Label>
                <Form.Control
                  type="text"
                  value={subCategoria.tempoAtendimento}
                  placeholder="Tempo Previsto para Atendimento"
                  onChange={this.onChange.bind(this)}
                />
              </Form.Group>
            </Row>
            <Row className="mb-1">
              <Form.Group as={Col}>
                <Form.Label>Template</Form.Label>
                <Form.Control
                  as="textarea"
                  rows={10}
                  placeholder="Template"
                  value={subCategoria.template}
                  onChange={this.handleSubCategoriaChange.bind(
                    this,
                    "template"
                  )}
                />
              </Form.Group>
            </Row>

            <Button
              style={{ float: "left" }}
              onClick={() => this.buttonVoltar()}
            >
              Voltar
            </Button>
            <Button
              style={{ float: "right" }}
              variant="primary"
              type="submit"
              onClick={() => this.createSubCategoria()}
              disabled={this.cantSubmit(subCategoria)}
            >
              Cadastrar
            </Button>
          </Form>
        </div>
      </div>
    );
  }
}

export default SubCategoriaCreate;
