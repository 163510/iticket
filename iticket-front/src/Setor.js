import React, { Component } from "react";
import userService from "./service/UserService";
import "bootstrap/dist/css/bootstrap.min.css";
import Supernav from "./Supernav";
import { Button } from "react-bootstrap";
import Helmet from "react-helmet";

class Setor extends Component {
  state = {
    setores: [],
  };

  async componentDidMount() {
    const response = await this.getSetores();
    this.setState({ setores: response });
  }

  async getSetores() {
    try {
      const userData = userService.getLoggedUser();
      const response = await fetch("http://localhost:3033/setor/all", {
        method: "GET",
        mode: "cors",
        headers: new Headers({
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: userData.token,
        }),
      });
      return await response.json();
    } catch (error) {
      console.log(error);
    }
  }

  createSetor() {
    this.props.history.push(`/setorCreate`);
  }

  render() {
    const { setores } = this.state;

    return (
      <div style={{ paddingLeft: "10px", paddingRight: "10px" }}>
        <Helmet>
          <title>Setores - iTicket</title>
        </Helmet>
        <div style={{ paddingBottom: "14px" }}>
          <Supernav />
        </div>
        <div>
          <h1>Setor</h1>
        </div>

        <div>
          <table className="table table-striped">
            <thead>
              <tr>
                <th>id</th>
                <th>Setor</th>
              </tr>
            </thead>
            <tbody>
              {setores.map((setor) => (
                <tr key={setor.id}>
                  <td>{setor.id}</td>
                  <td>{setor.nome}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
        <div>
          <Button style={{ float: "right" }} onClick={() => this.createSetor()}>
            Adicionar
          </Button>
        </div>
      </div>
    );
  }
}

export default Setor;
