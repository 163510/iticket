import React, { Component } from "react";
import userService from "./service/UserService";
import "bootstrap/dist/css/bootstrap.min.css";
import Supernav from "./Supernav";
import { Button } from "react-bootstrap";
import Helmet from "react-helmet";

class Sede extends Component {
  state = {
    sedes: [],
  };

  async componentDidMount() {
    const response = await this.getSedes();
    this.setState({ sedes: response });
  }

  async getSedes() {
    try {
      const userData = userService.getLoggedUser();
      const response = await fetch("http://localhost:3033/sede/all", {
        method: "GET",
        mode: "cors",
        headers: new Headers({
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: userData.token,
        }),
      });
      return await response.json();
    } catch (error) {
      console.log(error);
    }
  }

  createSede() {
    this.props.history.push(`/sedeCreate`);
  }

  render() {
    const { sedes } = this.state;

    return (
      <div style={{ paddingLeft: "10px", paddingRight: "10px" }}>
        <Helmet>
          <title>Sedes - iTicket</title>
        </Helmet>
        <div style={{ paddingBottom: "14px" }}>
          <Supernav />
        </div>
        <div>
          <h1>Setor</h1>
        </div>

        <div>
          <table className="table table-striped">
            <thead>
              <tr>
                <th>id</th>
                <th>Sede</th>
              </tr>
            </thead>
            <tbody>
              {sedes.map((setor) => (
                <tr key={setor.id}>
                  <td>{setor.id}</td>
                  <td>{setor.nome}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
        <div>
          <Button style={{ float: "right" }} onClick={() => this.createSede()}>
            Adicionar
          </Button>
        </div>
      </div>
    );
  }
}

export default Sede;
