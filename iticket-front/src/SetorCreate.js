import React, { Component } from "react";
import userService from "./service/UserService";
import "bootstrap/dist/css/bootstrap.min.css";
import Supernav from "./Supernav";
import { Button, Col, Row, Form } from "react-bootstrap";

class SetorCreate extends Component {
  state = {
    setor: {
      nome: "",
    },
  };

  handleChange(change, e) {
    let setor = this.state.setor;
    setor[change] = e.target.value;
    this.setState({ setor: setor });
  }

  createSetor() {
    try {
      const userData = userService.getLoggedUser();
      fetch("http://localhost:3033/setor", {
        method: "POST",
        mode: "cors",
        headers: new Headers({
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: userData.token,
        }),
        body: JSON.stringify(this.parseCreateSetorBody(this.state.setor)),
      });
      let setor = this.state.setor;
      setor.nome = "";
      this.setState({ setor: setor });
    } catch (error) {
      console.log(error);
    }
  }
  parseCreateSetorBody(setor) {
    return {
      nome: setor.nome,
    };
  }

  canSubmit(setor) {
    return setor.nome === null || setor.nome === "";
  }
  listSetor() {
    this.props.history.push(`/setor`);
  }
  render() {
    const { setor } = this.state;
    return (
      <div>
        <div style={{ paddingBottom: "14px" }}>
          <Supernav />
        </div>
        <div
          style={{
            padding: "40px",
            backgroundColor: "#F3F3F3",
          }}
        >
          <Form>
            <Row className="mb-1">
              <Form.Group as={Col}>
                <Form.Label>Nome</Form.Label>
                <Form.Control
                  type="text"
                  value={setor.nome}
                  placeholder="Nome"
                  onChange={this.handleChange.bind(this, "nome")}
                />
              </Form.Group>
            </Row>
            <Button style={{ float: "left" }} onClick={() => this.listSetor()}>
              Voltar
            </Button>
            <Button
              style={{ float: "right" }}
              variant="primary"
              type="submit"
              onClick={() => this.createSetor()}
              disabled={this.canSubmit(setor)}
            >
              Cadastrar
            </Button>
          </Form>
        </div>
      </div>
    );
  }
}

export default SetorCreate;
