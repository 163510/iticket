import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import './Home2.css';
import Supernav from "./Supernav";
import Helmet from "react-helmet";

class Home2 extends Component {
    render() {
        return (
            <section class="jumbotron text-center">
                <Helmet>
                    <title>Home - iTicket</title>
                </Helmet>
                <div>
                    <Supernav />
                </div>
                <div class="container">
                    <h1 class="jumbotron-heading">iTicket</h1>
                    <p class="lead text-muted">Para abrir seu chamado, clique no botão Chamados abaixo e, em seguida, clique em Novo.</p>
                    <p>
                        <a href="./chamados" class="btn btn-primary my-2">Chamados</a>
                        <a href="./usuario" class="btn btn-secondary my-2">Usuários</a>
                        <a href="./categoria" class="btn btn-secondary my-2">Categorias</a>
                    </p>
                </div>

                <div class="album py-5 bg-light">
                    <div class="container">

                        <div class="row">
                            <div class="col-md-4">
                                <div class="card mb-4 box-shadow">
                                    <div class="card-body">
                                        <p class="card-text">Lembre-se de registrar chamado para obter atendimento.</p>
                                        <div class="d-flex justify-content-between align-items-center">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="card mb-4 box-shadow">
                                    <div class="card-body">
                                        <p class="card-text">Suporte solicitado por e-mail ou telefone não será realizado</p>
                                        <div class="d-flex justify-content-between align-items-center">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="card mb-4 box-shadow">
                                    <div class="card-body">
                                        <p class="card-text">Ao final do atendimento, não esqueça de deixar sua nota no chamado!</p>
                                        <div class="d-flex justify-content-between align-items-center">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

export default Home2;