const userService = {
  async login(data) {
    const response = await fetch("http://localhost:3031/usuario/login", {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    });
    return response;
  },

  setLoggedUser(data) {
    let parsedData = JSON.stringify(data);
    localStorage.setItem("user", parsedData);
  },
  logout() {
    localStorage.removeItem("user");
  },
  getLoggedUser() {
    let data = localStorage.getItem("user");
    if (!data) return null;
    try {
      let parsedData = JSON.parse(data);
      return parsedData;
    } catch (error) {
      console.log(error);
      return null;
    }
  },
};

export default userService;
