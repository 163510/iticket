import React, { Component } from "react";
import "./myCss.css";

class Home extends Component {
  state = {
    item1Color: "lightBlue",
  };

  handleDivClick = (to) => {
    this.props.history.push(to);
  };

  render() {
    return (
      <div className="container">
        <div
          className="item"
          onClick={() => this.handleDivClick("/usuario")}
          style={{ background: this.state.item1Color }}
        >
          Usuario
        </div>
        <div
          className="item"
          onClick={() => this.handleDivClick("/categoria")}
          style={{ background: this.state.item1Color }}
        >
          Chamados
        </div>
        <div
          className="item"
          onClick={() => this.handleDivClick("/chamado")}
          style={{ background: this.state.item1Color }}
        >
          Categoria
        </div>
      </div>
    );
  }
}

export default Home;
