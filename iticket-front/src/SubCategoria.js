import React, { Component } from "react";
import userService from "./service/UserService";
import "bootstrap/dist/css/bootstrap.min.css";
import Supernav from "./Supernav";
import { Button } from "react-bootstrap";
import Helmet from "react-helmet";

class SubCategoria extends Component {
  state = {
    subCategorias: [],
  };

  async componentDidMount() {
    const response = await this.getSubCategory();
    console.log(response);
    this.setState({ subCategorias: response });
  }

  async getSubCategory() {
    try {
      const userData = userService.getLoggedUser();
      const response = await fetch("http://localhost:3032/subcategoria/all", {
        method: "GET",
        mode: "cors",
        headers: new Headers({
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: userData.token,
        }),
      });
      return await response.json();
    } catch (error) {
      console.log(error);
    }
  }

  createSubCategory() {
    this.props.history.push(`/subCategoriaCreate`);
  }

  render() {
    const { subCategorias } = this.state;

    return (
      <div style={{ paddingLeft: "10px", paddingRight: "10px" }}>
        <Helmet>
          <title>Subcategorias - iTicket</title>
        </Helmet>
        <div style={{ paddingBottom: "14px" }}>
          <Supernav />
        </div>
        <h1>Subcategorias</h1>
        <div>
          <table className="table table-striped">
            <thead>
              <tr>
                <th>ID</th>
                <th>Nome</th>
                <th>Categoria</th>
                <th>Descrição</th>
                <th>Tempo Previsto</th>
              </tr>
            </thead>
            <tbody>
              {console.log(subCategorias)}
              {subCategorias.map((subCategoria) => (
                <tr key={subCategoria.id}>
                  <td>{subCategoria.id}</td>
                  <td>{subCategoria.nome}</td>
                  <td>{subCategoria.categoria.nome}</td>
                  <td>{subCategoria.descricao}</td>
                  <td>{`${subCategoria.tempoPrevistoAtendimento} Minutos`}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
        <div>
          <Button
            style={{ float: "right" }}
            onClick={() => this.createSubCategory()}
          >
            Adicionar
          </Button>
        </div>
      </div>
    );
  }
}

export default SubCategoria;
