import React, { Component } from "react";
import userService from "./service/UserService";
import "bootstrap/dist/css/bootstrap.min.css";
import Supernav from "./Supernav";
import Helmet from "react-helmet";



class Grupo extends Component {
    state = {
        grupos: [],
    };

    async componentDidMount() {
        const response = await this.getGroups();
        this.setState({ grupos: response });
    }

    async getGroups() {
        try {
            const userData = userService.getLoggedUser();
            const response = await fetch("http://localhost:3031/grupo/all", {
                method: "GET",
                mode: "cors",
                headers: new Headers({
                    Accept: "application/json",
                    "Content-Type": "application/json",
                    Authorization: userData.token,
                }),
            });
            return await response.json();
        } catch (error) {
            console.log(error);
        }
    }

    updateGrupo(user) { }

    render() {
        const { grupos } = this.state;

        return (
            <div style={{ paddingLeft: "10px", paddingRight: "10px" }}>
                <Helmet>
                    <title>Grupos - iTicket</title>
                </Helmet>
                <div style={{ paddingBottom: "14px" }}><Supernav /></div>
                <h1>Grupos</h1>
                <div>
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nome</th>
                                <th>Descrição</th>
                            </tr>
                        </thead>
                        <tbody>
                            {grupos.map((grupo) => (
                                <tr key={grupo.id}>
                                    <td>{grupo.id}</td>
                                    <td>{grupo.nome}</td>
                                    <td>{grupo.descricao}</td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            </div>

        );
    }
}

export default Grupo;
