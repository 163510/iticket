import React, { Component } from "react";
import userService from "./service/UserService";
import "bootstrap/dist/css/bootstrap.min.css";
import Supernav from "./Supernav";
import Helmet from "react-helmet";

class Usuario extends Component {
  state = {
    users: [],
  };

  async componentDidMount() {
    const response = await this.getUsers();
    this.setState({ users: response });
  }

  async getUsers() {
    const userData = userService.getLoggedUser();
    const response = await fetch("http://localhost:3031/usuario/all", {
      method: "GET",
      mode: "cors",
      headers: new Headers({
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: userData.token,
      }),
    });
    return await response.json();
  }

  updateUsuario(user) {}

  goToCreate() {
    this.props.history.push("/usuariocreate");
  }

  render() {
    const { users } = this.state;

    return (
      <div style={{ paddingLeft: "10px", paddingRight: "10px" }}>
        <Helmet>
          <title>Usuários - iTicket</title>
        </Helmet>
        <div style={{ paddingBottom: "14px" }}>
          <Supernav />
        </div>
        <h1>Usuários</h1>
        <button
          type="button"
          onClick={() => this.goToCreate()}
          class="btn btn-primary"
        >
          Novo
        </button>
        <div>
          <table className="table table-striped">
            <thead>
              <tr>
                <th>id</th>
                <th>nome</th>
                <th>telefone</th>
                <th>email</th>
              </tr>
            </thead>
            <tbody>
              {users.map((user) => (
                <tr key={user.id}>
                  <td>{user.id}</td>
                  <td>{user.nome}</td>
                  <td>{user.telefone}</td>
                  <td>{user.email}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default Usuario;
