CREATE TABLE sede (
	id BIGINT NOT NULL auto_increment,
    nome VARCHAR(255),
    data_criacao TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    data_atualizacao TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
)COLLATE='utf8_general_ci'
engine = InnoDB;

CREATE TABLE setor (
	id BIGINT NOT NULL auto_increment,
    nome VARCHAR(255),
    data_criacao TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    data_atualizacao TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
)COLLATE='utf8_general_ci'
engine = InnoDB;

ALTER TABLE chamado ADD COLUMN sede_id BIGINT AFTER atendente_id;
ALTER TABLE chamado ADD COLUMN setor_id BIGINT AFTER sede_id;