CREATE TABLE categoria (
	id BIGINT NOT NULL auto_increment,
    nome VARCHAR(255),
    descricao text,
    data_criacao TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    data_atualizacao TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
)COLLATE='utf8_general_ci'
engine = InnoDB;

CREATE TABLE sub_categoria(
	id BIGINT NOT NULL auto_increment,
    categoria_id BIGINT NOT NULL,
    nome VARCHAR(255),
    descricao text,
    template text,
    tempo_previsto_atendimento INTEGER,
    data_criacao TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    data_atualizacao TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    FOREIGN KEY(categoria_id) REFERENCES categoria (id)
)COLLATE='utf8_general_ci'
engine = InnoDB;

CREATE TABLE grupo(
	id BIGINT NOT NULL auto_increment,
    nome VARCHAR(255),
    descricao text,
    data_criacao TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    data_atualizacao TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
)COLLATE='utf8_general_ci'
engine = InnoDB;

CREATE TABLE usuario(
	id BIGINT NOT NULL auto_increment,
    nome VARCHAR(255),
    telefone VARCHAR(15),
    email VARCHAR(255),
    senha VARCHAR(255),
    primeiro_acesso BIT,
    usuario_criador_id BIGINT,
    data_criacao TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    data_atualizacao TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
)COLLATE='utf8_general_ci'
engine = InnoDB;

CREATE TABLE usuario_grupo(
	usuario_id BIGINT NOT NULL,
    grupo_id BIGINT NOT NULL,
    PRIMARY KEY(usuario_id, grupo_id)
)COLLATE='utf8_general_ci'
engine = InnoDB;

CREATE TABLE chamado(
	id BIGINT NOT NULL auto_increment,
    sub_categoria_id BIGINT NOT NULL,
    cliente_id BIGINT,
    atendente_id BIGINT,
    titulo TEXT,
    descricao LONGTEXT,
    data_criacao TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    data_atualizacao TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    data_finalizacao TIMESTAMP,
    PRIMARY KEY(id)
)COLLATE='utf8_general_ci'
engine = InnoDB;

CREATE TABLE nota(
	id BIGINT NOT NULL auto_increment,
    usuario_id BIGINT NOT NULL,
    chamado_id BIGINT NOT NULL,
    nota INTEGER NOT NULL,
    comentario text,
    data_criacao TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
    FOREIGN KEY (usuario_id) REFERENCES usuario (id)
)COLLATE='utf8_general_ci'
engine = InnoDB;

CREATE TABLE anexo(
	id BIGINT NOT NULL auto_increment,
    chamado_id BIGINT,
    message_id BIGINT,
    arquivo MEDIUMBLOB,
    nome_arquivo VARCHAR(255),
    formato_arquivo VARCHAR(255),
	data_criacao TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (id)
)COLLATE='utf8_general_ci'
engine = InnoDB;

CREATE TABLE mensagem(
	id BIGINT NOT NULL auto_increment,
    usuario_id BIGINT NOT NULL,
    chamado_id BIGINT NOT NULL,
    conteudo LONGTEXT,
    data_criacao TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
)COLLATE='utf8_general_ci'
engine = InnoDB;

