package iticket.integration.itcketchamado.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import iticket.integration.itcketchamado.client.UsuarioClient;
import iticket.integration.itcketchamado.domain.AtendenteAverage;
import iticket.integration.itcketchamado.domain.AverageNotaResponse;
import iticket.integration.itcketchamado.dto.NotaDTO;
import iticket.integration.itcketchamado.dto.UsuarioDTO;
import iticket.integration.itcketchamado.entity.NotaEntity;
import iticket.integration.itcketchamado.exception.chamado.ChamadoNotFoundException;
import iticket.integration.itcketchamado.exception.nota.ChamadoHasNotaException;
import iticket.integration.itcketchamado.exception.nota.NotaNotFoundException;
import iticket.integration.itcketchamado.repository.ChamadoRepository;
import iticket.integration.itcketchamado.repository.NotaRepository;
import iticket.integration.itcketchamado.validator.NotaValidator;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class NotaService {

	@Autowired
	private NotaRepository notaRepository;
	
	@Autowired
	private ChamadoRepository chamadoRepository;
	
	@Autowired
	private NotaValidator notaValidator;
	
	@Autowired
	private ChamadoService chamadoService;
	
	@Autowired
	private UsuarioClient usuarioClient;
	
	public List<NotaDTO> findAll() {
		List<NotaEntity> notas = notaRepository.findAll();
		return notas.stream()
				.map(NotaDTO::new)
				.collect(Collectors.toList());
	}
	
	public NotaDTO findById(Long id) {
		Optional<NotaEntity> optNota = notaRepository.findById(id);
		
		if(optNota.isPresent()) {
			return new NotaDTO(optNota.get());
		}
		
		throw new NotaNotFoundException("Nenhuma nota com id <" + id + "> foi encontrada!");
	}
	
	public List<NotaDTO> findByAtendenteId(Long atendenteId) {
		List<NotaEntity> notas = notaRepository.findByAtendenteId(atendenteId);
		return notas.stream()
				.map(NotaDTO::new)
				.collect(Collectors.toList());
	}
	
	public void saveNota(NotaDTO nota) {
		
		notaValidator.validateNewNota(nota);

		if(!chamadoRepository.existsById(nota.getChamadoId())) {
			throw new ChamadoNotFoundException("Nenhum chamado foi encontrado com o id <" + nota.getChamadoId() + ">");
		}

		if(chamadoHasNota(nota.getChamadoId())) {
			throw new ChamadoHasNotaException("O chamado <" + nota.getChamadoId() + "> já possui uma nota atribuida!");
		}
		
		notaRepository.save(new NotaEntity(nota));
		
		chamadoService.finishChamado(nota.getChamadoId());
	}
	
	
	public AverageNotaResponse calculeAverageNota(Long days, String token) {
		
		List<NotaEntity> notas = notaRepository.findByDataCriacaoGreaterThanEqual(LocalDateTime.now().minusDays(days));
		
		List<AtendenteAverage> averageByAtendente = new ArrayList<>();
		if(notas.isEmpty()) {
			return new AverageNotaResponse(0.0, days, notas.size(), averageByAtendente);
		}
		
		Double allAverage = calculateAverage(notas);
		
		Map<Long, List<NotaEntity>> notasByAtendente = notas.stream().collect(Collectors.groupingBy(NotaEntity::getAtendenteId));
		
		// key = atendente id || value = atendente notas
		notasByAtendente.forEach((k,v)-> {
			
			UsuarioDTO atendente = getAtendenteNome(k, token);
			
			Double average = calculateAverage(v);
			
			AtendenteAverage atendenteAverage = new AtendenteAverage(atendente, average, v.size());
			averageByAtendente.add(atendenteAverage);
		});
		
		return new AverageNotaResponse(allAverage, days, notas.size(), averageByAtendente);
	}
	
	private UsuarioDTO getAtendenteNome(Long k, String token) {

		try {
			return usuarioClient.findUserById(k, token);
		}catch (Exception e) {
			log.error("getAtendenteNome() - [ERROR] - message <{}> - e:", e.getMessage(), e);
		}

		return null;
	}

	private Double calculateAverage(List<NotaEntity> notas) {
		return BigDecimal.valueOf(notas.stream().mapToLong(NotaEntity::getNota).sum())
				.divide(BigDecimal.valueOf(notas.size()), 2, RoundingMode.CEILING)
				.doubleValue();
	}
	
	public AtendenteAverage calculeAverageAtendenteNota(Long days, Long atendenteId, String token) {
		
		log.info("Data: {}", LocalDateTime.now().minusDays(days));
		
		List<NotaEntity> notas = notaRepository.findByDataCriacaoGreaterThanEqualAndAtendenteId(LocalDateTime.now().minusDays(days), atendenteId);
		
		UsuarioDTO user = usuarioClient.findUserById(atendenteId, token);
		
		if(notas.isEmpty()) {
			return new AtendenteAverage(user, 0.0, notas);
		}
		
		Double value = calculateAverage(notas);
		
		return new AtendenteAverage(user, value, notas);
	}
	
	private boolean chamadoHasNota(Long chamadoId) {
		Long qtdNotasChamado = notaRepository.countByChamadoId(chamadoId);
		return qtdNotasChamado > 0;
	}
	
}
