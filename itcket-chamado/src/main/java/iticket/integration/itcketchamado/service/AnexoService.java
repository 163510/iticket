package iticket.integration.itcketchamado.service;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import iticket.integration.itcketchamado.dto.AnexoDTO;
import iticket.integration.itcketchamado.entity.AnexoEntity;
import iticket.integration.itcketchamado.exception.anexo.AnexoNotFoundException;
import iticket.integration.itcketchamado.repository.AnexoRepository;
import iticket.integration.itcketchamado.validator.AnexoValidator;

@Service
public class AnexoService {

	@Autowired
	private AnexoRepository anexoRepository;
	
	@Autowired
	private AnexoValidator anexoValidator;
	
	public void saveAnexo(MultipartFile file, Long chamadoId, Long messageId) throws IOException {
		
		anexoValidator.validateNewFile(file, chamadoId, messageId);
		
		AnexoEntity anexo = new AnexoEntity(chamadoId, messageId, file.getBytes(), file.getResource().getFilename(), file.getContentType());

		anexoRepository.save(anexo);
	}

	public List<AnexoDTO> findByChamadoId(Long chamadoId) {

		List<AnexoEntity> anexosEntities = anexoRepository.findByChamadoId(chamadoId);
		
		return anexosEntities.stream().map(AnexoDTO::new).collect(Collectors.toList());
		
	}

	public List<AnexoDTO> finByMessageId(Long messageId) {

		List<AnexoEntity> anexos = anexoRepository.findByMessageId(messageId);
		
		return anexos.stream().map(AnexoDTO::new).collect(Collectors.toList());
	}

	public ResponseEntity<ByteArrayResource> downloadFile(Long anexoId) {
		
		Optional<AnexoEntity> optAnexo = anexoRepository.findById(anexoId);
		
		if(!optAnexo.isPresent()) {
			throw new AnexoNotFoundException("Anexo de id <" + anexoId + "> não encontrado!");
		}
		
		AnexoEntity anexo = optAnexo.get();
		ByteArrayResource resource = new ByteArrayResource(anexo.getArquivo());
		return ResponseEntity
				.status(HttpStatus.OK)
				.header("Content-Disposition", "attachment; filename=" + anexo.getNomeArquivo())
				.contentLength(resource.contentLength())
				.contentType(MediaType.parseMediaType(anexo.getFormatoArquivo()))
				.body(resource);
	}

	public List<AnexoDTO> findAll() {

		List<AnexoEntity> anexos = anexoRepository.findAll();
		
		return anexos.stream().map(AnexoDTO::new).collect(Collectors.toList());
	}

	public void deleteAnexo(Long id) {

		anexoRepository.deleteById(id);
		
	}
	

}
