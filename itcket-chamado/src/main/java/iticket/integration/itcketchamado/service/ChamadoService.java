package iticket.integration.itcketchamado.service;

import static iticket.integration.itcketchamado.constants.ChamadoStatus.CONCLUDED;
import static iticket.integration.itcketchamado.constants.ChamadoStatus.FINISHED;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import iticket.integration.itcketchamado.client.CategoriaClient;
import iticket.integration.itcketchamado.client.IticketCompanyClient;
import iticket.integration.itcketchamado.client.UsuarioClient;
import iticket.integration.itcketchamado.constants.ChamadoStatus;
import iticket.integration.itcketchamado.domain.ChamadoBySedeAndSetor;
import iticket.integration.itcketchamado.domain.ChamadoByStatus;
import iticket.integration.itcketchamado.domain.ChamadoRequest;
import iticket.integration.itcketchamado.domain.ChamadosBySedeSetorResponse;
import iticket.integration.itcketchamado.domain.ChamadosBySubCategoria;
import iticket.integration.itcketchamado.domain.Response;
import iticket.integration.itcketchamado.domain.UpdateChamado;
import iticket.integration.itcketchamado.domain.UpdateStatusRequest;
import iticket.integration.itcketchamado.dto.ChamadoDTO;
import iticket.integration.itcketchamado.dto.ChamadoDetalheDTO;
import iticket.integration.itcketchamado.dto.SedeDTO;
import iticket.integration.itcketchamado.dto.SetorDTO;
import iticket.integration.itcketchamado.dto.SubCategoriaDTO;
import iticket.integration.itcketchamado.dto.UsuarioDTO;
import iticket.integration.itcketchamado.entity.ChamadoEntity;
import iticket.integration.itcketchamado.exception.chamado.ChamadoNotFoundException;
import iticket.integration.itcketchamado.exception.chamado.ChamadoUpdateException;
import iticket.integration.itcketchamado.repository.ChamadoRepository;
import iticket.integration.itcketchamado.repository.SedeRepository;
import iticket.integration.itcketchamado.repository.SetorRepository;
import iticket.integration.itcketchamado.repository.UsuarioRepository;
import iticket.integration.itcketchamado.validator.ChamadoValidator;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ChamadoService {

	@Autowired
	private ChamadoRepository chamadoRepository;
	
	@Autowired
	private ChamadoValidator chamadoValidator;
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Autowired
	private SedeRepository sedeRepository;
	
	@Autowired
	private SetorRepository setorRepository;
	
	@Autowired
	private UsuarioClient usuarioClient;
	
	@Autowired
	private CategoriaClient categoriaClient;
	
	@Autowired
	private IticketCompanyClient companyClient;
	
	public List<ChamadoDTO> getChamados() {
		
		List<ChamadoEntity> chamados = chamadoRepository.findChamadosNotConcluded();
		
		return chamados.stream().map(ChamadoDTO::new).collect(Collectors.toList());
	}
	
	public void updateChamadoStatus(UpdateStatusRequest request) {
		
		
		chamadoValidator.validateChamadoUpdateStatus(request);
		
		Optional<ChamadoEntity> optChamado = chamadoRepository.findById(request.getId());
		
		if(!optChamado.isPresent()) {
			throw new ChamadoNotFoundException("Nenhum chamado com id <" + request.getId() + "> foi encontrado!");
		}
		
		ChamadoEntity chamado = optChamado.get();
		
		if(CONCLUDED.getStatus().equalsIgnoreCase(chamado.getStatus())) {
			throw new ChamadoUpdateException("O chamado está com status concluido e não pode ser alterado!");
		}
		
		if(FINISHED.getStatus().equalsIgnoreCase(chamado.getStatus())) {
			chamado.setDataFinalizacao(LocalDateTime.now());
		}
		
		chamado.setStatus(request.getStatus());
		chamadoRepository.save(chamado);
	}

	// used by scheudler only
	public void concludefinishedChamados() {
		
		List<ChamadoEntity> finishedChamados = chamadoRepository.findChamadosLongtimeFinished();
		
		for (ChamadoEntity chamadoEntity : finishedChamados) {
			chamadoEntity.setStatus(CONCLUDED.getStatus());
		}
		
		chamadoRepository.saveAll(finishedChamados);
		
	}

	public void finishChamado(Long id) {
		
		Optional<ChamadoEntity> optChamado = chamadoRepository.findById(id);

		if(!optChamado.isPresent()) {
			throw new ChamadoNotFoundException("Chamado com id <" + id + "> não encontrado!");
		}
		
		ChamadoEntity chamado = optChamado.get();
		chamado.setStatus(CONCLUDED.getStatus());
		
		chamadoRepository.save(chamado);
	}

	public Response createChamado(ChamadoRequest request) {

		chamadoValidator.validateChamdoCreate(request);
		
		ChamadoEntity entity = new ChamadoEntity(request);
		
		chamadoRepository.save(entity);
		
		return new Response(true, new ArrayList<>());
	}

	public ResponseEntity<Response> updateChamado(Long id, UpdateChamado request) {

		Optional<ChamadoEntity> optChamado = chamadoRepository.findById(id);
		
		Response response = new Response(true, new ArrayList<>());
		
		if(!optChamado.isPresent()) {
			response.setSuccess(false);
			response.getErrors().add("Chamado de id <" + id + "> não encontrado!");
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}
		
		ChamadoEntity chamado = optChamado.get();
		
		if(request.getTitulo() != null && !request.getTitulo().isEmpty()) {
			chamado.setTitulo(request.getTitulo());
		}
		
		if(request.getDescricao() != null && !request.getDescricao().isEmpty()) {
			chamado.setDescricao(request.getDescricao());
		}
		
		chamadoRepository.save(chamado);
		
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}

	public ResponseEntity<Response> updateChamadoAtendente(Long chamadoId, Long atendenteId) {

		Response response = new Response(true, new ArrayList<>());
		
		Optional<ChamadoEntity> optChamado = chamadoRepository.findById(chamadoId);
		
		if(optChamado.isEmpty()) {
			response.setSuccess(false);
			response.getErrors().add("Chamado com id <" + chamadoId + "> não encontrado!");
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}
		
		if(!usuarioRepository.existsById(atendenteId)) {
			response.setSuccess(false);
			response.getErrors().add("Atendente com id <" + atendenteId + "> não encontrado!");
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}
		
		ChamadoEntity chamado = optChamado.get();
		chamado.setAtendenteId(atendenteId);
		
		chamadoRepository.save(chamado);
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}

	public ResponseEntity<Response> updateChamadoSetor(Long chamadoId, Long setorId) {

		Response response = new Response(true, new ArrayList<>());
		
		Optional<ChamadoEntity> optChamado = chamadoRepository.findById(chamadoId);
		
		if(optChamado.isEmpty()) {
			response.setSuccess(false);
			response.getErrors().add("Chamado com id <" + chamadoId + "> não encontrado!");
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}
		
		if(!setorRepository.existsById(setorId)) {
			response.setSuccess(false);
			response.getErrors().add("Setor com id <" + setorId + "> não encontrado!");
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}
		
		ChamadoEntity chamado = optChamado.get();
		chamado.setSetorId(setorId);
		
		chamadoRepository.save(chamado);
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
	public ResponseEntity<Response> updateChamadoSede(Long chamadoId, Long sedeId) {

		Response response = new Response(true, new ArrayList<>());
		
		Optional<ChamadoEntity> optChamado = chamadoRepository.findById(chamadoId);
		
		if(optChamado.isEmpty()) {
			response.setSuccess(false);
			response.getErrors().add("Chamado com id <" + chamadoId + "> não encontrado!");
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}
		
		if(!sedeRepository.existsById(sedeId)) {
			response.setSuccess(false);
			response.getErrors().add("Sede com id <" + sedeId + "> não encontrado!");
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}
		
		ChamadoEntity chamado = optChamado.get();
		chamado.setSedeId(sedeId);
		
		chamadoRepository.save(chamado);
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
	public ResponseEntity<ChamadoDetalheDTO> getChamado(Long id, String token) {
		
		Optional<ChamadoEntity> optChamado = chamadoRepository.findById(id);
		
		if(!optChamado.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		ChamadoEntity chamadoEntity = optChamado.get();
		UsuarioDTO atendente = new UsuarioDTO();
		if(chamadoEntity.getAtendenteId() != null) {
			atendente = usuarioClient.findUserById(chamadoEntity.getAtendenteId(), token);
		}
		
		UsuarioDTO cliente = usuarioClient.findUserById(chamadoEntity.getClienteId(), token);
		
		SubCategoriaDTO subCategoria = categoriaClient.findById(chamadoEntity.getSubCategoriaId(), token);
		
		SedeDTO sede = companyClient.findSedeById(chamadoEntity.getSedeId(), token);
		
		SetorDTO setor = companyClient.findSetorById(chamadoEntity.getSetorId(), token);
		ChamadoDetalheDTO response = new ChamadoDetalheDTO(atendente, cliente, subCategoria, sede, setor, chamadoEntity);
		log.info("getChamado() - response <{}>", response);
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
	public ResponseEntity<List<ChamadosBySubCategoria>> getChamadosGroupBySubcategoria(Long days, String token) {
		
		List<ChamadoEntity> chamados = chamadoRepository.findByDataCriacaoGreaterThanEqual(LocalDateTime.now().minusDays(days));
		
		//key = subcategory id || value = chamados
		Map<Long, List<ChamadoEntity>> chamadosbySubCategoria = chamados.stream().collect(Collectors.groupingBy(ChamadoEntity::getSubCategoriaId));
		
		List<ChamadosBySubCategoria> chamadosBySubcategoria = new ArrayList<>();
		chamadosbySubCategoria.forEach((subCategoryId, subCategoriaChamados) -> {
			
			SubCategoriaDTO subcategoria = categoriaClient.findById(subCategoryId, token);
			
			//key = status || value = chamados
			Map<String, List<ChamadoEntity>> subCategoriaChamadosByStatus = subCategoriaChamados.stream().collect(Collectors.groupingBy(ChamadoEntity::getStatus));
			
			ChamadoStatus[] status = ChamadoStatus.values();
			List<ChamadoByStatus> chamadosByStatus = new ArrayList<>();
			for (ChamadoStatus chamadoStatus : status) {
				
				if(subCategoriaChamadosByStatus.containsKey(chamadoStatus.getStatus())) {
					Integer size = subCategoriaChamadosByStatus.get(chamadoStatus.getStatus()).size();
					chamadosByStatus.add(new ChamadoByStatus(chamadoStatus.getTransalation(), size));
					continue;
				}
				chamadosByStatus.add(new ChamadoByStatus(chamadoStatus.getTransalation(), 0));
			}
			ChamadosBySubCategoria chamadoBySubCategoria = new ChamadosBySubCategoria(subcategoria, subCategoriaChamados.size(), chamadosByStatus);
			chamadosBySubcategoria.add(chamadoBySubCategoria);
			
		});
		
		return ResponseEntity.status(HttpStatus.OK).body(chamadosBySubcategoria);
	}
	
	public ResponseEntity<ChamadosBySedeSetorResponse> getChamadosBySedeAndSetor(Long days, String token) {
		
		log.info("getChamadosBySedeAndSetor() - days <{}> - time <{}>", LocalDateTime.now().minusDays(days));
		
		List<ChamadoEntity> chamados = chamadoRepository.findByDataCriacaoGreaterThanEqual(LocalDateTime.now().minusDays(days));
		log.info("getChamadosBySedeAndSetor() - chamados <{}>", chamados.size());
		
		//key = sede id || value chamados
		Map<Long, List<ChamadoEntity>> chamadosBySede = chamados.stream().collect(Collectors.groupingBy(ChamadoEntity::getSedeId));
		
		List<ChamadoBySedeAndSetor> response = new ArrayList<>();
		Map<Long, SetorDTO> setores = new HashMap<>();
		
		chamadosBySede.forEach((sedeId, sedeChamados) -> {
			
			SedeDTO sede = companyClient.findSedeById(sedeId, token);
			
			//key = setor id || value chamados
			Map<Long, List<ChamadoEntity>> sedeChamadosBySetor = sedeChamados.stream().collect(Collectors.groupingBy(ChamadoEntity::getSetorId));
			
			sedeChamadosBySetor.forEach((setorId, setorSedeChamados) -> {
		
				SetorDTO setor = findSetor(setorId, setores, token);
				
				ChamadoBySedeAndSetor chamado = new ChamadoBySedeAndSetor();
				chamado.setSede(sede.getNome());
				chamado.setSedeTotalChamados(sedeChamados.size());
				chamado.setPorcentagemSedeDeChamados(calculatePercent(sedeChamados.size(), chamados.size()));

				chamado.setSetor(setor.getNome());
				chamado.setSetorTotalChamados(setorSedeChamados.size());
				chamado.setPorcentagemSetorDeChamados(calculatePercent(setorSedeChamados.size(), sedeChamados.size()));
				response.add(chamado);
			});
			
		});
		
		ChamadosBySedeSetorResponse resp = new ChamadosBySedeSetorResponse(chamados.size(), response);
		
		return ResponseEntity.status(HttpStatus.OK).body(resp);
	}
	
	private SetorDTO findSetor(Long setorId, Map<Long, SetorDTO> setores, String token) {

		if(setores.containsKey(setorId)) {
			return setores.get(setorId);
		}
		
		SetorDTO setor = companyClient.findSetorById(setorId, token);
		
		setores.put(setor.getId(), setor);
		return setor;
	}

	private Double calculatePercent(Integer value, Integer max) {
		return BigDecimal.valueOf(value)
				.multiply(BigDecimal.valueOf(100))
				.divide(BigDecimal.valueOf(max), 2, RoundingMode.HALF_UP)
				.doubleValue();
	}
	
}

