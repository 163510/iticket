package iticket.integration.itcketchamado.service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import iticket.integration.itcketchamado.client.UsuarioClient;
import iticket.integration.itcketchamado.dto.MensagemDTO;
import iticket.integration.itcketchamado.dto.UsuarioDTO;
import iticket.integration.itcketchamado.entity.MensagemEntity;
import iticket.integration.itcketchamado.exception.chamado.ChamadoNotFoundException;
import iticket.integration.itcketchamado.exception.mensagem.MensagemNotFoundException;
import iticket.integration.itcketchamado.repository.ChamadoRepository;
import iticket.integration.itcketchamado.repository.MensagemRepository;
import iticket.integration.itcketchamado.validator.MensagemValidator;

@Service
public class MensagemService {

	@Autowired
	private MensagemValidator validator;
	
	@Autowired
	private MensagemRepository mensagemRespository;
	
	@Autowired
	private ChamadoRepository chamadoRepository;
	
	@Autowired
	private UsuarioClient usuarioCliente;
	
	public List<MensagemDTO> findMenssagesByChamado(Long chamadoId, String token) {
		
		List<MensagemEntity> mensagens = mensagemRespository.findByChamadoId(chamadoId);
		
		List<MensagemDTO> mensagensDto = new ArrayList<>();
		Map<Long, UsuarioDTO> usuarios = new HashMap<>();
		for (MensagemEntity entity : mensagens) {
			try {
				
				if(!usuarios.containsKey(entity.getUsuarioId())) {
					UsuarioDTO usuario = usuarioCliente.findUserById(entity.getUsuarioId(), token);
					usuarios.put(entity.getUsuarioId(), usuario);
					mensagensDto.add(new MensagemDTO(entity, usuario.getNome()));
					continue;
				}
				
				mensagensDto.add(new MensagemDTO(entity, usuarios.get(entity.getUsuarioId()).getNome()));
				
			}catch (Exception e) {
				mensagensDto.add(new MensagemDTO(entity, "Usuario Escondido"));
			}
		}
		mensagensDto.sort(Comparator.comparing(MensagemDTO::getId).reversed());
		return mensagensDto;
	}


	public void addMessage(MensagemDTO mensagem) {

		validator.validateNewMessage(mensagem);
		
		if(!chamadoRepository.existsById(mensagem.getChamadoId())) {
			throw new ChamadoNotFoundException("Nenhum chamado foi encontrado com o id <" + mensagem.getChamadoId() + ">");
		}
		
		MensagemEntity entity = new MensagemEntity(mensagem);
		
		mensagemRespository.save(entity);
		
	}


	public void updateMessage(MensagemDTO mensagem) {

		validator.validateUpdateMessage(mensagem);
		
		if(!chamadoRepository.existsById(mensagem.getChamadoId())) {
			throw new ChamadoNotFoundException("Nenhum chamado foi encontrado com o id <" + mensagem.getChamadoId() + ">");
		}
		
		if(!mensagemRespository.existsById(mensagem.getId())) {
			throw new MensagemNotFoundException("Nenhuma mensagem foi encontrada com o id <" + mensagem.getId() + ">");
		}
		
		MensagemEntity entity = new MensagemEntity(mensagem);
		
		mensagemRespository.save(entity);
		
	}


	public void deleteMensagem(Long id) {

		mensagemRespository.deleteById(id);
		
	}

	
	
}
