package iticket.integration.itcketchamado.client;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import iticket.integration.itcketchamado.domain.Response;
import iticket.integration.itcketchamado.dto.UsuarioDTO;

@FeignClient(name = "UsuarioClient", url = "http://localhost:3031")
public interface UsuarioClient {

	@PostMapping(path = "/usuario/validate/token", headers = {"apikey:NkkkcmpSbmV2ZDQzTVI3byoycFdiTk1fMmFMVmVTLWZIcjZMU2dGQ0tHNUU0RUZGTiU="})
	ResponseEntity<Response> validateUserToken(
			@RequestHeader("Authorization") String token,
			@RequestHeader("apikey") String apikey,
			@RequestParam("roles") List<String> role);
	
	@GetMapping(path = "/usuario/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	UsuarioDTO findUserById(
			@PathVariable("id") Long id, 
			@RequestHeader("Authorization") String token);
}
