package iticket.integration.itcketchamado.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;

import iticket.integration.itcketchamado.dto.SubCategoriaDTO;

@FeignClient(name = "CategoriaClient", url = "http://localhost:3032")
public interface CategoriaClient {

	@GetMapping("/subcategoria/{id}")
	SubCategoriaDTO findById(
			@PathVariable("id") Long id, 
			@RequestHeader("Authorization") String token);
	
}
