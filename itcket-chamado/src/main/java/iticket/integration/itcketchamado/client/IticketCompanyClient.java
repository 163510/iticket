package iticket.integration.itcketchamado.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;

import iticket.integration.itcketchamado.dto.SedeDTO;
import iticket.integration.itcketchamado.dto.SetorDTO;

@FeignClient(name = "IticketCompanyClient", url = "http://localhost:3033")
public interface IticketCompanyClient {

	@GetMapping("/sede/{id}")
	SedeDTO findSedeById(
			@PathVariable("id") Long id, 
			@RequestHeader("Authorization") String token);

	@GetMapping("/setor/{id}")
	SetorDTO findSetorById(
			@PathVariable("id") Long id, 
			@RequestHeader("Authorization") String token);
	
}
