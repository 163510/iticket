package iticket.integration.itcketchamado.constants;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ChamadoStatus {

	OPEN("OPEN", "Aberto"),
	WORKING_IN_PROGRESS("WORKING_IN_PROGRESS", "Em Atendimento"),
	WAITING_EXTERNAL_SUPPORT("WAITING_EXTERNAL_SUPPORT", "Aguardando Serviço Externo"),
	FINISHED("FINISHED", "Finalizado"),
	CONCLUDED("CONCLUDED", "Concluido");
	
	private String status;
	private String transalation;
	
	public static String getTransalationByStatus(String status) {
		List<ChamadoStatus> values = Arrays.asList(ChamadoStatus.values());
		
		for (ChamadoStatus chamadoStatus : values) {
			if(chamadoStatus.getStatus().equalsIgnoreCase(status)) {
				return chamadoStatus.getTransalation();
			}
		}
		
		return status;
	}
	
	public static boolean isValidStatus(String status) {
		List<ChamadoStatus> values = Arrays.asList(ChamadoStatus.values());
		List<String> statusList = values.stream().map(ChamadoStatus::getStatus).collect(Collectors.toList());
		return statusList.contains(status);
	}
	
}
