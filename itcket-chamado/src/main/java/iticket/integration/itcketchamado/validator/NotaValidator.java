package iticket.integration.itcketchamado.validator;

import java.util.ArrayList;

import org.springframework.stereotype.Component;

import iticket.integration.itcketchamado.domain.ValidationBody;
import iticket.integration.itcketchamado.dto.NotaDTO;
import iticket.integration.itcketchamado.exception.nota.MissingNotaDataException;

@Component
public class NotaValidator extends Validator{

	public void validateNewNota(NotaDTO nota) {
		throwIfNull(nota, "A nota informada esta nula!");

		ValidationBody validationBody = new ValidationBody(true, new ArrayList<>());
		
		notaHasChamado(nota, validationBody);
		notaHasValidNota(nota,validationBody);
		notaHasAtendente(nota, validationBody);
		
		if(!validationBody.isValid()) {
			throw new MissingNotaDataException(joinErrors(validationBody.getErrors()));
		}
	}

	private void notaHasAtendente(NotaDTO nota, ValidationBody validationBody) {

		if(nota.getAtendenteId() == null) {
			validationBody.setValid(false);
			validationBody.getErrors().add("A nota precisa ser atribuida a um atendente!");
		}
		
	}

	private void notaHasValidNota(NotaDTO nota, ValidationBody validationBody) {

		if(nota.getNota() == null) {
			validationBody.setValid(false);
			validationBody.getErrors().add("Nenhum valor foi atribuido a nota!");
			return;
		}
		
		if(nota.getNota() > 100) {
			validationBody.setValid(false);
			validationBody.getErrors().add("A nota atribuida foi <" + nota.getNota() + "> o maximo aceitado é 100!");
		}
		
		if(nota.getNota() < 0) {
			validationBody.setValid(false);
			validationBody.getErrors().add("A nota atribuida foi <" + nota.getNota() + "> o minimo aceitado é 0!");
		}
		
	}

	private void notaHasChamado(NotaDTO nota, ValidationBody validationBody) {
		if(nota.getChamadoId() == null) {
			validationBody.setValid(false);
			validationBody.getErrors().add("A nota informado não está vinculada a um chamado!");
		}
	}
	
}
