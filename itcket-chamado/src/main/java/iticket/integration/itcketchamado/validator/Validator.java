package iticket.integration.itcketchamado.validator;

import java.util.List;
import java.util.stream.Collectors;

import iticket.integration.itcketchamado.exception.InvalidDataException;


public class Validator {

	protected void throwIfNull(Object obj, String message) {
		if(obj == null) {
			throw new InvalidDataException(message);
		}
	}
	
	protected void throwIfEmpty(byte[] bytes, String message) {
		if(bytes.length == 0) {
			throw new InvalidDataException(message);
		}
	}
	
	protected String joinErrors(List<String> errors) {
		return errors.stream().collect(Collectors.joining("-"));
	}
	
}
