package iticket.integration.itcketchamado.validator;

import java.util.ArrayList;

import org.springframework.stereotype.Component;

import iticket.integration.itcketchamado.domain.ValidationBody;
import iticket.integration.itcketchamado.dto.MensagemDTO;
import iticket.integration.itcketchamado.exception.mensagem.MissingMensagemDataException;

@Component
public class MensagemValidator extends Validator {

	public void validateNewMessage(MensagemDTO mensagem) {
		
		throwIfNull(mensagem, "Por Favor informe uma mensagem valida no request body!");
		
		ValidationBody validationBody = new ValidationBody(true, new ArrayList<>());
		
		mensagemDoesNotHaveChamado(mensagem, validationBody);
		mensagemDoesNotHaveConteudo(mensagem, validationBody);
		mensagemDoesNotHaveUsuario(mensagem, validationBody);
		
		if(!validationBody.isValid()) {
			throw new MissingMensagemDataException(joinErrors(validationBody.getErrors()));
		}
	}

	public void validateUpdateMessage(MensagemDTO mensagem) {
		
		throwIfNull(mensagem, "Por Favor informe uma mensagem valida no request body!");
		
		ValidationBody validationBody = new ValidationBody(true, new ArrayList<>());
		
		mensagemDoesNotHaveChamado(mensagem, validationBody);
		mensagemDoesNotHaveConteudo(mensagem, validationBody);
		mensagemDoesNotHaveUsuario(mensagem, validationBody);
		mensagemDoesNotHaveId(mensagem, validationBody);
		
		if(!validationBody.isValid()) {
			throw new MissingMensagemDataException(joinErrors(validationBody.getErrors()));
		}
		
	}
	
	private void mensagemDoesNotHaveId(MensagemDTO mensagem, ValidationBody validationBody) {

		if(mensagem.getId() == null) {
			validationBody.setValid(false);
			validationBody.getErrors().add("Para realizar a atualização, por favor informe o id da mensagem que deseja atualizar!");
		}
		
	}

	private void mensagemDoesNotHaveUsuario(MensagemDTO mensagem, ValidationBody validationBody) {

		if(mensagem.getUsuarioId() == null) {
			validationBody.setValid(false);
			validationBody.getErrors().add("A mensagem precisa ter um usuario escritor!");
		}
		
	}

	private void mensagemDoesNotHaveConteudo(MensagemDTO mensagem, ValidationBody validationBody) {

		if(mensagem.getConteudo() == null || mensagem.getConteudo().isEmpty()) {
			validationBody.setValid(false);
			validationBody.getErrors().add("A mensagme precisa possuir um conteudo!");
		}
		
	}

	private void mensagemDoesNotHaveChamado(MensagemDTO mensagem, ValidationBody validationBody) {
		
		if(mensagem.getChamadoId() == null) {
			validationBody.setValid(false);
			validationBody.getErrors().add("A mensagem precisa estar vinculada a um chamado!");
		}
		
	}

}
