package iticket.integration.itcketchamado.validator;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import iticket.integration.itcketchamado.constants.ChamadoStatus;
import iticket.integration.itcketchamado.domain.ChamadoRequest;
import iticket.integration.itcketchamado.domain.UpdateStatusRequest;
import iticket.integration.itcketchamado.domain.ValidationBody;
import iticket.integration.itcketchamado.exception.chamado.MissingChamadoDataException;
import iticket.integration.itcketchamado.repository.SubCategoriaRepository;
import iticket.integration.itcketchamado.repository.UsuarioRepository;

@Component
public class ChamadoValidator extends Validator{
	
	@Autowired
	private SubCategoriaRepository subCategoriaRepository;
	
	@Autowired
	private UsuarioRepository usuarioRepository;

	public void validateChamadoUpdateStatus(UpdateStatusRequest request) {
		
		throwIfNull(request, "Por favor, informe um body request valido!");
		
		ValidationBody validationBody = new ValidationBody(true, new ArrayList<>());

		validateChamadoIdIsPresent(request, validationBody);
		validateStatusIsNotEmpty(request, validationBody);
		validateStatusIsValid(request, validationBody);
		
		if(!validationBody.isValid()) {
			throw new MissingChamadoDataException(joinErrors(validationBody.getErrors()));
		}
	}

	private void validateChamadoIdIsPresent(UpdateStatusRequest request, ValidationBody validationBody) {

		if(request.getId() == null) {
			validationBody.setValid(false);
			validationBody.getErrors().add("O id do chamado está nulo!");
		}
		
	}

	private void validateStatusIsNotEmpty(UpdateStatusRequest request, ValidationBody validationBody) {

		if(request.getStatus() == null || request.getStatus().isEmpty()) {
			validationBody.setValid(false);
			validationBody.getErrors().add("Status está nulo ou vazio!");
		}
		
	}

	private void validateStatusIsValid(UpdateStatusRequest request, ValidationBody validationBody) {

		if(!ChamadoStatus.isValidStatus(request.getStatus())) {
			validationBody.setValid(false);
			validationBody.getErrors().add("O status <" + request.getStatus() + "> não é um status válido!");
		}
		
	}

	public void validateChamdoCreate(ChamadoRequest request) {

		throwIfNull(request, "Por Favor, Informe um corpo de requisição válido!");
		
		ValidationBody validationBody = new ValidationBody(true, new ArrayList<>());
		
		validateSubCategoria(request, validationBody);
		validateClient(request, validationBody);
		valiteTitulo(request, validationBody);
		validateDescricao(request, validationBody);
		validateIdNotNull(request.getSedeId(), validationBody, "Por Favor, informe a sede");
		validateIdNotNull(request.getSetorId(), validationBody, "Por Favor, informe o setor");
		
		if(!validationBody.isValid()) {
			throw new MissingChamadoDataException(joinErrors(validationBody.getErrors()));
		}
		
	}

	private void validateIdNotNull(Long id, ValidationBody validationBody, String msg) {

		if(id == null) {
			validationBody.setValid(false);
			validationBody.getErrors().add(msg);
		}
		
	}

	private void validateDescricao(ChamadoRequest request, ValidationBody validationBody) {

		if(request.getDescricao() == null || request.getDescricao().isEmpty()) {
			validationBody.setValid(false);
			validationBody.getErrors().add("Por favor, informe a descricao!");
		}
		
	}

	private void valiteTitulo(ChamadoRequest request, ValidationBody validationBody) {

		if(request.getTitulo() == null || request.getTitulo().isEmpty()) {
			validationBody.setValid(false);
			validationBody.getErrors().add("Por favor, informe o titulo!");
		}
		
	}

	private void validateClient(ChamadoRequest request, ValidationBody validationBody) {

		if(request.getClienteId() == null) {
			validationBody.setValid(false);
			validationBody.getErrors().add("Por favor, informe o id do cliente!");
			return;
		}
		
		if(!usuarioRepository.existsById(request.getClienteId())) {
			validationBody.setValid(false);
			validationBody.getErrors().add("Por favor, informe um cliente válido!");
		}
		
	}

	private void validateSubCategoria(ChamadoRequest request, ValidationBody validationBody) {

		if(request.getSubCategoriaId() == null) {
			validationBody.setValid(false);
			validationBody.getErrors().add("Por favor, informe o id da sub categoria!");
			return;
		}
		
		if(!subCategoriaRepository.existsById(request.getSubCategoriaId())) {
			validationBody.setValid(false);
			validationBody.getErrors().add("Por favor, informe uma sub categoria válida!");
		}
	}
	
	
}
