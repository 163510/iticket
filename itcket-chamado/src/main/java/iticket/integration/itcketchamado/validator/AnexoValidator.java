package iticket.integration.itcketchamado.validator;

import java.io.IOException;
import java.util.ArrayList;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import iticket.integration.itcketchamado.domain.ValidationBody;
import iticket.integration.itcketchamado.exception.anexo.MissingAnexoDataException;

@Component
public class AnexoValidator extends Validator{

	
	public void validateNewFile(MultipartFile file, Long chamadoId, Long messageId) throws IOException {
		
		throwIfNull(file, "É necessário informar um arquivo!");
		throwIfEmpty(file.getBytes(), "O arquivo deve possuir conteudo!");
		
		ValidationBody validationBody = new ValidationBody(true, new ArrayList<>());
		
		fileDoesNotHaveChamadoOrMessage(validationBody, chamadoId, messageId);
		fileHasChamadoAndMessage(validationBody, chamadoId, messageId);
		
		if(!validationBody.isValid()) {
			throw new MissingAnexoDataException(joinErrors(validationBody.getErrors()));
		}
	}

	private void fileHasChamadoAndMessage(ValidationBody validationBody, Long chamadoId, Long messageId) {
		
		if(chamadoId != null && messageId != null) {
			validationBody.setValid(false);
			validationBody.getErrors().add("O arquivo não pode estar vinculado a um chamado e uma mensagem ao mesmo tempo!");
		}
		
	}

	private void fileDoesNotHaveChamadoOrMessage(ValidationBody validationBody, Long chamadoId, Long messageId) {

		if(chamadoId == null && messageId == null) {
			validationBody.setValid(false);
			validationBody.getErrors().add("O arquivo precisa estar vinculado a um chamado ou a uma mensagem!");
		}
		
	}
	
	
}
