package iticket.integration.itcketchamado.scheduler;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import iticket.integration.itcketchamado.service.ChamadoService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ChamadoScheduler {

	@Autowired
	private ChamadoService service;
	
	@Scheduled(cron = "* * 1 * * *")
	private void runConcludeSchedule() {
		try {
			LocalDateTime startDate = LocalDateTime.now();
			log.info("runConcludeSchedule() - [START] - start date <{}>", startDate);
			
			service.concludefinishedChamados();
			
			log.info("runConcludeSchedule() - [END] - start date <{}> - end date <{}>", startDate, LocalDateTime.now());
		}catch (Exception e) {
			log.error("runConcludeSchedule() - [ERROR] - Exception - message <{}>", e.getMessage());
		}
	}
	
}
