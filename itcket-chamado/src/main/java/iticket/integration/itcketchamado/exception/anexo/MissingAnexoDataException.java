package iticket.integration.itcketchamado.exception.anexo;

public class MissingAnexoDataException extends RuntimeException {

	private static final long serialVersionUID = -8392944607245181102L;

	public MissingAnexoDataException() {
		super();
	}
	
	public MissingAnexoDataException(String msg) {
		super(msg);
	}
}
