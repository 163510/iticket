package iticket.integration.itcketchamado.exception.mensagem;

public class MissingMensagemDataException extends RuntimeException {

	private static final long serialVersionUID = -6587006127879905702L;

	public MissingMensagemDataException() {
		super();
	}
	
	public MissingMensagemDataException(String msg) {
		super(msg);
	}
}
