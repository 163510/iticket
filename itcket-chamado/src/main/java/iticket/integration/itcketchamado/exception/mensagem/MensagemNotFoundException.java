package iticket.integration.itcketchamado.exception.mensagem;

public class MensagemNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -2522336766472454501L;

	public MensagemNotFoundException() {
		super();
	}

	public MensagemNotFoundException(String msg) {
		super(msg);
	}
}
