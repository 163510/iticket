package iticket.integration.itcketchamado.exception.chamado;

public class ChamadoUpdateException extends RuntimeException {

	private static final long serialVersionUID = -1977777038362342816L;

	public ChamadoUpdateException() {
		super();
	}
	
	public ChamadoUpdateException(String msg) {
		super(msg);
	}
}
