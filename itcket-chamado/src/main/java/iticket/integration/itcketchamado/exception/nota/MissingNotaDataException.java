package iticket.integration.itcketchamado.exception.nota;

public class MissingNotaDataException extends RuntimeException {

	private static final long serialVersionUID = -3436945470653853646L;

	public MissingNotaDataException() {
		super();
	}
	
	public MissingNotaDataException(String msg) {
		super(msg);
	}
}
