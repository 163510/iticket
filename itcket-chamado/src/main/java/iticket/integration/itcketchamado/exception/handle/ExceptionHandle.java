package iticket.integration.itcketchamado.exception.handle;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import feign.FeignException;
import iticket.integration.itcketchamado.domain.Response;
import iticket.integration.itcketchamado.exception.InvalidDataException;
import iticket.integration.itcketchamado.exception.anexo.AnexoNotFoundException;
import iticket.integration.itcketchamado.exception.anexo.MissingAnexoDataException;
import iticket.integration.itcketchamado.exception.chamado.ChamadoNotFoundException;
import iticket.integration.itcketchamado.exception.chamado.ChamadoUpdateException;
import iticket.integration.itcketchamado.exception.chamado.MissingChamadoDataException;
import iticket.integration.itcketchamado.exception.mensagem.MensagemNotFoundException;
import iticket.integration.itcketchamado.exception.mensagem.MissingMensagemDataException;
import iticket.integration.itcketchamado.exception.nota.ChamadoHasNotaException;
import iticket.integration.itcketchamado.exception.nota.MissingNotaDataException;
import iticket.integration.itcketchamado.exception.nota.NotaNotFoundException;
import iticket.integration.itcketchamado.utils.JsonUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestControllerAdvice
public class ExceptionHandle {

	// ###########################
	// #### EXCEPTIONS GERAIS ####
	// ###########################
	
	@ExceptionHandler(Exception.class)
	public ResponseEntity<Response> handleException(WebRequest request, Exception e) {
		log.error("handleException() - [ERROR] - Exception - request path <{}> - message <{}> - error: ", request.getContextPath(), e.getMessage(), e);
		Response response = new Response(false, Arrays.asList(e.getMessage()));
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
	}
	
	@ExceptionHandler(FeignException.class)
	public ResponseEntity<String> handleFeignException(FeignException e) {
		log.error("handleFeignException() - [ERROR] - FeignException - message <{}> - error: ", e.getMessage(), e);
		return ResponseEntity.status(e.status()).body(e.contentUTF8());
	}
	
	@ExceptionHandler(InvalidDataException.class)
	public ResponseEntity<Response> handleInvalidDataException(WebRequest request, InvalidDataException e) {
		log.error("handleInvalidDataException() - [ERROR] - InvalidDataException - request path <{}> - message <{}>", request.getContextPath(), e.getMessage());
		Response response = new Response(false, Arrays.asList(e.getMessage()));
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
	}
	
	@ExceptionHandler(IOException.class)
	public ResponseEntity<Response> handleIOException(WebRequest request, Exception e) {
		log.error("handleIOException() - [ERROR] - IOException - request path <{}> - message <{}> - error: ", request.getContextPath(), e.getMessage(), e);
		Response response = new Response(false, Arrays.asList(e.getMessage()));
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
	}
	
	// ###########################
	// ##### EXCEPTIONS NOTA #####
	// ###########################
	
	@ExceptionHandler(NotaNotFoundException.class)
	public ResponseEntity<Response> handleNotaNotFoundException(WebRequest request, NotaNotFoundException e) {
		log.info("handleNotaNotFoundException() - [INFO] - NotaNotFoundException - request path <{}> - message <{}>", request.getContextPath(), e.getMessage());
		Response response = new Response(false, Arrays.asList(e.getMessage()));
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
	}
	
	@ExceptionHandler(ChamadoHasNotaException.class)
	public ResponseEntity<Response> handleChamadoHasNotaException(WebRequest request, ChamadoHasNotaException e) {
		log.info("handleChamadoHasNotaException() - [INFO] - ChamadoHasNotaException - request path <{}> - message <{}>", request.getContextPath(), e.getMessage());
		Response response = new Response(false, Arrays.asList(e.getMessage()));
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
	}
	
	@ExceptionHandler(MissingNotaDataException.class)
	public ResponseEntity<Response> handleMissingNotaDataException(WebRequest request, MissingNotaDataException e) {
		List<String> errors = Arrays.asList(e.getMessage().split("-"));
		log.info("handleMissingNotaDataException() - [INFO] - MissingNotaDataException - request path <{}> - messages <{}>", request.getContextPath(), JsonUtils.safeWriteValueAsString(errors));
		Response response = new Response(false, errors);
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
	}
	
	// ###########################
	// ### EXCEPTIONS CHAMADO ####
	// ###########################
	
	@ExceptionHandler(ChamadoNotFoundException.class)
	public ResponseEntity<Response> handleChamadoNotFoundException(WebRequest request, ChamadoNotFoundException e) {
		log.info("handleChamadoNotFoundException() - [INFO] - ChamadoNotFoundException - request path <{}> - message <{}>", request.getContextPath(), e.getMessage());
		Response response = new Response(false, Arrays.asList(e.getMessage()));
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
	}
	
	@ExceptionHandler(ChamadoUpdateException.class)
	public ResponseEntity<Response> handleChamadoUpdateException(WebRequest request, ChamadoUpdateException e) {
		log.info("handleChamadoUpdateException() - [INFO] - ChamadoUpdateException - request path <{}> - message <{}>", request.getContextPath(), e.getMessage());
		Response response = new Response(false, Arrays.asList(e.getMessage()));
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
	}
	
	@ExceptionHandler(MissingChamadoDataException.class)
	public ResponseEntity<Response> handleMissingChamadoDataException(MissingChamadoDataException e) {
		List<String> errors = Arrays.asList(e.getMessage().split("-"));
		log.info("handleMissingChamadoDataException() - [INFO] - MissingNotaDataException - messages <{}>", JsonUtils.safeWriteValueAsString(errors));
		Response response = new Response(false, errors);
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
	}
	
	// ###########################
	// #### EXCEPTIONS ANEXO #####
	// ###########################
	
	@ExceptionHandler(MissingAnexoDataException.class)
	public ResponseEntity<Response> handleMissingAnexoDataException(WebRequest request, MissingAnexoDataException e) {
		List<String> errors = Arrays.asList(e.getMessage().split("-"));
		log.info("handleMissingAnexoDataException() - [INFO] - MissingAnexoDataException - request path <{}> - messages <{}>", request.getContextPath(), JsonUtils.safeWriteValueAsString(errors));
		Response response = new Response(false, errors);
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
	}
	
	@ExceptionHandler(AnexoNotFoundException.class)
	public ResponseEntity<Response> handleAnexoNotFoundException(WebRequest request, AnexoNotFoundException e) {
		List<String> errors = Arrays.asList(e.getMessage().split("-"));
		log.info("handleMissingAnexoDataException() - [INFO] - AnexoNotFoundException - request path <{}> - messages <{}>", request.getContextPath(), e.getMessage());
		Response response = new Response(false, errors);
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
	}
	
	// ###########################
	// ### EXCEPTIONS MENSAGEM ###
	// ###########################
	
	@ExceptionHandler(MissingMensagemDataException.class)
	public ResponseEntity<Response> handleMissingMensagemDataException(WebRequest request, MissingMensagemDataException e) {
		List<String> errors = Arrays.asList(e.getMessage().split("-"));
		log.info("handleMissingMensagemDataException() - [INFO] - MissingMensagemDataException - request path <{}> - messages <{}>", request.getContextPath(), JsonUtils.safeWriteValueAsString(errors));
		Response response = new Response(false, errors);
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
	}
	
	@ExceptionHandler(MensagemNotFoundException.class)
	public ResponseEntity<Response> handleMensagemNotFoundException(WebRequest request, MensagemNotFoundException e) {
		log.info("handleMensagemNotFoundException() - [INFO] - MensagemNotFoundException - request path <{}> - message <{}>", request.getContextPath(), e.getMessage());
		Response response = new Response(false, Arrays.asList(e.getMessage()));
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
	}
}
