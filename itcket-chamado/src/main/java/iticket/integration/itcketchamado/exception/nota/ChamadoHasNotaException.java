package iticket.integration.itcketchamado.exception.nota;

public class ChamadoHasNotaException extends RuntimeException {

	private static final long serialVersionUID = 1461469225104397036L;

	public ChamadoHasNotaException() {
		super();
	}
	
	public ChamadoHasNotaException(String msg) {
		super(msg);
	}
	
}
