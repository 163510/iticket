package iticket.integration.itcketchamado.exception;

public class InvalidDataException extends RuntimeException {

	private static final long serialVersionUID = 8236432980643446951L;

	public InvalidDataException() {
		super();
	}
	
	public InvalidDataException(String msg) {
		super(msg);
	}
	
}
