package iticket.integration.itcketchamado.exception.chamado;

public class MissingChamadoDataException extends RuntimeException {

	private static final long serialVersionUID = 5601703274115131336L;

	public MissingChamadoDataException() {
		super();
	}
	
	public MissingChamadoDataException(String msg) {
		super(msg);
	}
}
