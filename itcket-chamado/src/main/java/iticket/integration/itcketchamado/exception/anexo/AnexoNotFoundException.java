package iticket.integration.itcketchamado.exception.anexo;

public class AnexoNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -317604897033621008L;

	public AnexoNotFoundException() {
		super();
	}
	
	public AnexoNotFoundException(String msg) {
		super(msg);
	}
}
