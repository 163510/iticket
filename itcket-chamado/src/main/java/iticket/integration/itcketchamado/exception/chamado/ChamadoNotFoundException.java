package iticket.integration.itcketchamado.exception.chamado;

public class ChamadoNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -4857560255792187360L;

	public ChamadoNotFoundException() {
		super();
	}
	
	public ChamadoNotFoundException(String msg) {
		super(msg);
	}
}
