package iticket.integration.itcketchamado.exception.nota;

public class NotaNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -7710866354034433134L;

	public NotaNotFoundException() {
		super();
	}
	
	public NotaNotFoundException(String msg) {
		super(msg);
	}
}
