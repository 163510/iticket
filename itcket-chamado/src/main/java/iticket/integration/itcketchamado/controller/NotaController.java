package iticket.integration.itcketchamado.controller;

import static iticket.integration.itcketchamado.constants.PermissionRoles.ADMIN;
import static iticket.integration.itcketchamado.constants.PermissionRoles.USUARIO;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import iticket.integration.itcketchamado.client.UsuarioClient;
import iticket.integration.itcketchamado.domain.AtendenteAverage;
import iticket.integration.itcketchamado.domain.AverageNotaResponse;
import iticket.integration.itcketchamado.domain.Response;
import iticket.integration.itcketchamado.dto.NotaDTO;
import iticket.integration.itcketchamado.service.NotaService;

@RestController
public class NotaController {

	@Autowired
	private NotaService notaService;
	
	@Autowired
	private UsuarioClient usuarioClient;
	
	private static final String APIKEY = "NkkkcmpSbmV2ZDQzTVI3byoycFdiTk1fMmFMVmVTLWZIcjZMU2dGQ0tHNUU0RUZGTiU=";
	
	@GetMapping(path = "/nota", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<NotaDTO>> findAll(@RequestHeader("Authorization") String token) {
		
		usuarioClient.validateUserToken(token, APIKEY, Arrays.asList(ADMIN.getRole()));
		
		return ResponseEntity.status(HttpStatus.OK).body(notaService.findAll());
	}
	
	@GetMapping(path = "/nota/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<NotaDTO> findById(@PathVariable("id") Long id, @RequestHeader("Authorization") String token) {
		
		usuarioClient.validateUserToken(token, APIKEY, Arrays.asList(ADMIN.getRole()));
		
		return ResponseEntity.status(HttpStatus.OK).body(notaService.findById(id));
	}
	
	@GetMapping(path = "/nota/atendente/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<NotaDTO>> findByAtendente(@PathVariable("id") Long id, @RequestHeader("Authorization") String token) {
		
		usuarioClient.validateUserToken(token, APIKEY, Arrays.asList(ADMIN.getRole()));
		
		return ResponseEntity.status(HttpStatus.OK).body(notaService.findByAtendenteId(id));
	}
	
	@GetMapping(path = "/nota/average", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AverageNotaResponse> findAverageNota(@RequestParam("days") Long days, @RequestHeader("Authorization") String token) {
		
		usuarioClient.validateUserToken(token, APIKEY, Arrays.asList(ADMIN.getRole()));
		
		return ResponseEntity.status(HttpStatus.OK).body(notaService.calculeAverageNota(days, token));
	}
	
	@GetMapping(path = "/nota/average/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AtendenteAverage> findAverageNotaByAtendente(
			@RequestParam("days") Long days, 
			@PathVariable("id") Long atendenteId, 
			@RequestHeader("Authorization") String token) {
		
		usuarioClient.validateUserToken(token, APIKEY, Arrays.asList(ADMIN.getRole()));
		
		return ResponseEntity.status(HttpStatus.OK).body(notaService.calculeAverageAtendenteNota(days, atendenteId, token));
	}
	
	@PostMapping(path = "/nota", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response> saveNota(@RequestHeader("Authorization") String token, @RequestBody NotaDTO nota) {
		
		usuarioClient.validateUserToken(token, APIKEY, Arrays.asList(USUARIO.getRole()));
		
		notaService.saveNota(nota);
		
		return ResponseEntity.status(HttpStatus.CREATED).body(new Response(true, new ArrayList<>()));
	}
}
