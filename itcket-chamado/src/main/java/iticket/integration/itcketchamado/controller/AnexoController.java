package iticket.integration.itcketchamado.controller;

import static iticket.integration.itcketchamado.constants.PermissionRoles.ATENDENTE;
import static iticket.integration.itcketchamado.constants.PermissionRoles.USUARIO;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import iticket.integration.itcketchamado.client.UsuarioClient;
import iticket.integration.itcketchamado.domain.Response;
import iticket.integration.itcketchamado.dto.AnexoDTO;
import iticket.integration.itcketchamado.service.AnexoService;

@RestController
public class AnexoController {

	@Autowired
	private AnexoService anexoService;
	
	@Autowired
	private UsuarioClient usuarioClient;
	
	private static final String APIKEY = "NkkkcmpSbmV2ZDQzTVI3byoycFdiTk1fMmFMVmVTLWZIcjZMU2dGQ0tHNUU0RUZGTiU=";
	
	@PostMapping("/anexo")
	public ResponseEntity<Object> saveAnexo(
			@RequestParam("file") MultipartFile file,
			@RequestParam(value = "chamadoId", required = false) Long chamadoId,
			@RequestParam(value = "mensagemId", required = false) Long mensagemId,
			@RequestHeader("Authorization") String token) throws IOException {
	
		usuarioClient.validateUserToken(token, APIKEY, Arrays.asList(ATENDENTE.getRole(), USUARIO.getRole()));
		
		anexoService.saveAnexo(file, chamadoId, mensagemId);
		
		return ResponseEntity.status(HttpStatus.CREATED).body(new Response(true, new ArrayList<>()));
	}
	
	@GetMapping("/chamado/anexos")
	public ResponseEntity<List<AnexoDTO>> findAnexosByChamadoId(
			@RequestParam("chamado_id") Long chamadoId,
			@RequestHeader("Authorization") String token) {
		
		usuarioClient.validateUserToken(token, APIKEY, Arrays.asList(ATENDENTE.getRole(), USUARIO.getRole()));
		
		List<AnexoDTO> anexos = anexoService.findByChamadoId(chamadoId);
		
		return ResponseEntity.status(HttpStatus.OK).body(anexos);
	}
	
	@GetMapping("/message/anexos")
	public ResponseEntity<List<AnexoDTO>> findAnexosByMessageId(
			@RequestParam("message_id") Long messageId,
			@RequestHeader("Authorization") String token) {
		
		usuarioClient.validateUserToken(token, APIKEY, Arrays.asList(ATENDENTE.getRole(), USUARIO.getRole()));
		
		List<AnexoDTO> anexos = anexoService.finByMessageId(messageId);
		
		return ResponseEntity.status(HttpStatus.OK).body(anexos);
	}
	
	@GetMapping("/anexo")
	public ResponseEntity<ByteArrayResource> downloadAnexo(
			@RequestParam("anexo_id") Long anexoId,
			@RequestHeader("Authorization") String token){
		
		usuarioClient.validateUserToken(token, APIKEY, Arrays.asList(ATENDENTE.getRole(), USUARIO.getRole()));
		
		return anexoService.downloadFile(anexoId);
	}
	
	@GetMapping("/anexos")
	public ResponseEntity<List<AnexoDTO>> findAll(
			@RequestHeader("Authorization") String token) {
		
		usuarioClient.validateUserToken(token, APIKEY, Arrays.asList(ATENDENTE.getRole(), USUARIO.getRole()));
		
		List<AnexoDTO> anexos = anexoService.findAll();
		
		return ResponseEntity.status(HttpStatus.OK).body(anexos);
	}
	
	@DeleteMapping("/anexo/{id}/delete")
	public ResponseEntity<Response> anexoDelete(
			@PathVariable("id") Long id,
			@RequestHeader("Authorization") String token) {
		
		usuarioClient.validateUserToken(token, APIKEY, Arrays.asList(ATENDENTE.getRole(), USUARIO.getRole()));
		
		anexoService.deleteAnexo(id);
		
		return ResponseEntity.status(HttpStatus.OK).body(new Response(true, new ArrayList<>()));
	}
}
