package iticket.integration.itcketchamado.controller;

import static iticket.integration.itcketchamado.constants.PermissionRoles.ATENDENTE;
import static iticket.integration.itcketchamado.constants.PermissionRoles.USUARIO;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import iticket.integration.itcketchamado.client.UsuarioClient;
import iticket.integration.itcketchamado.domain.Response;
import iticket.integration.itcketchamado.dto.MensagemDTO;
import iticket.integration.itcketchamado.service.MensagemService;

@RestController
public class MensagemController {

	@Autowired
	private MensagemService mensagemService;
	
	@Autowired
	private UsuarioClient usuarioClient;
	
	private static final String APIKEY = "NkkkcmpSbmV2ZDQzTVI3byoycFdiTk1fMmFMVmVTLWZIcjZMU2dGQ0tHNUU0RUZGTiU=";
	
	@GetMapping("/mensagem/chamado")
	public ResponseEntity<List<MensagemDTO>> findChamadoMensagens(@RequestParam("chamado_id") Long chamadoId, @RequestHeader("Authorization") String token) {
		
		usuarioClient.validateUserToken(token, APIKEY, Arrays.asList(ATENDENTE.getRole(), USUARIO.getRole()));
		
		List<MensagemDTO> mensagens = mensagemService.findMenssagesByChamado(chamadoId, token);
		
		return ResponseEntity.status(HttpStatus.OK).body(mensagens);
	}
	
	@PostMapping("/mensagem")
	public ResponseEntity<Response> addMessage(@RequestBody MensagemDTO mensagem, @RequestHeader("Authorization") String token) {
		
		usuarioClient.validateUserToken(token, APIKEY, Arrays.asList(ATENDENTE.getRole(), USUARIO.getRole()));
		
		mensagemService.addMessage(mensagem);
		
		return ResponseEntity.status(HttpStatus.OK).body(new Response(true, new ArrayList<>()));
	}
	
	@PutMapping("/mensagem")
	public ResponseEntity<Response> updateMessage(@RequestBody MensagemDTO mensagem, @RequestHeader("Authorization") String token) {
		
		usuarioClient.validateUserToken(token, APIKEY, Arrays.asList(ATENDENTE.getRole(), USUARIO.getRole()));
		
		mensagemService.updateMessage(mensagem);
	
		return ResponseEntity.status(HttpStatus.OK).body(new Response(true, new ArrayList<>()));
	}
	
	@DeleteMapping("/message/{id}/delete")
	public ResponseEntity<Response> deleteMessage(@PathParam("id") Long id, @RequestHeader("Authorization") String token) {
		
		usuarioClient.validateUserToken(token, APIKEY, Arrays.asList(ATENDENTE.getRole(), USUARIO.getRole()));
		
		mensagemService.deleteMensagem(id);
		
		return ResponseEntity.status(HttpStatus.OK).body(new Response(true, new ArrayList<>()));
	}
}
