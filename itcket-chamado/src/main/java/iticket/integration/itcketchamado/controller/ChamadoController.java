package iticket.integration.itcketchamado.controller;

import static iticket.integration.itcketchamado.constants.PermissionRoles.ADMIN;
import static iticket.integration.itcketchamado.constants.PermissionRoles.ATENDENTE;
import static iticket.integration.itcketchamado.constants.PermissionRoles.USUARIO;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import iticket.integration.itcketchamado.client.UsuarioClient;
import iticket.integration.itcketchamado.domain.ChamadoRequest;
import iticket.integration.itcketchamado.domain.ChamadosBySedeSetorResponse;
import iticket.integration.itcketchamado.domain.ChamadosBySubCategoria;
import iticket.integration.itcketchamado.domain.Response;
import iticket.integration.itcketchamado.domain.UpdateChamado;
import iticket.integration.itcketchamado.domain.UpdateStatusRequest;
import iticket.integration.itcketchamado.dto.ChamadoDTO;
import iticket.integration.itcketchamado.dto.ChamadoDetalheDTO;
import iticket.integration.itcketchamado.service.ChamadoService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class ChamadoController {

	@Autowired
	private ChamadoService chamadoService;
	
	@Autowired
	private UsuarioClient usuarioClient;
	
	private static final String APIKEY = "NkkkcmpSbmV2ZDQzTVI3byoycFdiTk1fMmFMVmVTLWZIcjZMU2dGQ0tHNUU0RUZGTiU=";
	
	@GetMapping("/chamados")
	public ResponseEntity<List<ChamadoDTO>> getChamados(@RequestHeader("Authorization") String token) {
		
		usuarioClient.validateUserToken(token, APIKEY, Arrays.asList(ATENDENTE.getRole(), USUARIO.getRole()));
		
		List<ChamadoDTO> chamados = chamadoService.getChamados();
		
		return ResponseEntity.status(HttpStatus.OK).body(chamados);
	}
	
	@GetMapping("/chamado/{id}")
	public ResponseEntity<ChamadoDetalheDTO> getChamado(@PathVariable("id") Long id, @RequestHeader("Authorization") String token) {
		
		usuarioClient.validateUserToken(token, APIKEY, Arrays.asList(ATENDENTE.getRole(), USUARIO.getRole()));
		
		return chamadoService.getChamado(id, token);
	}
	
	@PostMapping("/chamado")
	public ResponseEntity<Response> createChamado(@RequestBody ChamadoRequest request, @RequestHeader("Authorization") String token) {
		
		log.info("request: {}", request);
		
		usuarioClient.validateUserToken(token, APIKEY, Arrays.asList(USUARIO.getRole()));
		
		Response response = chamadoService.createChamado(request);
		
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
	@PutMapping("/chamado/update/status")
	public ResponseEntity<Object> updateChamadoStatus(@RequestHeader("Authorization") String token, @RequestBody UpdateStatusRequest request) {
		
		usuarioClient.validateUserToken(token, APIKEY, Arrays.asList(ATENDENTE.getRole()));
		
		chamadoService.updateChamadoStatus(request);
		
		return ResponseEntity.status(HttpStatus.OK).build();
	}
	
	@PutMapping("/chamado/update/{id}")
	public ResponseEntity<Response> updateChamado(@PathVariable("id") Long id, @RequestHeader("Authorization") String token, @RequestBody UpdateChamado request){
		log.error("request: {}", request);
		usuarioClient.validateUserToken(token, APIKEY, Arrays.asList(ATENDENTE.getRole(), USUARIO.getRole()));
		
		return chamadoService.updateChamado(id, request);
	}
	
	@PutMapping("/chamado/{id}/update/atendente/{atendente_id}")
	public ResponseEntity<Response> updateChamadoAtendente(@PathVariable("id") Long chamadoId, @PathVariable("atendente_id") Long atendenteId, @RequestHeader("Authorization") String token) {
		
		usuarioClient.validateUserToken(token, APIKEY, Arrays.asList(ATENDENTE.getRole()));
		
		return chamadoService.updateChamadoAtendente(chamadoId, atendenteId);
	}
	
	@PutMapping("/chamado/{id}/update/sede/{sede_id}")
	public ResponseEntity<Response> updateChamadoSede(@PathVariable("id") Long chamadoId, @PathVariable("sede_id") Long sedeId, @RequestHeader("Authorization") String token) {
		
		usuarioClient.validateUserToken(token, APIKEY, Arrays.asList(ATENDENTE.getRole()));
		
		return chamadoService.updateChamadoSede(chamadoId, sedeId);
	}
	
	@PutMapping("/chamado/{id}/update/setor/{setor_id}")
	public ResponseEntity<Response> updateChamadoSetor(@PathVariable("id") Long chamadoId, @PathVariable("setor_id") Long setorId, @RequestHeader("Authorization") String token) {
		
		usuarioClient.validateUserToken(token, APIKEY, Arrays.asList(ATENDENTE.getRole()));
		
		return chamadoService.updateChamadoSetor(chamadoId, setorId);
	}
	
	@GetMapping("/chamados/subcategoria")
	public ResponseEntity<List<ChamadosBySubCategoria>> findChamadosGroupedBySubcategoria(@RequestParam("days") Long days, @RequestHeader("Authorization") String token) {
		
		usuarioClient.validateUserToken(token, APIKEY, Arrays.asList(ADMIN.getRole()));
		
		return chamadoService.getChamadosGroupBySubcategoria(days, token);
	}
	
	@GetMapping("/chamados/sede/setor")
	public ResponseEntity<ChamadosBySedeSetorResponse> findChamadosGroupedBySedeAndSetor(@RequestParam("days") Long days, @RequestHeader("Authorization") String token) {
		
		usuarioClient.validateUserToken(token, APIKEY, Arrays.asList(ADMIN.getRole()));
		
		return chamadoService.getChamadosBySedeAndSetor(days, token);
	}
}
