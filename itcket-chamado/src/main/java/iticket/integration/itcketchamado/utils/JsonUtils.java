package iticket.integration.itcketchamado.utils;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

@Component
public class JsonUtils {

	private JsonUtils() {}
	
	private static final ObjectMapper MAPPER = new ObjectMapper()
			.enable(DeserializationFeature.ACCEPT_EMPTY_ARRAY_AS_NULL_OBJECT)
			.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
			.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
			.registerModule(new JavaTimeModule());
	
	public static String safeWriteValueAsString(Object obj) {
		try {
			return MAPPER.writeValueAsString(obj);
		}catch(Exception e) {
			return safeToStringObject(obj);
		}
	}
	
	private static String safeToStringObject(Object obj) {
		try {
			return obj.toString();
		}catch (Exception e) {
			return "[failed to mapper.writeValueAsString(obj)]";
		}
	}
}
