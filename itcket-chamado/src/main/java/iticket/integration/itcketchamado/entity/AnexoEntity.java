package iticket.integration.itcketchamado.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "anexo")
public class AnexoEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "chamado_id")
	private Long chamadoId;
	
	@Column(name = "message_id")
	private Long messageId;
	
	@Lob
	@JsonIgnore
	@Column(name = "arquivo")
	private byte[] arquivo;
	
	@Column(name = "nome_arquivo")
	private String nomeArquivo;
	
	@Column(name = "formato_arquivo")
	private String formatoArquivo;
	
	@Column(name = "data_criacao")
	private LocalDateTime dataCriacao;

	public AnexoEntity(Long chamadoId, Long messageId, byte[] arquivo, String nomeArquivo, String formatoArquivo) {
		this.chamadoId = chamadoId;
		this.messageId = messageId;
		this.arquivo = arquivo;
		this.nomeArquivo = nomeArquivo;
		this.formatoArquivo = formatoArquivo;
	}
	
}
