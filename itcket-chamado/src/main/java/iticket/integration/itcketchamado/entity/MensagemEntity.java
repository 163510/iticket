package iticket.integration.itcketchamado.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import iticket.integration.itcketchamado.dto.MensagemDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "mensagem")
public class MensagemEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "usuario_id")
	private Long usuarioId;
	
	@Column(name = "chamado_id")
	private Long chamadoId;
	
	@Lob
	@Column(name = "conteudo")
	private String conteudo;
	
	@Column(name = "data_criacao")
	private LocalDateTime dataCriacao;
	
	public MensagemEntity(MensagemDTO dto) {
		this.id = dto.getId();
		this.usuarioId = dto.getUsuarioId();
		this.chamadoId = dto.getChamadoId();
		this.conteudo = dto.getConteudo();
		this.dataCriacao = dto.getDataCriacao();
	}
	
}
