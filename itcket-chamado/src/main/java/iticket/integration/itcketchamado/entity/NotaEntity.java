package iticket.integration.itcketchamado.entity;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import iticket.integration.itcketchamado.dto.NotaDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "nota")
public class NotaEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "usuario_id")
	private Long usuarioId;
	
	@Column(name = "atendente_id")
	private Long atendenteId;
	
	@Column(name = "chamado_id")
	private Long chamadoId;
	
	@Column(name = "nota")
	private Long nota;
	
	@Lob
	@Column(name = "comentario")
	private String comentario;
	
	@Column(name = "data_criacao")
	private LocalDateTime dataCriacao;
	
	public NotaEntity(NotaDTO dto) {
		this.id = dto.getId();
		this.atendenteId = dto.getAtendenteId();
		this.usuarioId = dto.getUsuarioId();
		this.chamadoId = dto.getChamadoId();
		this.nota = dto.getNota();
		this.comentario = dto.getComentario();
		this.dataCriacao = formatDate(dto.getDataCriacao());
	} 
	
	private LocalDateTime formatDate(String date) {
		if(date != null) {
			DateTimeFormatter pattern = DateTimeFormatter.ofPattern("dd/MM/yyy HH:mm:ss");
			return LocalDateTime.parse(date, pattern);
		}
		return null;
	}
}
