package iticket.integration.itcketchamado.entity;

import static iticket.integration.itcketchamado.constants.ChamadoStatus.OPEN;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import iticket.integration.itcketchamado.domain.ChamadoRequest;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Table(name = "chamado")
@NoArgsConstructor
@AllArgsConstructor
public class ChamadoEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "sub_categoria_id")
	private Long subCategoriaId;
	
	@Column(name = "cliente_id")
	private Long clienteId;
	
	@Column(name = "atendente_id")
	private Long atendenteId;
	
	@Column(name = "sede_id")
	private Long sedeId;
	
	@Column(name = "setor_id")
	private Long setorId;
	
	@Column(name = "status")
	private String status;
	
	@Lob
	@Column(name = "titulo")
	private String titulo;
	
	@Lob
	@Column(name = "descricao")
	private String descricao;
	
	@Column(name = "data_criacao")
	private LocalDateTime dataCriacao;
	
	@Column(name = "data_atualizacao")
	private LocalDateTime dataAtualizacao;
	
	@Column(name = "data_finalizacao")
	private LocalDateTime dataFinalizacao;
	
	public ChamadoEntity(ChamadoRequest request) {
		this.subCategoriaId = request.getSubCategoriaId();
		this.clienteId = request.getClienteId();
		this.sedeId = request.getSedeId();
		this.setorId = request.getSetorId();
		this.titulo = request.getTitulo();
		this.descricao = request.getDescricao();
		this.status = OPEN.getStatus();
	} 
	
}
