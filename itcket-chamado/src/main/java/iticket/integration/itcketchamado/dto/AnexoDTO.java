package iticket.integration.itcketchamado.dto;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import iticket.integration.itcketchamado.entity.AnexoEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_NULL)
public class AnexoDTO {

	private Long id;
	private Long chamadoId;
	private Long messageId;
	private String nomeArquivo;
	private String formatoArquivo;
	private LocalDateTime dataCriacao;

	public AnexoDTO(AnexoEntity entity) {
		this.id = entity.getId();
		this.chamadoId = entity.getChamadoId();
		this.messageId = entity.getMessageId();
		this.nomeArquivo = entity.getNomeArquivo();
		this.formatoArquivo = entity.getFormatoArquivo();
		this.dataCriacao = entity.getDataCriacao();
	}
	
}
