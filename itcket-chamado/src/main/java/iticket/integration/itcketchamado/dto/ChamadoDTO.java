package iticket.integration.itcketchamado.dto;

import java.time.LocalDateTime;

import iticket.integration.itcketchamado.constants.ChamadoStatus;
import iticket.integration.itcketchamado.entity.ChamadoEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ChamadoDTO {

	private Long id;
	private Long subCategoriaId;
	private Long clienteId;
	private Long atendenteId;
	private Long sedeId;
	private Long setorId;
	private String status;
	private String titulo;
	private String descricao;
	private LocalDateTime dataCriacao;
	private LocalDateTime dataAtualizacao;
	private LocalDateTime dataFinalizacao;
	
	public ChamadoDTO(ChamadoEntity entity) {
		this.id = entity.getId();
		this.subCategoriaId = entity.getSubCategoriaId();
		this.clienteId = entity.getClienteId();
		this.atendenteId = entity.getAtendenteId();
		this.sedeId = entity.getSedeId();
		this.setorId = entity.getSetorId();
		this.status = ChamadoStatus.getTransalationByStatus(entity.getStatus());
		this.titulo = entity.getTitulo();
		this.descricao = entity.getDescricao();
		this.dataCriacao = entity.getDataCriacao();
		this.dataAtualizacao = entity.getDataAtualizacao();
		this.dataFinalizacao = entity.getDataAtualizacao();
	}
}
