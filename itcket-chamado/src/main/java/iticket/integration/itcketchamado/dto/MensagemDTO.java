package iticket.integration.itcketchamado.dto;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import iticket.integration.itcketchamado.entity.MensagemEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class MensagemDTO {
	
	private Long id;
	private String username;
	private Long usuarioId;
	private Long chamadoId;
	private String conteudo;
	private LocalDateTime dataCriacao;
	
	public MensagemDTO(MensagemEntity entity) {
		this.id = entity.getId();
		this.usuarioId = entity.getUsuarioId();
		this.chamadoId = entity.getChamadoId();
		this.conteudo = entity.getConteudo();
		this.dataCriacao = entity.getDataCriacao();
	}
	
	public MensagemDTO(MensagemEntity entity, String username) {
		this.id = entity.getId();
		this.username = username;
		this.usuarioId = entity.getUsuarioId();
		this.chamadoId = entity.getChamadoId();
		this.conteudo = entity.getConteudo();
		this.dataCriacao = entity.getDataCriacao();
	}
	
}
