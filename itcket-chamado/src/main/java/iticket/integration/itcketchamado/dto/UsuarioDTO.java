package iticket.integration.itcketchamado.dto;

import java.time.LocalDateTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class UsuarioDTO {

	private Long id;
	private String nome;
	private String telefone;
	private String email;
	private Long usuarioCriadorId;
	private Boolean primeiroAcesso;
	private LocalDateTime dataCriacao;
	private LocalDateTime dataAtualizacao;
	private List<GrupoDTO> grupos;
	
}
