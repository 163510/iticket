package iticket.integration.itcketchamado.dto;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import iticket.integration.itcketchamado.constants.ChamadoStatus;
import iticket.integration.itcketchamado.entity.ChamadoEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_NULL)
public class ChamadoDetalheDTO {

	private Long id;
	private UsuarioDetalhamentoDTO cliente;
	private UsuarioDetalhamentoDTO atendente;
	private SubCategoriaDetalhamentoDTO subCategoria;
	private SedeDetalhamentoDTO sede;
	private SetorDetalhamentoDTO setor;
	private String status;
	private String titulo;
	private String descricao;
	private String dataCriacao;
	private String dataAtualizacao;
	private String dataFinalizacao;
	
	
	public ChamadoDetalheDTO(UsuarioDTO atendente, UsuarioDTO cliente, SubCategoriaDTO subCategoria, SedeDTO sede, SetorDTO setor, ChamadoEntity entity) {
		this.id = entity.getId();
		this.status = ChamadoStatus.getTransalationByStatus(entity.getStatus());
		this.titulo = entity.getTitulo();
		this.descricao = entity.getDescricao();
		this.dataCriacao = formatDate(entity.getDataCriacao());
		this.dataAtualizacao = formatDate(entity.getDataAtualizacao());
		this.dataFinalizacao = formatDate(entity.getDataFinalizacao());
		this.cliente = new UsuarioDetalhamentoDTO(cliente);
		this.atendente = new UsuarioDetalhamentoDTO(atendente);
		this.subCategoria = new SubCategoriaDetalhamentoDTO(subCategoria);
		this.sede = new SedeDetalhamentoDTO(sede);
		this.setor = new SetorDetalhamentoDTO(setor);
	}
	
	private String formatDate(LocalDateTime date) {
		if(date != null) {
			DateTimeFormatter pattern = DateTimeFormatter.ofPattern("dd/MM/yyy HH:mm:ss");
			return date.format(pattern);
		}
		return "";
	}
}
