package iticket.integration.itcketchamado.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_NULL)
public class SubCategoriaDetalhamentoDTO {

	private Long id;
	private String subCategoria;
	private String categoria;
	
	public SubCategoriaDetalhamentoDTO(SubCategoriaDTO subCategoria) {
		this.id = subCategoria.getId();
		this.subCategoria = subCategoria.getNome();
		this.categoria = subCategoria.getCategoria().getNome();
	}
}
