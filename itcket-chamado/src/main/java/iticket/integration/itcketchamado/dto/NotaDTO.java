package iticket.integration.itcketchamado.dto;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import iticket.integration.itcketchamado.entity.NotaEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_NULL)
public class NotaDTO {
	
	@JsonProperty(value = "id")
	private Long id;
	
	@JsonProperty(value = "usuario_id")
	private Long usuarioId;
	
	@JsonProperty(value = "atendente_id")
	private Long atendenteId;
	
	@JsonProperty(value = "chamado_id")
	private Long chamadoId;
	
	@JsonProperty(value = "nota")
	private Long nota;
	
	@JsonProperty(value = "comentario")
	private String comentario;
	
	@JsonProperty(value = "data_criacao")
	private String dataCriacao;
	
	public NotaDTO(NotaEntity entity) {
		this.id = entity.getId();
		this.usuarioId = entity.getUsuarioId();
		this.atendenteId = entity.getAtendenteId();
		this.chamadoId = entity.getChamadoId();
		this.nota = entity.getNota();
		this.comentario = entity.getComentario();
		this.dataCriacao = formatDate(entity.getDataCriacao());
	} 
	
	private String formatDate(LocalDateTime date) {
		if(date != null) {
			DateTimeFormatter pattern = DateTimeFormatter.ofPattern("dd/MM/yyy HH:mm:ss");
			return date.format(pattern);
		}
		return "";
	}
}
