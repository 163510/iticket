package iticket.integration.itcketchamado.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_NULL)
public class SedeDetalhamentoDTO {

	private Long id;
	private String nome;
	
	public SedeDetalhamentoDTO(SedeDTO dto) {
		this.id = dto.getId();
		this.nome = dto.getNome();
	}
	
}
