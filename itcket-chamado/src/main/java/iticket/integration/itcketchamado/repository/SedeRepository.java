package iticket.integration.itcketchamado.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import iticket.integration.itcketchamado.entity.SedeEntity;

@Repository
public interface SedeRepository extends JpaRepository<SedeEntity, Long>{

}
