package iticket.integration.itcketchamado.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import iticket.integration.itcketchamado.entity.SetorEntity;

@Repository
public interface SetorRepository extends JpaRepository<SetorEntity, Long>{

}
