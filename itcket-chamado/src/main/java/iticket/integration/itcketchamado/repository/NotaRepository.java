package iticket.integration.itcketchamado.repository;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import iticket.integration.itcketchamado.entity.NotaEntity;

@Repository
public interface NotaRepository extends JpaRepository<NotaEntity, Long>{

	List<NotaEntity> findByAtendenteId(Long atendenteId);
	
	Long countByChamadoId(Long chamadoId);
	
	List<NotaEntity> findByDataCriacaoGreaterThanEqual(LocalDateTime date);
	
	List<NotaEntity> findByDataCriacaoGreaterThanEqualAndAtendenteId(LocalDateTime date, Long atendenteId);
}
