package iticket.integration.itcketchamado.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import iticket.integration.itcketchamado.entity.MensagemEntity;

@Repository
public interface MensagemRepository extends JpaRepository<MensagemEntity, Long>{

	List<MensagemEntity> findByChamadoId(Long chamadoId);
	
}
