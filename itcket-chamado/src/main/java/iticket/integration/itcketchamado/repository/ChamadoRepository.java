package iticket.integration.itcketchamado.repository;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import iticket.integration.itcketchamado.entity.ChamadoEntity;

@Repository
public interface ChamadoRepository extends JpaRepository<ChamadoEntity, Long>{

	@Query(value = "SELECT * FROM chamado WHERE status != 'CONCLUDED'", nativeQuery = true)
	List<ChamadoEntity> findChamadosNotConcluded();

	@Query(value = "SELECT * FROM chamado WHERE status IS 'FINISHED' AND DATE(DATE_ADD(data_finalizacao, INTERVAL +1 DAY)) <= CURRENT_DATE", nativeQuery = true)
	List<ChamadoEntity> findChamadosLongtimeFinished();
	
	List<ChamadoEntity> findByDataCriacaoGreaterThanEqual(LocalDateTime dataCriacao);
}
