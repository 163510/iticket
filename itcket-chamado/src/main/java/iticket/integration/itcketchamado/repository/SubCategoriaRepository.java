package iticket.integration.itcketchamado.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import iticket.integration.itcketchamado.entity.SubCategoriaEntity;

@Repository
public interface SubCategoriaRepository extends JpaRepository<SubCategoriaEntity, Long> {

}
