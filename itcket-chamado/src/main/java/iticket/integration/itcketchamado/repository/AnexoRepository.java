package iticket.integration.itcketchamado.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import iticket.integration.itcketchamado.entity.AnexoEntity;

@Repository
public interface AnexoRepository extends JpaRepository<AnexoEntity, Long>{

	List<AnexoEntity> findByChamadoId(Long chamadoId);
	List<AnexoEntity> findByMessageId(Long messageId);
}
