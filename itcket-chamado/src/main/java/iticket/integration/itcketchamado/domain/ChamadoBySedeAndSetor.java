package iticket.integration.itcketchamado.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ChamadoBySedeAndSetor {

	private String sede;
	private String setor;
	private Integer sedeTotalChamados;
	private Integer setorTotalChamados;
	private Double porcentagemSedeDeChamados;
	private Double porcentagemSetorDeChamados;
	private Double porcentagemSetorDaSedeDeChamados;
	
}
