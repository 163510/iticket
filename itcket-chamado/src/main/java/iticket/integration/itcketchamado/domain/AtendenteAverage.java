package iticket.integration.itcketchamado.domain;

import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import iticket.integration.itcketchamado.dto.NotaDTO;
import iticket.integration.itcketchamado.dto.UsuarioDTO;
import iticket.integration.itcketchamado.entity.NotaEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_NULL)
public class AtendenteAverage {

	private Long id;
	private String nome;
	private String email;
	private Double average;
	private Integer quantity;
	private List<NotaDTO> notas;
	
	public AtendenteAverage(UsuarioDTO user, Double average) {
		this.id = user.getId();
		this.nome = user.getNome();
		this.email = user.getEmail();
		this.average = average;
	}
	
	public AtendenteAverage(UsuarioDTO user, Double average, Integer quantity) {
		this.id = user.getId();
		this.nome = user.getNome();
		this.email = user.getEmail();
		this.average = average;
		this.quantity = quantity;
	}
	
	public AtendenteAverage(UsuarioDTO user, Double average, List<NotaEntity> notas) {
		this.id = user.getId();
		this.nome = user.getNome();
		this.email = user.getEmail();
		this.average = average;
		this.quantity = notas.size();
		this.notas = notas.stream().map(NotaDTO::new).collect(Collectors.toList());
	}
	
}
