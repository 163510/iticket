package iticket.integration.itcketchamado.domain;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ValidationBody {

	private boolean valid;
	private List<String> errors;
	
}
