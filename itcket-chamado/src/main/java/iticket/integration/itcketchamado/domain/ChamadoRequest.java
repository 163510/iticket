package iticket.integration.itcketchamado.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ChamadoRequest {

	private Long subCategoriaId;
	private Long clienteId;
	private Long atendenteId;
	private Long sedeId;
	private Long setorId;
	private String titulo;
	private String descricao;
	
}
