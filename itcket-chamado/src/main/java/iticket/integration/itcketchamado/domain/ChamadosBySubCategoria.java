package iticket.integration.itcketchamado.domain;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import iticket.integration.itcketchamado.dto.SubCategoriaDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_NULL)
public class ChamadosBySubCategoria {

	private Long subCategoriaId;
	private String subCategoria;
	private String categoria;
	private Integer quantity;
	private List<ChamadoByStatus> chamadosByStatus;
	
	public ChamadosBySubCategoria(SubCategoriaDTO subCategoria, Integer quantity, List<ChamadoByStatus> chamadosByStatus) {
		this.subCategoriaId = subCategoria.getId();
		this.subCategoria = subCategoria.getNome();
		this.categoria = subCategoria.getCategoria().getNome();
		this.quantity = quantity;
		this.chamadosByStatus = chamadosByStatus;
	}
}
