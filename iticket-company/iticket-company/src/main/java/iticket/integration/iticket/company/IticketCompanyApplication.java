package iticket.integration.iticket.company;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients
@SpringBootApplication
public class IticketCompanyApplication {

	public static void main(String[] args) {
		SpringApplication.run(IticketCompanyApplication.class, args);
	}

}
