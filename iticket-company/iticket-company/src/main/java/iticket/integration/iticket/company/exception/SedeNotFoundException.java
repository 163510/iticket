package iticket.integration.iticket.company.exception;

public class SedeNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -4236068561437617063L;

	public SedeNotFoundException() {
		super();
	}
	
	public SedeNotFoundException(String msg) {
		super(msg);
	}
}
