package iticket.integration.iticket.company.client;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import iticket.integration.iticket.company.domain.Response;

@FeignClient(name = "UsuarioClient", url = "http://localhost:3031")
public interface UsuarioClient {

	@PostMapping(path = "/usuario/validate/token")
	ResponseEntity<Response> validateUserToken(
			@RequestHeader("Authorization") String token, 
			@RequestHeader("apikey") String apikey,
			@RequestParam("roles") List<String> roles);
	
}
