package iticket.integration.iticket.company.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import iticket.integration.iticket.company.domain.CreationRequest;
import iticket.integration.iticket.company.domain.Response;
import iticket.integration.iticket.company.dto.SetorDTO;
import iticket.integration.iticket.company.entity.SetorEntity;
import iticket.integration.iticket.company.exception.SetorNotFoundException;
import iticket.integration.iticket.company.repository.SetorRepository;

@Service
public class SetorService {

	@Autowired
	private SetorRepository setorRepository;
	
	public List<SetorDTO> findAll() {
		
		List<SetorEntity> setores = setorRepository.findAll();
		
		return setores.stream().map(SetorDTO::new).collect(Collectors.toList());
	}

	public ResponseEntity<SetorDTO> findById(Long id) {

		Optional<SetorEntity> optSetor = setorRepository.findById(id);
		
		if(!optSetor.isPresent()) {
			throw new SetorNotFoundException("Setor de id <" + id + "> não encontrado!");
		}
		
		return ResponseEntity.status(HttpStatus.OK).body(new SetorDTO(optSetor.get()));
	}

	public ResponseEntity<Response> createSetor(CreationRequest request) {
		Response response = new Response(true, new ArrayList<>());
		
		if(request.getNome() == null || request.getNome().trim().isEmpty()) {
			response.setSuccess(false);
			response.getErrors().add("Por favor informe um nome válido!");
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}
		
		SetorEntity entity = new SetorEntity();
		entity.setNome(request.getNome());
		
		setorRepository.save(entity);
		
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}

	public ResponseEntity<Response> updateSetor(Long id, CreationRequest request) {
		Response response = new Response(true, new ArrayList<>());
		
		if(request.getNome() == null || request.getNome().trim().isEmpty()) {
			response.setSuccess(false);
			response.getErrors().add("Por favor informe um nome válido!");
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}
		
		Optional<SetorEntity> optSetor = setorRepository.findById(id);
		
		if(!optSetor.isPresent()) {
			response.setSuccess(false);
			response.getErrors().add("Setor com id informado não encontrado!");
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}
		
		SetorEntity entity = optSetor.get();
		entity.setNome(request.getNome());
		
		setorRepository.save(entity);
		
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
	public Response deleteSetor(Long id) {
		
		setorRepository.deleteById(id);
		
		return new Response(true, new ArrayList<>());
	}
}
