package iticket.integration.iticket.company.exception;

public class SetorNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 133737472615393210L;

	public SetorNotFoundException() {
		super();
	}
	
	public SetorNotFoundException(String msg) {
		super(msg);
	}
	
}
