package iticket.integration.iticket.company.controller;

import static iticket.integration.iticket.company.constants.PermissionRoles.ATENDENTE;
import static iticket.integration.iticket.company.constants.PermissionRoles.USUARIO;
import static iticket.integration.iticket.company.constants.PermissionRoles.ADMIN;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import iticket.integration.iticket.company.client.UsuarioClient;
import iticket.integration.iticket.company.domain.CreationRequest;
import iticket.integration.iticket.company.domain.Response;
import iticket.integration.iticket.company.dto.SedeDTO;
import iticket.integration.iticket.company.service.SedeService;

@RestController
public class SedeController {

	@Autowired
	private SedeService sedeService;
	
	@Autowired
	private UsuarioClient usuarioClient;
	
	private static final String APIKEY = "NkkkcmpSbmV2ZDQzTVI3byoycFdiTk1fMmFMVmVTLWZIcjZMU2dGQ0tHNUU0RUZGTiU=";
	
	@GetMapping("/sede/all")
	public ResponseEntity<List<SedeDTO>> findAll(@RequestHeader("Authorization") String token) {
		
		usuarioClient.validateUserToken(token, APIKEY, Arrays.asList(ADMIN.getRole(), ATENDENTE.getRole(), USUARIO.getRole()));
		
		return ResponseEntity.status(HttpStatus.OK).body(sedeService.findAll());
	}
	
	@GetMapping("/sede/{id}")
	public ResponseEntity<SedeDTO> findById(@PathVariable("id") Long id, @RequestHeader("Authorization") String token) {
		
		usuarioClient.validateUserToken(token, APIKEY, Arrays.asList(ADMIN.getRole(), ATENDENTE.getRole(), USUARIO.getRole()));
		
		return sedeService.findById(id);
	}
	
	@PostMapping("/sede")
	public ResponseEntity<Response> createSetor(@RequestHeader("Authorization") String token, @RequestBody CreationRequest request) {
		
		usuarioClient.validateUserToken(token, APIKEY, Arrays.asList(ADMIN.getRole()));
		
		return sedeService.createSetor(request);
	}
	
	@PutMapping("/sede/{id}")
	public ResponseEntity<Response> updateSetor(@PathVariable("id") Long id, @RequestHeader("Authorization") String token, @RequestBody CreationRequest request) {
		
		usuarioClient.validateUserToken(token, APIKEY, Arrays.asList(ADMIN.getRole()));
		
		return sedeService.updateSetor(id, request);
	}
	
	@DeleteMapping("/sede/{id}")
	public ResponseEntity<Response> deleteSetor(@PathVariable("id") Long id, @RequestHeader("Authorization") String token) {
		
		usuarioClient.validateUserToken(token, APIKEY, Arrays.asList(ADMIN.getRole()));
		
		return ResponseEntity.status(HttpStatus.OK).body(sedeService.deleteSetor(id));
	}
	
}
