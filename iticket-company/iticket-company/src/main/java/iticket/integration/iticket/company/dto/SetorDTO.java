package iticket.integration.iticket.company.dto;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import iticket.integration.iticket.company.entity.SetorEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_NULL)
public class SetorDTO {

	private Long id;
	private String nome;
	private LocalDateTime dataCriacao;
	private LocalDateTime dataAtualizacao;
	
	public SetorDTO(SetorEntity entity) {
		this.id = entity.getId();
		this.nome = entity.getNome();
		this.dataCriacao = entity.getDataCriacao();
		this.dataAtualizacao = entity.getDataAtualizacao();
	}
}
