package iticket.integration.iticket.company.exception.handle;

import java.util.ArrayList;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import feign.FeignException;
import iticket.integration.iticket.company.domain.Response;
import iticket.integration.iticket.company.exception.SedeNotFoundException;
import iticket.integration.iticket.company.exception.SetorNotFoundException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestControllerAdvice
public class Handler {

	@ExceptionHandler(SetorNotFoundException.class)
	public ResponseEntity<Response> handleSetorNotFoundException(SetorNotFoundException snfe) {
		log.error("handleSetorNotFoundException() - [ERROR] - message <{}>", snfe.getMessage());
		Response response = new Response(false, new ArrayList<>());
		response.getErrors().add(snfe.getMessage());
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
	}
	
	@ExceptionHandler(SedeNotFoundException.class)
	public ResponseEntity<Response> handleSedeNotFoundException(SedeNotFoundException snfe) {
		log.error("handleSedeNotFoundException() - [ERROR] - message <{}>", snfe.getMessage());
		Response response = new Response(false, new ArrayList<>());
		response.getErrors().add(snfe.getMessage());
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
	}
	
	@ExceptionHandler(FeignException.class)
	public ResponseEntity<Response> handleFeignException(FeignException e) {
		log.error("handleFeignException() - [ERROR] - message <{}> - e:", e.contentUTF8(), e);
		Response response = new Response(false, new ArrayList<>());
		response.getErrors().add(e.contentUTF8());
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
	}
	
	@ExceptionHandler(Exception.class)
	public ResponseEntity<Response> handleException(Exception e) {
		log.error("handleException() - [ERROR] - message <{}> - e:", e.getMessage());
		Response response = new Response(false, new ArrayList<>());
		response.getErrors().add(e.getMessage());
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
	}
	
	
}
