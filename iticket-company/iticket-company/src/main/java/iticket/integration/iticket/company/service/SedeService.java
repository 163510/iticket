package iticket.integration.iticket.company.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import iticket.integration.iticket.company.domain.CreationRequest;
import iticket.integration.iticket.company.domain.Response;
import iticket.integration.iticket.company.dto.SedeDTO;
import iticket.integration.iticket.company.entity.SedeEntity;
import iticket.integration.iticket.company.exception.SedeNotFoundException;
import iticket.integration.iticket.company.repository.SedeRepository;

@Service
public class SedeService {

	@Autowired
	private SedeRepository sedeRepository;
	
	public List<SedeDTO> findAll() {
		
		List<SedeEntity> sedes = sedeRepository.findAll();
		
		return sedes.stream().map(SedeDTO::new).collect(Collectors.toList());
	}

	public ResponseEntity<SedeDTO> findById(Long id) {

		Optional<SedeEntity> optSede = sedeRepository.findById(id);
		
		if(!optSede.isPresent()) {
			throw new SedeNotFoundException("Sede de id <" + id + "> não encontrado!");
		}
		
		return ResponseEntity.status(HttpStatus.OK).body(new SedeDTO(optSede.get()));
	}

	public ResponseEntity<Response> createSetor(CreationRequest request) {
		Response response = new Response(true, new ArrayList<>());
		
		if(request.getNome() == null || request.getNome().trim().isEmpty()) {
			response.setSuccess(false);
			response.getErrors().add("Por favor informe um nome válido!");
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}
		
		SedeEntity entity = new SedeEntity();
		entity.setNome(request.getNome());
		
		sedeRepository.save(entity);
		
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}

	public ResponseEntity<Response> updateSetor(Long id, CreationRequest request) {
		Response response = new Response(true, new ArrayList<>());
		
		if(request.getNome() == null || request.getNome().trim().isEmpty()) {
			response.setSuccess(false);
			response.getErrors().add("Por favor informe um nome válido!");
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}
		
		Optional<SedeEntity> optSede = sedeRepository.findById(id);
		
		if(!optSede.isPresent()) {
			response.setSuccess(false);
			response.getErrors().add("Sede com id informado não encontrado!");
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}
		
		SedeEntity entity = optSede.get();
		entity.setNome(request.getNome());
		
		sedeRepository.save(entity);
		
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
	public Response deleteSetor(Long id) {
		
		sedeRepository.deleteById(id);
		
		return new Response(true, new ArrayList<>());
	}
	
}
