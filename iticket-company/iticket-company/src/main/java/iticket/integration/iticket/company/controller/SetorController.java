package iticket.integration.iticket.company.controller;

import static iticket.integration.iticket.company.constants.PermissionRoles.ADMIN;
import static iticket.integration.iticket.company.constants.PermissionRoles.ATENDENTE;
import static iticket.integration.iticket.company.constants.PermissionRoles.USUARIO;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import iticket.integration.iticket.company.client.UsuarioClient;
import iticket.integration.iticket.company.domain.CreationRequest;
import iticket.integration.iticket.company.domain.Response;
import iticket.integration.iticket.company.dto.SetorDTO;
import iticket.integration.iticket.company.service.SetorService;

@RestController
public class SetorController {

	@Autowired
	private UsuarioClient usuarioClient;
	
	@Autowired
	private SetorService setorService;
	
	private static final String APIKEY = "NkkkcmpSbmV2ZDQzTVI3byoycFdiTk1fMmFMVmVTLWZIcjZMU2dGQ0tHNUU0RUZGTiU=";
	
	@GetMapping("/setor/all")
	public ResponseEntity<List<SetorDTO>> findAll(@RequestHeader("Authorization") String token) {
		
		usuarioClient.validateUserToken(token, APIKEY, Arrays.asList(ADMIN.getRole(), ATENDENTE.getRole(), USUARIO.getRole()));
		
		return ResponseEntity.status(HttpStatus.OK).body(setorService.findAll());
	}
	
	@GetMapping("/setor/{id}")
	public ResponseEntity<SetorDTO> findById(@PathVariable("id") Long id, @RequestHeader("Authorization") String token) {
		
		usuarioClient.validateUserToken(token, APIKEY, Arrays.asList(ADMIN.getRole(), ATENDENTE.getRole(), USUARIO.getRole()));
		
		return setorService.findById(id);
	}
	
	@PostMapping("/setor")
	public ResponseEntity<Response> createSetor(@RequestHeader("Authorization") String token, @RequestBody CreationRequest request) {
		
		usuarioClient.validateUserToken(token, APIKEY, Arrays.asList(ADMIN.getRole()));
		
		return setorService.createSetor(request);
	}
	
	@PutMapping("/setor/{id}")
	public ResponseEntity<Response> updateSetor(@PathVariable("id") Long id, @RequestHeader("Authorization") String token, @RequestBody CreationRequest request) {
		
		usuarioClient.validateUserToken(token, APIKEY, Arrays.asList(ADMIN.getRole()));
		
		return setorService.updateSetor(id, request);
	}
	
	@DeleteMapping("/setor/{id}")
	public ResponseEntity<Response> deleteSetor(@PathVariable("id") Long id, @RequestHeader("Authorization") String token) {
		
		usuarioClient.validateUserToken(token, APIKEY, Arrays.asList(ADMIN.getRole()));
		
		return ResponseEntity.status(HttpStatus.OK).body(setorService.deleteSetor(id));
	}
}
