package iticket.integration.iticket.company.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import iticket.integration.iticket.company.entity.SetorEntity;

@Repository
public interface SetorRepository extends JpaRepository<SetorEntity, Long>{

}
