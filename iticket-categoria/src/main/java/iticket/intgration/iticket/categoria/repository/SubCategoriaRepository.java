package iticket.intgration.iticket.categoria.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import iticket.intgration.iticket.categoria.entity.SubCategoriaEntity;

@Repository
public interface SubCategoriaRepository extends JpaRepository<SubCategoriaEntity, Long> {

	@Query(value = "SELECT * FROM sub_categoria WHERE categoria_id = :id", nativeQuery = true)
	List<SubCategoriaEntity> findByCategoryId(Long id);
	
}
