package iticket.intgration.iticket.categoria.exception;

public class SubCategoriaNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 4809223632658500313L;

	public SubCategoriaNotFoundException() {
		super();
	}
	
	public SubCategoriaNotFoundException(String msg) {
		super(msg);
	}
}
