package iticket.intgration.iticket.categoria;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients
@SpringBootApplication
public class IticketCategoriaApplication {

	public static void main(String[] args) {
		SpringApplication.run(IticketCategoriaApplication.class, args);
	}

}
