package iticket.intgration.iticket.categoria.controller;

import static iticket.intgration.iticket.categoria.constants.PermissionRoles.ATENDENTE;
import static iticket.intgration.iticket.categoria.constants.PermissionRoles.USUARIO;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import iticket.intgration.iticket.categoria.client.UsuarioClient;
import iticket.intgration.iticket.categoria.domain.Response;
import iticket.intgration.iticket.categoria.dto.SubCategoriaDTO;
import iticket.intgration.iticket.categoria.service.SubCategoriaService;

@RestController
public class SubCategoriaController {

	@Autowired
	private SubCategoriaService subCategoriaService;
	
	@Autowired
	private UsuarioClient usuarioClient;
	
	private static final String APIKEY = "NkkkcmpSbmV2ZDQzTVI3byoycFdiTk1fMmFMVmVTLWZIcjZMU2dGQ0tHNUU0RUZGTiU=";

	@GetMapping("/subcategoria/all")
	public ResponseEntity<List<SubCategoriaDTO>> findAll(@RequestHeader("Authorization") String token) {
		usuarioClient.validateUserToken(token, APIKEY, Arrays.asList(ATENDENTE.getRole(), USUARIO.getRole()));
		List<SubCategoriaDTO> categorias = subCategoriaService.findAll();
		return ResponseEntity.status(HttpStatus.OK).body(categorias);
	}
	
	@GetMapping("/subcategoria/{id}")
	public ResponseEntity<SubCategoriaDTO> findById(@PathVariable("id") Long id, @RequestHeader("Authorization") String token) {
		usuarioClient.validateUserToken(token, APIKEY, Arrays.asList(ATENDENTE.getRole(), USUARIO.getRole()));
		return subCategoriaService.findById(id);
	}
	
	@GetMapping("/subcategoria/categoria/{id}")
	public ResponseEntity<List<SubCategoriaDTO>> findByCategoryId(@PathVariable("id") Long id, @RequestHeader("Authorization") String token) {
		usuarioClient.validateUserToken(token, APIKEY, Arrays.asList(ATENDENTE.getRole(), USUARIO.getRole()));
		return ResponseEntity.status(HttpStatus.OK).body(subCategoriaService.findByCategoryId(id));
	}
	
	@PostMapping("/subcategoria")
	public ResponseEntity<Response> saveSubCategoria(@RequestHeader("Authorization") String token, @RequestBody SubCategoriaDTO subCategoria) {
		usuarioClient.validateUserToken(token, APIKEY, Arrays.asList(ATENDENTE.getRole()));
		return subCategoriaService.save(subCategoria);
	}
	
	@PutMapping("/subcategoria/{id}")
	public ResponseEntity<Response> updateSubCategoria(@PathVariable("id") Long id, @RequestHeader("Authorization") String token, @RequestBody SubCategoriaDTO subCategoria) {
		usuarioClient.validateUserToken(token, APIKEY, Arrays.asList(ATENDENTE.getRole()));
		return subCategoriaService.updateSubCategoria(id, subCategoria);
	}
	
	@DeleteMapping("/subcategoria/{id}")
	public ResponseEntity<Response> deleteSubCategoria(@PathVariable("id") Long id, @RequestHeader("Authorization") String token) {
		usuarioClient.validateUserToken(token, APIKEY, Arrays.asList(ATENDENTE.getRole()));
		return subCategoriaService.deleteById(id);
	}
}
