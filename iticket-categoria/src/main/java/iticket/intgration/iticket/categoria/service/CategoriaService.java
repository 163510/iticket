package iticket.intgration.iticket.categoria.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import iticket.intgration.iticket.categoria.domain.Response;
import iticket.intgration.iticket.categoria.domain.categoria.CategoriaRequest;
import iticket.intgration.iticket.categoria.dto.CategoriaDTO;
import iticket.intgration.iticket.categoria.entity.CategoriaEntity;
import iticket.intgration.iticket.categoria.exception.CategoriaNotfoundException;
import iticket.intgration.iticket.categoria.repository.CategoriaRepository;

@Service
public class CategoriaService {

	@Autowired
	private CategoriaRepository categoriaRepository;
	
	public List<CategoriaDTO> findAll() {

		List<CategoriaEntity> categorias = categoriaRepository.findAll();
		
		return categorias.stream().map(CategoriaDTO::new).collect(Collectors.toList());
	}

	public ResponseEntity<CategoriaDTO> findById(Long id) {

		Optional<CategoriaEntity> optCategoria = categoriaRepository.findById(id);
		
		if(!optCategoria.isPresent()) {
			throw new CategoriaNotfoundException("Categoria com id <" + id + "> não encontrada!");
		}
		
		return ResponseEntity.status(HttpStatus.OK).body(new CategoriaDTO(optCategoria.get()));
	}

	public ResponseEntity<Response> saveCategoria(CategoriaRequest categoria) {

		Response response = new Response(true, new ArrayList<>());
		
		validateNewCategoria(categoria, response);
		
		if(!response.isSuccess()) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}
		
		CategoriaEntity entity = new CategoriaEntity();
		entity.setNome(categoria.getNome());
		entity.setDescricao(categoria.getDescricao());
		
		categoriaRepository.save(entity);
		
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}

	private void validateNewCategoria(CategoriaRequest categoria, Response response) {

		if(categoria == null) {
			response.setSuccess(false);
			response.getErrors().add("Por Favor, informe um categoria válida!");
			return;
		}
		
		if(categoria.getNome() == null || categoria.getNome().isEmpty()) {
			response.setSuccess(false);
			response.getErrors().add("O nome da categoria é requerido!");
		}
	}

	public ResponseEntity<Response> updateCategoria(Long id, CategoriaRequest categoriaReq) {

		Optional<CategoriaEntity> optCategoria = categoriaRepository.findById(id);
		
		Response response = new Response(true, new ArrayList<>());
		
		if(!optCategoria.isPresent()) {
			response.setSuccess(false);
			response.getErrors().add("Nenhuma categoria foi encontrada com o id <" + id + ">");
			return ResponseEntity.status(HttpStatus.OK).body(response);
		}
		
		validateNewCategoria(categoriaReq, response);
		
		if(!response.isSuccess()) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}
		
		CategoriaEntity categoria = optCategoria.get();
		
		categoria.setNome(categoriaReq.getNome());
		if(categoriaReq.getDescricao() != null) {
			categoria.setDescricao(categoriaReq.getDescricao());
		}
		
		categoriaRepository.save(categoria);
		
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}

	public ResponseEntity<Response> deleteCategoria(Long id) {

		Response response = new Response(true, new ArrayList<>());
		
		categoriaRepository.deleteById(id);
		
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}

	
	
}
