package iticket.intgration.iticket.categoria.controller;

import static iticket.intgration.iticket.categoria.constants.PermissionRoles.ATENDENTE;
import static iticket.intgration.iticket.categoria.constants.PermissionRoles.USUARIO;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import iticket.intgration.iticket.categoria.client.UsuarioClient;
import iticket.intgration.iticket.categoria.domain.Response;
import iticket.intgration.iticket.categoria.domain.categoria.CategoriaRequest;
import iticket.intgration.iticket.categoria.dto.CategoriaDTO;
import iticket.intgration.iticket.categoria.service.CategoriaService;

@RestController
public class CategoriaController {

	@Autowired
	private CategoriaService categoriaService;
	
	@Autowired
	private UsuarioClient usuarioClient;
	
	private static final String APIKEY = "NkkkcmpSbmV2ZDQzTVI3byoycFdiTk1fMmFMVmVTLWZIcjZMU2dGQ0tHNUU0RUZGTiU=";
	
	@GetMapping("/categoria/all")
	public ResponseEntity<List<CategoriaDTO>> findCategorias(@RequestHeader("Authorization") String token) {
		
		usuarioClient.validateUserToken(token, APIKEY, Arrays.asList(ATENDENTE.getRole(), USUARIO.getRole()));
		
		List<CategoriaDTO> categorias = categoriaService.findAll();
		return ResponseEntity.status(HttpStatus.OK).body(categorias);
	} 
	
	@GetMapping("/categoria/{id}")
	public ResponseEntity<CategoriaDTO> findCategoriaById(@PathVariable("id") Long id, @RequestHeader("Authorization") String token) {
		
		usuarioClient.validateUserToken(token, APIKEY, Arrays.asList(ATENDENTE.getRole(), USUARIO.getRole()));
		
		return categoriaService.findById(id);
	}

	@PostMapping("/categoria")
	public ResponseEntity<Response> saveCategoria(@RequestHeader("Authorization") String token, @RequestBody CategoriaRequest categoria) {
		
		usuarioClient.validateUserToken(token, APIKEY, Arrays.asList(ATENDENTE.getRole()));
		
		return categoriaService.saveCategoria(categoria);
	}
	
	@PutMapping("/cateogria/{id}")
	public ResponseEntity<Response> updateCategoria(@PathVariable("id") Long id, @RequestHeader("Authorization") String token, @RequestBody CategoriaRequest categoria) {
		
		usuarioClient.validateUserToken(token, APIKEY, Arrays.asList(ATENDENTE.getRole()));
		
		return categoriaService.updateCategoria(id, categoria);
	}
	
	@DeleteMapping("/categoria/{id}")
	public ResponseEntity<Response> deleteCategoria(@PathVariable("id") Long id, @RequestHeader("Authorization") String token) {
		
		usuarioClient.validateUserToken(token, APIKEY, Arrays.asList(ATENDENTE.getRole()));
		
		return categoriaService.deleteCategoria(id);
	}
}

