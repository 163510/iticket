package iticket.intgration.iticket.categoria.exception.handle;

import java.util.Arrays;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import feign.FeignException;
import iticket.intgration.iticket.categoria.domain.Response;
import iticket.intgration.iticket.categoria.exception.CategoriaNotfoundException;
import iticket.intgration.iticket.categoria.exception.SubCategoriaNotFoundException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestControllerAdvice
public class Handler {

	// ###########################
	// ######## CATEGORIA ########
	// ###########################
	
	@ExceptionHandler(CategoriaNotfoundException.class)
	public ResponseEntity<Response> handleCategoriaNotfoundException(WebRequest request, CategoriaNotfoundException e) {
		log.error("handleCategoriaNotfoundException() - [INFO] - CategoriaNotfoundException - request path <{}> - message <{}> - error: ", request.getContextPath(), e.getMessage(), e);
		Response response = new Response(false, Arrays.asList(e.getMessage()));
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
	}
	
	// ###########################
	// ###### SUB CATEGORIA ######
	// ###########################
	
	@ExceptionHandler(SubCategoriaNotFoundException.class)
	public ResponseEntity<Response> handleSubCategoriaNotFoundException(WebRequest request, SubCategoriaNotFoundException e) {
		log.error("handleSubCategoriaNotFoundException() - [INFO] - SubCategoriaNotFoundException - request path <{}> - message <{}> - error: ", request.getContextPath(), e.getMessage(), e);
		Response response = new Response(false, Arrays.asList(e.getMessage()));
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
	}
	
	// ###########################
	// #### EXCEPTIONS GERAIS ####
	// ###########################
	
	@ExceptionHandler(Exception.class)
	public ResponseEntity<Response> handleException(WebRequest request, Exception e) {
		log.error("handleException() - [ERROR] - Exception - request path <{}> - message <{}> - error: ", request.getContextPath(), e.getMessage(), e);
		Response response = new Response(false, Arrays.asList(e.getMessage()));
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
	}
	
	@ExceptionHandler(FeignException.class)
	public ResponseEntity<Response> handleFeignException(WebRequest request, FeignException e) {
		log.error("handleFeignException() - [ERROR] - FeignException - request path <{}> - message <{}> - error: ", request.getContextPath(), e.contentUTF8(), e);
		Response response = new Response(false, Arrays.asList(e.contentUTF8()));
		return ResponseEntity.status(e.status()).body(response);
	}
}
