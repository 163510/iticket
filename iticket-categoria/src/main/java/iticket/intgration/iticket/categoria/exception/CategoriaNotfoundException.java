package iticket.intgration.iticket.categoria.exception;

public class CategoriaNotfoundException extends RuntimeException{

	private static final long serialVersionUID = -690103939755897519L;

	public CategoriaNotfoundException() {
		super();
	}
	
	public CategoriaNotfoundException(String msg) {
		super(msg);
	}
}
