package iticket.intgration.iticket.categoria.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import iticket.intgration.iticket.categoria.domain.Response;
import iticket.intgration.iticket.categoria.dto.SubCategoriaDTO;
import iticket.intgration.iticket.categoria.entity.CategoriaEntity;
import iticket.intgration.iticket.categoria.entity.SubCategoriaEntity;
import iticket.intgration.iticket.categoria.exception.SubCategoriaNotFoundException;
import iticket.intgration.iticket.categoria.repository.SubCategoriaRepository;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class SubCategoriaService {

	@Autowired
	private SubCategoriaRepository subCategoriaRepository;
	
	public List<SubCategoriaDTO> findAll() {

		List<SubCategoriaEntity> subcategories = subCategoriaRepository.findAll();
		
		return subcategories.stream().map(SubCategoriaDTO::new).collect(Collectors.toList());
	}

	public ResponseEntity<SubCategoriaDTO> findById(Long id) {

		Optional<SubCategoriaEntity> optSubCategoria = subCategoriaRepository.findById(id);
		
		if(!optSubCategoria.isPresent()) {
			throw new SubCategoriaNotFoundException("Sub categoria de id <" + id + "> não encontrada!");
		}
		
		SubCategoriaEntity subCategoria = optSubCategoria.get();
		return ResponseEntity.status(HttpStatus.OK).body(new SubCategoriaDTO(subCategoria));
	}
	
	public List<SubCategoriaDTO> findByCategoryId(Long id) {

		List<SubCategoriaEntity> subCategorias = subCategoriaRepository.findByCategoryId(id);
		
		return subCategorias.stream().map(SubCategoriaDTO::new).collect(Collectors.toList());
	}
	public ResponseEntity<Response> save(SubCategoriaDTO subCategoria) {
		log.info("sub categoria request: {}", subCategoria);
		Response response = new Response(true, new ArrayList<>());
		
		validateSubCategoria(subCategoria, response);
		
		if(!response.isSuccess()) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}
		
		SubCategoriaEntity entity = new SubCategoriaEntity(subCategoria);
		
		subCategoriaRepository.save(entity);
		
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}

	private void validateSubCategoria(SubCategoriaDTO subCategoria, Response response) {

		if(subCategoria == null) {
			response.setSuccess(false);
			response.getErrors().add("Por favor, informe uma subcategoria!");
			return;
		}
		
		if(subCategoria.getCategoria() == null) {
			response.setSuccess(false);
			response.getErrors().add("Por favor seleciona uma categoria!");
		}
		
		if(subCategoria.getNome() == null || subCategoria.getDescricao().isEmpty()) {
			response.setSuccess(false);
			response.getErrors().add("Por favor preencha o valor do campo nome!");
		}
		
		if(subCategoria.getTemplate() == null || subCategoria.getTemplate().isEmpty()) {
			response.setSuccess(false);
			response.getErrors().add("Por favor informe o campo template!");
		}
		
		if(subCategoria.getTempoPrevistoAtendimento() == null || subCategoria.getTempoPrevistoAtendimento() < 0) {
			response.setSuccess(false);
			response.getErrors().add("Por favor informe o campo tempo previsto de atendimento!");
		}
	}

	public ResponseEntity<Response> updateSubCategoria(Long id, SubCategoriaDTO subCategoria) {

		Response response = new Response(true, new ArrayList<>());
		
		Optional<SubCategoriaEntity> optSubcategoria = subCategoriaRepository.findById(id);
		
		if(!optSubcategoria.isPresent()) {
			response.setSuccess(false);
			response.getErrors().add("Nenhuma subcategoria foi encontrada com o id <" + subCategoria.getId() + ">");
			return ResponseEntity.status(HttpStatus.OK).body(response);
		}
		
		validateSubCategoria(subCategoria, response);
		
		if(!response.isSuccess()) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}
		
		SubCategoriaEntity entity = optSubcategoria.get();
		
		changeEntityInfos(entity, subCategoria);
		
		subCategoriaRepository.save(entity);
		
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}

	private void changeEntityInfos(SubCategoriaEntity entity, SubCategoriaDTO subCategoria) {
		entity.setNome(subCategoria.getNome());
		entity.setTemplate(subCategoria.getTemplate());
		entity.setDescricao(subCategoria.getDescricao());
		entity.setTempoPrevistoAtendimento(subCategoria.getTempoPrevistoAtendimento());
		entity.setCategoria(new CategoriaEntity(subCategoria.getCategoria()));
	}

	public ResponseEntity<Response> deleteById(Long id) {

		Response response = new Response(true, new ArrayList<>());
		
		subCategoriaRepository.deleteById(id);
		
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}

}
