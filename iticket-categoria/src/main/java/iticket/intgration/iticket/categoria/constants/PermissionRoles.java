package iticket.intgration.iticket.categoria.constants;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum PermissionRoles {

	ADMIN("ADMIN"),
	USUARIO("USUARIO"),
	ATENDENTE("ATENDENTE");
	
	private String role;
	
}
