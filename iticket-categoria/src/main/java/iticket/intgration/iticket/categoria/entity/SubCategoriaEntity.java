package iticket.intgration.iticket.categoria.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import iticket.intgration.iticket.categoria.dto.SubCategoriaDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Table(name = "sub_categoria")
@NoArgsConstructor
@AllArgsConstructor
public class SubCategoriaEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "nome")
	private String nome;
	
	@Lob
	@Column(name = "descricao")
	private String descricao;
	
	@Lob
	@Column(name = "template")
	private String template;
	
	//Campo em minutos
	@Column(name = "tempo_previsto_atendimento")
	private Integer tempoPrevistoAtendimento;
	
	@Column(name = "data_criacao")
	private LocalDateTime dataCriacao;
	
	@Column(name = "data_atualizacao")
	private LocalDateTime dataAtualizacao;
	
	@ManyToOne
	@JoinColumn(name = "categoria_id")
	private CategoriaEntity categoria;
	
	public SubCategoriaEntity(SubCategoriaDTO dto) {
		this.id = dto.getId();
		this.nome = dto.getNome();
		this.descricao = dto.getDescricao();
		this.template = dto.getTemplate();
		this.tempoPrevistoAtendimento = dto.getTempoPrevistoAtendimento();
		this.dataCriacao = dto.getDataCriacao();
		this.dataAtualizacao = dto.getDataAtualizacao();
		this.categoria = new CategoriaEntity(dto.getCategoria());
	}
}
