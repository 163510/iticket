package iticket.intgration.iticket.categoria.dto;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import iticket.intgration.iticket.categoria.entity.SubCategoriaEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SubCategoriaDTO {

	private Long id;
	private String nome;
	private String descricao;
	private String template;
	private Integer tempoPrevistoAtendimento;
	private LocalDateTime dataCriacao;
	private LocalDateTime dataAtualizacao;
	private CategoriaDTO categoria;
	
	public SubCategoriaDTO(SubCategoriaEntity entity) {
		this.id = entity.getId();
		this.nome = entity.getNome();
		this.descricao = entity.getDescricao();
		this.template = entity.getTemplate();
		this.tempoPrevistoAtendimento = entity.getTempoPrevistoAtendimento();
		this.dataCriacao = entity.getDataCriacao();
		this.dataAtualizacao = entity.getDataAtualizacao();
		this.categoria = new CategoriaDTO(entity.getCategoria());
	}
}
